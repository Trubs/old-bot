package com.rocks.sportibot.utils.extensions

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Test
import java.util.*

class TimeExtentionsKtTest {

    @Test
    fun getTimeAgoFrom() {
        val minusDays = 2
        val minusMinutes = 2

        val dateToday = Date()
        val dateLastOnline = dateToday
                .minus(Calendar.DAY_OF_MONTH, minusDays)
                .minus(Calendar.MINUTE, minusMinutes)

//        System.out.println("${tag()}: ${dateToday.toDisplayTimeFormat()}")
//        System.out.println("${tag()}: ${dateLastOnline.toDisplayTimeFormat()}")

        val humanTime = dateToday.getTimeAgoFrom(dateLastOnline)

        assertFalse(humanTime.isMoreThanMonth())

        assertEquals(humanTime.days, minusDays)
        assertEquals(humanTime.hours, 0)
        assertEquals(humanTime.minutes, minusMinutes)
        assertEquals(humanTime.seconds, 0)

//        System.out.println("${tag()}: $humanTime")
    }
}
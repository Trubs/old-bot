package com.rocks.sportibot.utils.extensions

import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.*

class DateExtensionsKtTest {

    @Test
    fun minus() {
        val currentDate = Date()

        val minusMonthDate = currentDate.minus(Calendar.MONTH, 1)

        assertNotEquals(currentDate, minusMonthDate)

        assertTrue(minusMonthDate.before(currentDate))
    }
}
package com.rocks.sportibot

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.onesignal.OneSignal
import com.rocks.sportibot.data.managers.device_token.AppDeviceTokenManager
import com.rocks.sportibot.di.app.AppComponent
import com.rocks.sportibot.di.app.AppModule
import com.rocks.sportibot.di.app.DaggerAppComponent
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.utils.extensions.log_i
import com.rocks.sportibot.utils.extensions.toDateDisplayFormat
import io.fabric.sdk.android.Fabric
import java.util.*
import javax.inject.Inject

class App : Application() {

    @Inject
    lateinit var appDeviceTokenManager: AppDeviceTokenManager

    /**
     * App created
     * */
    override fun onCreate() {
        super.onCreate()

        initDi()

        initCrashlytics()

        initPush()

        Crashlytics.log("Crashlytics initialized at the ${Date().toDateDisplayFormat()}")
    }

    /**
     * Initializing Dependency Injection
     * */
    private fun initDi() {
        val appComponent: AppComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
        ComponentProvider.init(appComponent)
        appComponent.inject(this)
    }

    /**
     * Initializing Crashlytics for reporting of the crashes, exceptions or logs
     * */
    private fun initCrashlytics() {
        val disabledCrashlytics = !BuildConfig.CRASHLYTICS_ENABLE

        log_i("Crashlytics enabled: ${!disabledCrashlytics}")

        Fabric.with(this, Crashlytics.Builder()
                .core(CrashlyticsCore.Builder()
                        .disabled(disabledCrashlytics)
                        .build())
                .build())
    }

    private fun initPush() {
        OneSignal.startInit(this).init()
        OneSignal.idsAvailable { userId, registrationId ->
            appDeviceTokenManager.onDeviceTokenUpdated(registrationId)
        }
    }
}

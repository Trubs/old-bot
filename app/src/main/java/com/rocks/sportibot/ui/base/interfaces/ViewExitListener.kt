package com.rocks.sportibot.ui.base.interfaces

interface ViewExitListener {
    fun exit()
}
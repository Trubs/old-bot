package com.rocks.sportibot.ui.login.sign_up

import com.rocks.sportibot.ui.base.interfaces.ViewProgressListener
import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract

interface SignUpContract {
    interface View : BaseContract.View,
            ViewProgressListener,
            ViewThrowableListener {

        /**
         * Opening [com.rocks.sportibot.ui.main.MainActivity]
         * */
        fun openMainActivity()

        /**
         * Opening [com.rocks.sportibot.ui.login.sign_in.SignInActivity]
         * */
        fun openLoginActivity()

    }

    interface Presenter : BaseContract.Presenter<View> {
        fun onSignUpPressed(signUpDto: SignUpDto)

        fun onLoginPressed()
    }
}

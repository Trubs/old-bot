package com.rocks.sportibot.ui.main.history

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.history.History
import com.rocks.sportibot.data.managers.OnFilterActivateWindow
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.clicklisteners.filteractivate.FilterActivateChangeListener
import com.rocks.sportibot.ui.base.fragment.mvp.BaseMvpFragment
import com.rocks.sportibot.ui.base.recycler_adapter.DrawableDividerDecorator
import com.rocks.sportibot.ui.base.recycler_adapter.VerticalSpaceItemDecoration
import com.rocks.sportibot.ui.main.history.settings.SettingsFragment
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment : BaseMvpFragment<HistoryContract.Presenter>(), HistoryContract.View {
    override val layoutRes: Int = R.layout.fragment_history

    private val adapter = HistoryAdapter()

    override fun injectPresenter() {
        ComponentProvider.Main.getHistoryComponent().inject(this)
        presenter.view = this
    }

    private val fragmentSettings: SettingsFragment = SettingsFragment()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setTitle(R.string.title_history)

        initViews()
        initListeners()
    }

    private fun initViews() {
        //set filterImageButton activated state if fragmentSettings is visible
        filterImageButton.isActivated = fragmentSettings.isAdded
        historyRecyclerView.layoutManager = LinearLayoutManager(context)
        val decoration = VerticalSpaceItemDecoration(resources.getDimensionPixelSize(R.dimen.margin_small),
                DrawableDividerDecorator(context!!, R.drawable.divider_gray))
        historyRecyclerView.addItemDecoration(decoration)
        historyRecyclerView.adapter = adapter
    }


    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener { presenter.onRefresh() }
        filterImageButton.setOnClickListener(FilterActivateChangeListener(OnFilterActivateWindow(this)))
        fragmentSettings.onFilterChangeListener = { presenter.onFilterChanged() }
        historyRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                (recyclerView.layoutManager as LinearLayoutManager).apply {
                    val visibleItemCount = recyclerView.layoutManager.childCount
                    val totalItemCount = recyclerView.layoutManager.itemCount
                    val pasVisibleItem = findFirstVisibleItemPosition()

                    if (visibleItemCount + pasVisibleItem >= totalItemCount) {
                        presenter.loadNext()
                    }
                }
            }
        })

    }

    override fun showPopupFilterWindow() {
        childFragmentManager.beginTransaction()
                .add(contentContainer.id, fragmentSettings)
                .commitAllowingStateLoss()
    }

    override fun hidePopupFilterWindow() {
        childFragmentManager.beginTransaction()
                .remove(fragmentSettings)
                .commitAllowingStateLoss()
    }

    override fun setHistory(historyList: List<History>) {
        adapter.setData(historyList)
        historyRecyclerView.smoothScrollToPosition(1)
    }

    override fun addHistory(historyList: List<History>) {
        adapter.addItems(historyList)
    }

    override fun showPaginationProgress() {
        adapter.showPaginationProgress()
    }

    override fun hidePaginationProgress() {
        adapter.hidePaginationProgress()
    }


}
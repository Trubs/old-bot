package com.rocks.sportibot.ui.widgets

import android.content.Context
import android.support.annotation.ColorInt
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.rocks.sportibot.R

class TextActiveState @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : AppCompatTextView(context, attrs, defStyleAttr) {

    lateinit var activeText: String
    lateinit var inactiveText: String

    @ColorInt
    var activeTextColor: Int = currentTextColor // default value

    @ColorInt
    var inactiveTextColor: Int = currentTextColor // default value

    init {
        attrs?.let { initAttr(it) }
    }

    private fun initAttr(attrs: AttributeSet) {
        val typedArray = context.theme.obtainStyledAttributes(attrs, R
                .styleable.TextActiveState,
                0,
                0)

        try {

            activeText = typedArray.getString(R.styleable.TextActiveState_activeText)
            inactiveText = typedArray.getString(R.styleable.TextActiveState_inactiveText)

            activeTextColor = typedArray.getColor(R.styleable.TextActiveState_activeTextColor, currentTextColor)
            inactiveTextColor = typedArray.getColor(R.styleable.TextActiveState_inactiveTextColor, currentTextColor)

            /*setActivated(boolean)*/
            if (typedArray.hasValue(R.styleable.TextActiveState_active)) {
                isActivated = typedArray.getBoolean(R.styleable.TextActiveState_active, false)
            }

        } finally {
            typedArray.recycle()
        }
    }

    override fun setActivated(activated: Boolean) {
        super.setActivated(activated)

        val text: String
        @ColorInt val textColor: Int

        if (activated) {
            text = activeText
            textColor = activeTextColor
        } else {
            text = inactiveText
            textColor = inactiveTextColor
        }

        setText(text)
        setTextColor(textColor)
    }

}

package com.rocks.sportibot.ui.base.recycler_adapter

import android.graphics.Canvas
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

open class BaseRecyclerItemDecorator(
        private val itemDecorator: RecyclerView.ItemDecoration? = null
) : RecyclerView.ItemDecoration() {

    override fun onDrawOver(c: Canvas?, parent: RecyclerView?, state: RecyclerView.State?) {
        super.onDrawOver(c, parent, state)
        itemDecorator?.onDrawOver(c, parent, state)
    }

    override fun onDrawOver(c: Canvas?, parent: RecyclerView?) {
        super.onDrawOver(c, parent)
        itemDecorator?.onDrawOver(c, parent)
    }

    override fun onDraw(c: Canvas?, parent: RecyclerView?, state: RecyclerView.State?) {
        super.onDraw(c, parent, state)
        itemDecorator?.onDraw(c, parent, state)
    }

    override fun onDraw(c: Canvas?, parent: RecyclerView?) {
        super.onDraw(c, parent)
        itemDecorator?.onDraw(c, parent)
    }

    override fun getItemOffsets(outRect: Rect?, itemPosition: Int, parent: RecyclerView?) {
        super.getItemOffsets(outRect, itemPosition, parent)
        itemDecorator?.getItemOffsets(outRect, itemPosition, parent)
    }

    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        super.getItemOffsets(outRect, view, parent, state)
        itemDecorator?.getItemOffsets(outRect, view, parent, state)
    }
}
package com.rocks.sportibot.ui.base.interfaces

interface OnNavigationBackListener {
    /**
     * Handle back navigation listener
     * @return false for the default action
     * true if absorbed (redefined) action
     * */
    fun onNavigationBackPressed(): Boolean {
        return false
    }
}
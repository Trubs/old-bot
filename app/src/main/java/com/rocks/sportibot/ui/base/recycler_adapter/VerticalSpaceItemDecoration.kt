package com.rocks.sportibot.ui.base.recycler_adapter

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class VerticalSpaceItemDecoration(private val verticalSpaceHeight: Int,
                                  itemDecoration: RecyclerView.ItemDecoration? = null)
    : BaseRecyclerItemDecorator(itemDecoration) {


    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect?.bottom = verticalSpaceHeight
    }
}
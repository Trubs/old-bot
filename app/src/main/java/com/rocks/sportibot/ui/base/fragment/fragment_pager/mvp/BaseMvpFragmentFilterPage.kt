package com.rocks.sportibot.ui.base.fragment.fragment_pager.mvp

import android.os.Bundle
import android.view.View
import com.rocks.sportibot.ui.base.fragment.fragment_pager.BaseFragmentFilterPage
import com.rocks.sportibot.ui.base.fragment.fragment_pager.OnFragmentPagerLifecycle
import com.rocks.sportibot.ui.base.interfaces.OnFilterChangeListener
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract
import javax.inject.Inject

/**
 * Fragment for the ViewPager with lifecycle [OnFragmentPagerLifecycle]
 * */
abstract class BaseMvpFragmentFilterPage<P : BaseContract.Presenter<*>>
    : BaseFragmentFilterPage()
        where P : OnFilterChangeListener,
              P : OnFragmentPagerLifecycle {

    @Inject
    protected lateinit var presenter: P

    protected abstract fun injectPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectPresenter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onCreate()
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onShownPage() {
        super.onShownPage()
        presenter.onShownPage()
    }

    override fun onHiddenPage() {
        super.onHiddenPage()
        presenter.onHiddenPage()
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun onFilterChanged() {
        super.onFilterChanged()
        presenter.onFilterChanged()
    }
}
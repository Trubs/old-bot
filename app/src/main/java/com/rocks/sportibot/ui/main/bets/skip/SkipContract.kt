package com.rocks.sportibot.ui.main.bets.skip

import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.ui.base.fragment.fragment_pager.OnFragmentPagerLifecycle
import com.rocks.sportibot.ui.base.interfaces.OnRefreshListener
import com.rocks.sportibot.ui.base.interfaces.ViewProgressListener
import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract

interface SkipContract {
    interface View : BaseContract.View,
            ViewThrowableListener,
            ViewProgressListener {

        fun setBetList(bets: List<Bet>)

        fun addBet(bet: Bet)
    }

    interface Presenter : BaseContract.Presenter<View>,
            OnFragmentPagerLifecycle,
            OnRefreshListener
}
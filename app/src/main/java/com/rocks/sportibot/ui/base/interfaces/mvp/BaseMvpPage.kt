package com.rocks.sportibot.ui.base.interfaces.mvp

import com.rocks.sportibot.ui.base.fragment.fragment_pager.BaseFragmentFilterPage
import com.rocks.sportibot.ui.base.interfaces.OnFilterChangeListener

abstract class BaseMvpPage<P : BaseContract.Presenter<*>> : BaseFragmentFilterPage() where P : OnFilterChangeListener

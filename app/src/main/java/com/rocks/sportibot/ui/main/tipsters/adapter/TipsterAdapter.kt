package com.rocks.sportibot.ui.main.tipsters.adapter

import android.view.ViewGroup
import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.ui.base.interfaces.ViewEnabledListener
import com.rocks.sportibot.ui.base.interfaces.tipster.OnTipsterClickListener
import com.rocks.sportibot.ui.base.recycler_adapter.BaseRecyclerAdapter
import com.rocks.sportibot.ui.base.recycler_adapter.BaseRecyclerViewHolder

class TipsterAdapter(private val tipsterEnum: TipsterEnum) : BaseRecyclerAdapter() {

    var onTipsterClickListener: OnTipsterClickListener? = null
    var onFollowTipsterClickListener: ((Tipster, ViewEnabledListener) -> Unit)? = null
    var onUnFollowTipsterClickListener: ((Tipster, ViewEnabledListener) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRecyclerViewHolder {
        return when (tipsterEnum) {
            TipsterEnum.MY -> TipsterMyViewHolder(parent)
            TipsterEnum.ALL -> TipsterAllViewHolder(parent)
        }
    }

    override fun onBindViewHolder(holder: BaseRecyclerViewHolder, position: Int) {
        (holder as BaseTipsterViewHolder).let {
            it.bind(getItemOfPosition(position))

            it.onTipsterClickListener = onTipsterClickListener
            it.onFollowTipsterClickListener = onFollowTipsterClickListener
            it.onUnFollowTipsterClickListener = onUnFollowTipsterClickListener
        }
    }

    fun setTipsters(list: List<Tipster>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    override fun updateItem(item: Any) {
        super.updateItem(item)
    }

    override fun addItem(item: Any) {
        super.addItem(item)
    }

    override fun removeItem(item: Any) {
        super.removeItem(item)
    }

    @Deprecated("use setTipsters(list)", ReplaceWith("super.setData(list)", "com.rocks.sportibot.ui.base.recycler_adapter.BaseRecyclerAdapter"))
    override fun setData(list: List<Any>) {
        super.setData(list)
    }

}
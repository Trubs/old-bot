package com.rocks.sportibot.ui.base.recycler_adapter

import android.support.v7.widget.RecyclerView
import com.rocks.sportibot.utils.extensions.log_e

open abstract class BaseRecyclerAdapter : RecyclerView.Adapter<BaseRecyclerViewHolder>() {
    protected val items: MutableList<Any> = mutableListOf()

    final override fun getItemCount(): Int = items.size
    final fun getLastPosition() = itemCount - 1

    fun <T> getItemOfPosition(position: Int): T = items[position] as T

    /**
     * Rewrite data of list and to notify adapter
     * */
    open fun setData(list: List<Any>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    /**
     * Add item to existing list
     * */
    open fun addItem(item: Any) {
        if (items.contains(item)) {
            log_e("list already contains item: $item")
            return
        }

        items.add(item)

        notifyItemInserted(getLastPosition())
    }

    /**
     * Add collection to existing list
     * */
    open fun addItems(list: Collection<Any>) {
        items.addAll(list)

        notifyItemRangeInserted(getLastPosition(), list.size)
    }

    /**
     * Remove item from the list
     * */
    open fun removeItem(item: Any) {

        val index = items.indexOf(item)

        if (index == -1) {
            log_e("list not contains item: $item")
            return
        }

        items.removeAt(index)
        notifyItemRemoved(index)
    }

    /**
     * Update item in the list
     * */
    open fun updateItem(item: Any) {
        val index = items.indexOf(item)

        if (index != -1) {

            items[index] = item

            notifyItemChanged(index)
        } else {
            log_e("Not found item in list")
            log_e("Cannot update item:$item\b" +
                    "index in list: $index")
        }
    }

}
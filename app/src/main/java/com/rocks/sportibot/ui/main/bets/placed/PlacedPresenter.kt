package com.rocks.sportibot.ui.main.bets.placed

import com.rocks.sportibot.data.interactors.bets.BetsInteractor
import com.rocks.sportibot.utils.extensions.manageProgressView
import com.rocks.sportibot.utils.extensions.scheduleIOToMainThread
import com.rocks.sportibot.utils.extensions.wrapViewObservable
import com.rocks.sportibot.utils.extensions.wrapViewSingle
import io.reactivex.disposables.CompositeDisposable

class PlacedPresenter(
        private val betsInteractor: BetsInteractor,
        private val compositeDisposable: CompositeDisposable
) : PlacedContract.Presenter {

    override lateinit var view: PlacedContract.View

    override fun onCreate() {
        super.onCreate()

        loadBets()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onRefresh() {
        loadBets()
    }

    private fun loadBets() {
        compositeDisposable.add(betsInteractor
                .getBetsPlacedAsync()
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .subscribeWith(wrapViewSingle(view, {
                    view.setBetList(it)
                })))

        //todo after stop() must be re-subscribing
        compositeDisposable.add(betsInteractor
                .getPlacedBetObservable()
                .scheduleIOToMainThread()
                .subscribeWith(wrapViewObservable(view, {
                    view.addBet(it)
                })))
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}
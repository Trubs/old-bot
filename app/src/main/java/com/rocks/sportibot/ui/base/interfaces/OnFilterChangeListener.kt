package com.rocks.sportibot.ui.base.interfaces

import com.rocks.sportibot.utils.extensions.log_i

interface OnFilterChangeListener {

    fun onFilterChanged() {
        log_i("onFilterChanged")
    }

}
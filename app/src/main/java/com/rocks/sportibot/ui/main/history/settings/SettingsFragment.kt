package com.rocks.sportibot.ui.main.history.settings

import android.os.Bundle
import android.view.View
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.history.filter.ShowBetsHistoryEnum
import com.rocks.sportibot.data.dto.history.filter.SortHistoryEnum
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.fragment.mvp.BaseMvpFragment
import com.rocks.sportibot.utils.extensions.showDateDialog
import kotlinx.android.synthetic.main.fragment_history_settings.*
import java.util.*

class SettingsFragment : BaseMvpFragment<SettingsContract.Presenter>(), SettingsContract.View {

    override val layoutRes: Int = R.layout.fragment_history_settings

    var onFilterChangeListener: (() -> Unit)? = null

    override fun injectPresenter() {
        ComponentProvider.Main.getHistoryComponent().inject(this)
        presenter.view = this

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() {
        dateFromTextView.setOnClickListener { presenter.onDateFromPressed() }
        dateUntilTextView.setOnClickListener { presenter.onDateUntilPressed() }

        showBetsRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                runningRadioButton.id -> presenter.onShowBetsChanged(ShowBetsHistoryEnum.RUNNING)
                openRadioButton.id -> presenter.onShowBetsChanged(ShowBetsHistoryEnum.OPEN)
            }
        }
        sortByRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                dateRadioButton.id -> presenter.onSortByChanged(SortHistoryEnum.DATE)
                profitRadioButton.id -> presenter.onSortByChanged(SortHistoryEnum.PROFIT)
                yieldRadioButton.id -> presenter.onSortByChanged(SortHistoryEnum.YIELD)
            }
        }
    }

    override fun setShowBetsLabel(showBetsHistoryEnum: ShowBetsHistoryEnum) {
        showBetsRadioGroup.check(when (showBetsHistoryEnum) {
            ShowBetsHistoryEnum.RUNNING -> runningRadioButton.id
            ShowBetsHistoryEnum.OPEN -> openRadioButton.id
        })
    }

    override fun setDateFromLabel(date: String) {
        dateFromTextView.text = date
    }

    override fun setDateUntilLabel(date: String) {
        dateUntilTextView.text = date
    }

    override fun setSortByLabel(sortHistoryEnum: SortHistoryEnum) {
        sortByRadioGroup.check(
                when (sortHistoryEnum) {
                    SortHistoryEnum.DATE -> dateRadioButton.id
                    SortHistoryEnum.PROFIT -> profitRadioButton.id
                    SortHistoryEnum.YIELD -> yieldRadioButton.id
                }
        )
    }

    override fun showSelectDateFromDialog(currentDate: Date, minDate: Date?, maxDate: Date?) {
        context?.showDateDialog(currentDate, minDate, maxDate) { year, month, dayOfMonth ->
            presenter.onDateFromChanged(year, month, dayOfMonth)
        }
    }

    override fun showSelectDateUntilDialog(currentDate: Date, minDate: Date?, maxDate: Date?) {
        context?.showDateDialog(currentDate, minDate, maxDate) { year, month, dayOfMonth ->
            presenter.onDateUntilChanged(year, month, dayOfMonth)
        }
    }

    override fun notifyFilterChanged() {
        onFilterChangeListener?.invoke()
    }

}
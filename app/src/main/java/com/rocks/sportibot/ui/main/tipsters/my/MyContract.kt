package com.rocks.sportibot.ui.main.tipsters.my

import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.ui.base.fragment.fragment_pager.OnFragmentPagerLifecycle
import com.rocks.sportibot.ui.base.interfaces.*
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract
import com.rocks.sportibot.ui.base.interfaces.tipster.OnTipsterClickListener

interface MyContract {
    interface View : BaseContract.View,
            ViewProgressListener,
            ViewThrowableListener {

        fun setTipsterList(tipsters: List<Tipster>)

        fun addTipster(tipsters: Tipster)

        fun removeTipster(tipsters: Tipster)
    }

    //todo move listeners to base presenter
    interface Presenter : BaseContract.Presenter<View>,
            OnTipsterClickListener,
            OnFilterChangeListener,
            OnFragmentPagerLifecycle,
            OnRefreshListener {

        /**
         * @param tipster - tipster to unsubscribe
         * @param viewEnabledListener - button enable listener
         * */
        fun onUnfollowTipsterPressed(tipster: Tipster, viewEnabledListener: ViewEnabledListener)

    }
}
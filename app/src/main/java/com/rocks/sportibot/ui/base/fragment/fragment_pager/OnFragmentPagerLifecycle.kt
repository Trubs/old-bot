package com.rocks.sportibot.ui.base.fragment.fragment_pager

import com.rocks.sportibot.utils.extensions.log_i

interface OnFragmentPagerLifecycle {

    /**
     * Called when a fragment is shown
     * */
    fun onShownPage() {
        log_i("onShownPage")
    }

    /**
     * Called when a fragment is hidden
     * */
    fun onHiddenPage() {
        log_i("onHiddenPage")
    }
}
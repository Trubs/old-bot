package com.rocks.sportibot.ui.base.interfaces

interface ViewEnabledListener {

    fun setEnabled(enable: Boolean)

}
package com.rocks.sportibot.ui.widgets

import android.content.Context
import android.support.v7.widget.AppCompatButton
import android.util.AttributeSet
import com.rocks.sportibot.R
import com.rocks.sportibot.ui.base.interfaces.ViewEnabledListener

class ButtonActiveState : AppCompatButton, ViewEnabledListener {

    lateinit var activeText: String
    lateinit var inactiveText: String


    @JvmOverloads constructor(
            context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
    ) : super(context, attrs, defStyleAttr) {
        attrs?.let { initAttr(it) }
    }

    private fun initAttr(attrs: AttributeSet) {
        val typedArray = context.theme.obtainStyledAttributes(attrs, R
                .styleable.TextActiveState,
                0,
                0)

        try {

            activeText = typedArray.getString(R.styleable.TextActiveState_activeText)
            inactiveText = typedArray.getString(R.styleable.TextActiveState_inactiveText)

            /*setActivated(boolean)*/
            if (typedArray.hasValue(R.styleable.TextActiveState_active)) {
                isActivated = typedArray.getBoolean(R.styleable.TextActiveState_active, false)
            }

        } finally {
            typedArray.recycle()
        }
    }

    override fun setActivated(activated: Boolean) {
        super.setActivated(activated)

        text = if (activated) activeText else inactiveText
    }
}

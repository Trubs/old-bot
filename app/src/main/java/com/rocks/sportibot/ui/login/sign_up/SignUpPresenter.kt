package com.rocks.sportibot.ui.login.sign_up

import com.rocks.sportibot.data.interactors.login.LoginInteractor
import com.rocks.sportibot.utils.extensions.manageProgressView
import com.rocks.sportibot.utils.extensions.scheduleIOToMainThread
import com.rocks.sportibot.utils.extensions.wrapViewSingle
import io.reactivex.disposables.CompositeDisposable

class SignUpPresenter(
        private val loginInteractor: LoginInteractor,
        private val compositeDisposable: CompositeDisposable
) : SignUpContract.Presenter {

    override lateinit var view: SignUpContract.View


    override fun onSignUpPressed(signUpDto: SignUpDto) {
        compositeDisposable.clear()

        compositeDisposable.add(loginInteractor
                .singUp(signUpDto)
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .subscribeWith(wrapViewSingle(view, {
                    view.openMainActivity()
                })))
    }

    override fun onLoginPressed() {
        view.openLoginActivity()
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}
package com.rocks.sportibot.ui.main.tipsters.adapter

import android.view.View
import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.ui.base.interfaces.ViewEnabledListener
import com.rocks.sportibot.ui.base.interfaces.tipster.OnTipsterClickListener
import com.rocks.sportibot.ui.base.recycler_adapter.BaseRecyclerViewHolder

abstract class BaseTipsterViewHolder(view: View) : BaseRecyclerViewHolder(view) {
    var onTipsterClickListener: OnTipsterClickListener? = null
    var onFollowTipsterClickListener: ((Tipster, ViewEnabledListener) -> Unit)? = null
    var onUnFollowTipsterClickListener: ((Tipster, ViewEnabledListener) -> Unit)? = null

    abstract fun bind(tipster: Tipster)
}
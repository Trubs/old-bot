package com.rocks.sportibot.ui.main.settings

import android.os.Bundle
import android.view.View
import com.rocks.sportibot.R
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.dialogs.ProgressDialog
import com.rocks.sportibot.ui.base.fragment.mvp.BaseMvpFragment
import com.rocks.sportibot.ui.login.sign_in.SignInActivity
import com.rocks.sportibot.utils.extensions.getTextString
import com.rocks.sportibot.utils.extensions.showToastMessage
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.include_user_api_fields.*

class SettingsFragment : BaseMvpFragment<SettingsContract.Presenter>(), SettingsContract.View {
    override val layoutRes: Int = R.layout.fragment_settings

    override fun injectPresenter() {
        ComponentProvider.Main.getSettingsComponent().inject(this)
        presenter.view = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog = ProgressDialog(activity, true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setTitle(R.string.title_settings)
        initListeners()
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener { presenter.onRefresh() }

        updateButton.setOnClickListener {
            presenter.updateApiPressed(
                    usernameEditText.getTextString(),
                    passwordEditText.getTextString()
            )
        }

        logoutButton.setOnClickListener { presenter.onLogoutPressed() }
    }

    override fun showApiUpdatedSuccessful() {
        context.showToastMessage(R.string.fragment_bets_settings_message_updated_successful)
    }

    override fun clearUserApiFields() {
        usernameEditText.text.clear()
        passwordEditText.text.clear()
    }

    override fun openLoginActivity() {
        startActivity(SignInActivity.getIntentClearTask(context!!))
    }

}
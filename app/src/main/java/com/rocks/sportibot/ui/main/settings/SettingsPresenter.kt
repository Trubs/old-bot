package com.rocks.sportibot.ui.main.settings

import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.Customer
import com.rocks.sportibot.data.dto.user.UserApiData
import com.rocks.sportibot.data.interactors.settings.SettingsInteractor
import com.rocks.sportibot.utils.extensions.*
import io.reactivex.disposables.CompositeDisposable

class SettingsPresenter(
        private val settingsInteractor: SettingsInteractor,
        private val compositeDisposable: CompositeDisposable
) : SettingsContract.Presenter {

    override lateinit var view: SettingsContract.View

    override fun onCreate() {
        super.onCreate()
        updateView(settingsInteractor.getAccount())
    }

    override fun onRefresh() {
        compositeDisposable.add(settingsInteractor
                .getAccountAsync()
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .subscribeWith(wrapViewObservable(view, onNextFunction = {
                    updateView(it)
                })))
    }

    override fun updateApiPressed(username: String?, password: String?) {
        checkUserApiFields(username, password, functionChecked = { userApiData ->
            compositeDisposable.add(settingsInteractor
                    .updateApiUser(userApiData)
                    .scheduleIOToMainThread()
                    .manageBlockProgressView(view)
                    .subscribeWith(wrapViewCompletable(view, {
                        view.showApiUpdatedSuccessful()
                        view.clearUserApiFields()
                    })))
        })
    }

    override fun onLogoutPressed() {
        compositeDisposable.add(settingsInteractor
                .logout()
                .scheduleIOToMainThread()
                .doOnSubscribe { view.showBlockProgress() }
                .doFinally { view.hideBlockProgress() }
                .subscribeWith(wrapViewCompletable(view, {
                    view.openLoginActivity()
                })))
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }

    private fun checkUserApiFields(username: String?,
                                   password: String?,
                                   functionChecked: ((UserApiData) -> Unit)) {

        if (username.isNullOrBlank()) {
            view.showError(R.string.fragment_settings_error_empty_username)
            return
        }

        if (password.isNullOrBlank()) {
            view.showError(R.string.fragment_settings_error_empty_password)
            return
        }

        functionChecked.invoke(UserApiData(username!!, password!!))

    }

    private fun updateView(customer: Customer) {
    }

}
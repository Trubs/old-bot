package com.rocks.sportibot.ui.widgets

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.rocks.sportibot.R
import kotlinx.android.synthetic.main.view_bet_range_text.view.*

class BetRangeView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_bet_range_text, this, true)

        val typedArray: TypedArray = context.theme.obtainStyledAttributes(attrs,
                R.styleable.BetRangeView,
                0,
                0)

        try {
            setLabel(typedArray.getString(R.styleable.BetRangeView_labelText))
            setValue(typedArray.getString(R.styleable.BetRangeView_valueText))
        } finally {
            typedArray.recycle()
        }
    }

    fun setLabel(title: String?) {
        labelTextView.text = title
    }

    fun setRange(label: String?) {
        rangeTextView.text = label
    }

    fun setValue(value: String?) {
        valueTextView.text = value
    }
}
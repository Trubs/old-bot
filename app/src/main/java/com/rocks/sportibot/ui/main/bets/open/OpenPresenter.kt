package com.rocks.sportibot.ui.main.bets.open

import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.data.interactors.bets.BetsInteractor
import com.rocks.sportibot.utils.extensions.manageProgressView
import com.rocks.sportibot.utils.extensions.scheduleIOToMainThread
import com.rocks.sportibot.utils.extensions.wrapViewObservable
import com.rocks.sportibot.utils.extensions.wrapViewSingle
import io.reactivex.disposables.CompositeDisposable

class OpenPresenter(
        private val betsInteractor: BetsInteractor,
        private val compositeDisposable: CompositeDisposable
) : OpenContract.Presenter {

    override lateinit var view: OpenContract.View

    override fun onCreate() {
        super.onCreate()

        loadBets()

        subscribeBetOpened()

        subscribeBetPlacedOrSkipped()
    }

    override fun onRefresh() {
        loadBets()
    }

    private fun subscribeBetOpened() {
        compositeDisposable.add(betsInteractor
                .getOpenBetAddedObservable()
                .scheduleIOToMainThread()
                .subscribeWith(wrapViewObservable(view, {
                    view.addBet(it)
                })))
    }

    private fun subscribeBetPlacedOrSkipped() {
        //placed
        compositeDisposable.add(betsInteractor
                .getPlacedBetObservable()
                .scheduleIOToMainThread()
                .subscribeWith(wrapViewObservable(view, {
                    view.removeBetFromList(it)
                })))

        //skipped
        compositeDisposable.add(betsInteractor
                .getSkippedBetObservable()
                .scheduleIOToMainThread()
                .subscribeWith(wrapViewObservable(view, {
                    view.removeBetFromList(it)
                })))
    }

    private fun loadBets() {
        compositeDisposable.add(betsInteractor
                .getBetsOpenAsync()
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .subscribeWith(wrapViewSingle(view, {
                    view.setBetList(it)
                })))
    }

    override fun onBetPressed(bet: Bet) {
        view.openBetDetailsActivity(bet)
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}

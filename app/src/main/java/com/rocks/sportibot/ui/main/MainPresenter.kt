package com.rocks.sportibot.ui.main

import com.rocks.sportibot.data.interactors.main.MainInteractor
import com.rocks.sportibot.utils.extensions.manageThrowableView
import com.rocks.sportibot.utils.extensions.scheduleIOToMainThread
import com.rocks.sportibot.utils.extensions.wrapViewObservable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class MainPresenter(
        private val mainInteractor: MainInteractor,
        private val compositeDisposable: CompositeDisposable
) : MainContract.Presenter {

    override lateinit var view: MainContract.View
    private var balanceDisposable: Disposable? = null

    override fun onCreate() {
        super.onCreate()
        subscribeToBalance(true)
    }

    override fun onStart() {
        super.onStart()
        //re-subscribe to changes
        subscribeToBalance(false)
    }

    override fun onBackrollPressed() {
        subscribeToBalance(true)
    }

    override fun onLiveHelpPressed() {
        //TODO("LiveHelp click not implemented")
    }

    override fun onBackPressed() {
        view.exit()
    }

    private fun subscribeToBalance(update: Boolean) {
        balanceDisposable?.let {
            compositeDisposable.remove(it)
        }

        balanceDisposable = mainInteractor
                .getBalance(update)
                .scheduleIOToMainThread()
                .doOnSubscribe { view.bankrollShowProgress() }
                .subscribeWith(wrapViewObservable(view,
                        onNextFunction = {
                            view.bankrollHideProgress()
                            view.bankrollSetValue(it.balance, it.currency)
                        },
                        onErrorFunction = {
                            it.manageThrowableView(view)
                            view.bankrollHideProgress()
                        }))

        compositeDisposable.add(balanceDisposable!!)
    }
}
package com.rocks.sportibot.ui.login.sign_up

data class SignUpDto(
        val email: String,
        val username: String,
        val password: String,

        val confirmPassword: String,
        val isOver18: Boolean,
        val isAgreeTerms: Boolean
) {
    fun isCorrectPasswords(): Boolean {
        return password.isNotBlank()
                && confirmPassword.isNotBlank()
                && password == confirmPassword
    }

    fun isAgreedTerms(): Boolean {
        return isOver18 && isAgreeTerms
    }
}
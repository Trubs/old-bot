package com.rocks.sportibot.ui.base.interfaces

interface ViewPaginationProgressListener {

    fun showPaginationProgress()

    fun hidePaginationProgress()

}
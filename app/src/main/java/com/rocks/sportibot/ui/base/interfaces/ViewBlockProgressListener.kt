package com.rocks.sportibot.ui.base.interfaces

interface ViewBlockProgressListener {

    fun showBlockProgress()

    fun hideBlockProgress()

}
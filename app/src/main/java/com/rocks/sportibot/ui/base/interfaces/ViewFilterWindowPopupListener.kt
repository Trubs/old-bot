package com.rocks.sportibot.ui.base.interfaces

interface ViewFilterWindowPopupListener {

    fun showPopupFilterWindow()

    fun hidePopupFilterWindow()
}
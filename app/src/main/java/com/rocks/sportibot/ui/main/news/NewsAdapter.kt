package com.rocks.sportibot.ui.main.news

import android.view.ViewGroup
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.news.News
import com.rocks.sportibot.ui.base.clicklisteners.OnDataClickListener
import com.rocks.sportibot.ui.base.recycler_adapter.BaseRecyclerAdapter
import com.rocks.sportibot.ui.base.recycler_adapter.BaseRecyclerViewHolder
import com.rocks.sportibot.ui.base.recycler_adapter.inflateLayoutView
import com.rocks.sportibot.utils.extensions.*
import kotlinx.android.synthetic.main.item_news.view.*

class NewsAdapter : BaseRecyclerAdapter() {

    var onNewsCLickListener: OnDataClickListener<News>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRecyclerViewHolder {
        return NewsViewHolder(parent)
    }

    override fun onBindViewHolder(holder: BaseRecyclerViewHolder, position: Int) {
        (holder as NewsViewHolder).bind(getItemOfPosition(position))
    }

    inner class NewsViewHolder(parent: ViewGroup)
        : BaseRecyclerViewHolder(parent.inflateLayoutView(R.layout.item_news)) {

        fun bind(news: News) {
            itemView.let {
                it.timeAgoTextView.text = news.createdAt?.toDateTimeFormat()?.getTimeAgoFrom(context)
                it.labelTextView.text = news.title
                it.messageTextView.text = news.message
            }
            itemView.prevThumbImageView.let {
                if (news.image == null) {
                    it.gone()
                } else {
                    it.visible()
                    it.loadImageInto(news.image)
                }
            }
            itemView.setOnClickListener { onNewsCLickListener?.onDataClick(news) }
        }
    }
}
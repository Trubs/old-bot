package com.rocks.sportibot.ui.main.tipsters.settings

import com.rocks.sportibot.data.dto.tipster.filter.SortTipstersByEnum
import com.rocks.sportibot.data.dto.tipster.filter.TipsterStatusEnum
import com.rocks.sportibot.ui.base.interfaces.ViewExitListener
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract

interface FilterContract {
    interface View : BaseContract.View, ViewExitListener {

        fun setSortByChecked(sortTipstersByEnum: SortTipstersByEnum)

        fun setViewOnly(tipsterStatusEnum: TipsterStatusEnum)

        fun notifyOnFilterChangedListener()
    }

    interface Presenter : BaseContract.Presenter<View> {

        fun onSortByChanged(sortTipstersByEnum: SortTipstersByEnum)

        fun onViewOnlyChanged(tipsterStatusEnum: TipsterStatusEnum)

        fun applyAndClose()

    }
}
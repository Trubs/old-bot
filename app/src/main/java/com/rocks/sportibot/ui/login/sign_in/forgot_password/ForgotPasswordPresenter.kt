package com.rocks.sportibot.ui.login.sign_in.forgot_password

import com.rocks.sportibot.data.interactors.login.LoginInteractor
import com.rocks.sportibot.utils.extensions.manageProgressView
import com.rocks.sportibot.utils.extensions.scheduleIOToMainThread
import com.rocks.sportibot.utils.extensions.wrapViewSingle
import io.reactivex.disposables.CompositeDisposable

class ForgotPasswordPresenter constructor(
        private val loginInteractor: LoginInteractor,
        private val compositeDisposable: CompositeDisposable
) : ForgottPasswordContract.Presenter {

    override lateinit var view: ForgottPasswordContract.View

    override fun onRestorePressed(email: String?) {
        compositeDisposable.clear()

        compositeDisposable.add(loginInteractor
                .restorePassword(email)
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .subscribeWith(wrapViewSingle(view, {
                    view.exit()
                })))
    }
}
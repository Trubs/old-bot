package com.rocks.sportibot.ui.main.news

import com.rocks.sportibot.data.dto.news.News
import com.rocks.sportibot.data.interactors.news.NewsInteractor
import com.rocks.sportibot.utils.extensions.manageProgressView
import com.rocks.sportibot.utils.extensions.scheduleIOToMainThread
import com.rocks.sportibot.utils.extensions.wrapViewSingle
import io.reactivex.disposables.CompositeDisposable

class NewsPresenter(
        private val newsInteractor: NewsInteractor,
        private val compositeDisposable: CompositeDisposable
) : NewsContract.Presenter {

    override lateinit var view: NewsContract.View

    override fun onCreate() {
        super.onCreate()

        loadNews()
    }

    private fun loadNews() {
        compositeDisposable.add(newsInteractor
                .getNewsAsync()
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .subscribeWith(wrapViewSingle(view, {
                    view.setNews(it)
                })))
    }

    override fun onNewsPressed(news: News) {
        view.openNewsDetailsActivity(news)
    }

    override fun onRefresh() {
        loadNews()
    }
}
package com.rocks.sportibot.ui.login.sign_in.forgot_password

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rocks.sportibot.R
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.fragment.mvp.BaseMvpDialogFragment
import kotlinx.android.synthetic.main.dialog_forgot_password.*

class ForgottDialogFragment
@SuppressLint("ValidFragment")
private constructor()
    : BaseMvpDialogFragment<ForgottPasswordContract.Presenter>(), ForgottPasswordContract.View {

    companion object {
        private val TAG: String = ForgottDialogFragment::class.java.simpleName
        val KEY_STRING_EMAIL = "$TAG-KEY_STRING_EMAIL"

        fun newInstance(email: String?): ForgottDialogFragment {
            val bundle = Bundle()
            val fragment = ForgottDialogFragment()

            bundle.putString(KEY_STRING_EMAIL, email)

            fragment.arguments = bundle
            return fragment
        }
    }

    override fun injectPresenter() {
        ComponentProvider.Login.getLoginComponent().inject(this)
        presenter.view = this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_forgot_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog.setTitle(R.string.dialog_forgot_title)

        initViews()
        initListeners()
    }

    private fun initViews() {
        emailEditText.setText(arguments?.getString(KEY_STRING_EMAIL))
    }

    private fun initListeners() {
        restoreButton.setOnClickListener { presenter.onRestorePressed(emailEditText.getText()) }
    }
}
package com.rocks.sportibot.ui.main.tipsters.settings

import com.rocks.sportibot.data.dto.tipster.filter.SortTipstersByEnum
import com.rocks.sportibot.data.dto.tipster.filter.TipsterStatusEnum
import com.rocks.sportibot.data.interactors.tipster.TipsterInteractor

class FilterPresenter(
        private val tipsterInteractor: TipsterInteractor
) : FilterContract.Presenter {

    override lateinit var view: FilterContract.View

    private val filter = tipsterInteractor.getFilter()

    override fun onCreate() {
        super.onCreate()

        with(filter) {
            view.setViewOnly(filter.status)
            view.setSortByChecked(filter.sortBy)
        }
    }

    override fun onSortByChanged(sortTipstersByEnum: SortTipstersByEnum) {
        filter.sortBy = sortTipstersByEnum
    }

    override fun onViewOnlyChanged(tipsterStatusEnum: TipsterStatusEnum) {
        filter.status = tipsterStatusEnum
    }

    override fun applyAndClose() {
        if (tipsterInteractor.applyFilter(filter)) {
            view.notifyOnFilterChangedListener()
        }
        view.exit()
    }
}
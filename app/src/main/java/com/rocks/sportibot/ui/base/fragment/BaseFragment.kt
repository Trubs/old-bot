package com.rocks.sportibot.ui.base.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.rocks.sportibot.R
import com.rocks.sportibot.ui.base.dialogs.ProgressDialog
import com.rocks.sportibot.ui.base.interfaces.*
import com.rocks.sportibot.utils.extensions.gone
import com.rocks.sportibot.utils.extensions.showToastMessage
import com.rocks.sportibot.utils.extensions.visible

abstract class BaseFragment : Fragment(),
        OnNavigationBackListener,
        ViewProgressListener,
        ViewDialogProgressListener,
        ViewBlockProgressListener,
        ViewThrowableListener {

    protected abstract val layoutRes: Int
    protected var progressBar: ProgressBar? = null
    protected var swipeRefreshLayout: SwipeRefreshLayout? = null

    protected lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog = ProgressDialog(activity)
    }

    final override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar = view.findViewById(R.id.progressBar)
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
    }

    override fun onNavigationBackPressed() = false

    override fun showProgress() {
        progressBar.visible()
        swipeRefreshLayout?.isEnabled = false
    }

    override fun showDialogProgress(onCancelListener: (() -> Unit)) {
        progressDialog.show()
        progressDialog.setOnCancelListener { onCancelListener.invoke() }
    }

    override fun showBlockProgress() {
        progressDialog.setCancelable(false)
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()
    }

    override fun hideBlockProgress() {
        progressDialog.setCancelable(true)
        progressDialog.setCanceledOnTouchOutside(true)
        progressDialog.dismiss()
    }

    override fun hideDialogProgress() {
        progressDialog.dismiss()
    }

    override fun hideProgress() {
        progressBar.gone()
        swipeRefreshLayout?.isRefreshing = false
        swipeRefreshLayout?.isEnabled = true
    }

    override fun showError(message: String) {
        context.showToastMessage(message)
    }

    override fun showError(messageRes: Int) {
        context.showToastMessage(messageRes)
    }

}
package com.rocks.sportibot.ui.base.pager

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import com.rocks.sportibot.ui.base.fragment.fragment_pager.BaseFragmentPage
import java.io.Serializable

class TabFragmentDto private constructor(
        val title: String?,
        val drawable: Drawable?,
        val fragment: BaseFragmentPage
) : Serializable {
    companion object {
        fun createTab(context: Context, stringRes: Int, drawableRes: Int, fragment: BaseFragmentPage): TabFragmentDto {
            val title = if (stringRes == -1) null else context.getString(stringRes)
            val drawable = if (drawableRes == -1) null else ContextCompat.getDrawable(context, drawableRes)
            return TabFragmentDto(
                    title,
                    drawable,
                    fragment)
        }
    }
}
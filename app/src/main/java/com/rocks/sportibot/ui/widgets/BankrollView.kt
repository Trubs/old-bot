package com.rocks.sportibot.ui.widgets

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.Currency
import com.rocks.sportibot.ui.base.interfaces.ViewProgressListener
import com.rocks.sportibot.ui.base.recycler_adapter.inflateLayoutView
import com.rocks.sportibot.utils.extensions.gone
import com.rocks.sportibot.utils.extensions.visible
import kotlinx.android.synthetic.main.view_bankroll.view.*

class BankrollView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr),
        ViewProgressListener {
    init {
        this.inflateLayoutView(R.layout.view_bankroll, true)
    }

    override fun showProgress() {
        rangeTextView.setText(R.string.view_bankroll_loading)
    }

    override fun hideProgress() {
        rangeTextView.setText(R.string.view_bankroll_label)
    }

    fun setBankroll(value: Float?, currency: Currency) {
        if (value == null) {
            bankrollValueTextView.gone()
            return
        } else {

            bankrollValueTextView.setText(value, currency)

            bankrollValueTextView.visible()
        }
    }
}
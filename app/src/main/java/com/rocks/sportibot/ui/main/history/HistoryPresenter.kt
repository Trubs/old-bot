package com.rocks.sportibot.ui.main.history

import com.rocks.sportibot.data.interactors.history.HistoryInteractor
import com.rocks.sportibot.utils.extensions.log_i
import com.rocks.sportibot.utils.extensions.manageProgressView
import com.rocks.sportibot.utils.extensions.scheduleIOToMainThread
import com.rocks.sportibot.utils.extensions.wrapViewSingle
import io.reactivex.disposables.CompositeDisposable

class HistoryPresenter(
        private val historyInteractor: HistoryInteractor,
        private val compositeDisposable: CompositeDisposable
) : HistoryContract.Presenter {
    //todo filter implement
    override lateinit var view: HistoryContract.View

    /**
     * The maximum size of the list to be loaded during pagination
     * */
    private val LIMIT_LOAD_LIST = 20

    /**
     * offset position to load
     */
    private var offset = 0

    /**
     * List load indicator
     * */
    private var isLoading = false

    /**
     * End of list indicator
     * */
    private var isEndPage = false


    override fun onCreate() {
        super.onCreate()
        loadHistoryWithReset()
    }

    /**
     * Called when the data must be refreshed
     * */
    override fun onRefresh() {
        loadHistoryWithReset()
    }

    /**
     * Load next history
     * */
    override fun loadNext() {
        if (isLoading) {
            log_i("Already loading")
            return
        }
        if (isEndPage) {
            log_i("Don't start loading. End of the list.")
            return
        }
        compositeDisposable.add(historyInteractor
                .getHistoryList(offset, LIMIT_LOAD_LIST)
                .doOnSubscribe { isLoading = true }
                .doFinally { isLoading = false }
                .doOnSuccess { updateOffset(it.size) }
                .scheduleIOToMainThread()
                .doOnSubscribe { view.showPaginationProgress() }
                .subscribeWith(wrapViewSingle(view,
                        successFunction = {
                            view.hidePaginationProgress()
                            view.addHistory(it)
                        },
                        onErrorFunction = {
                            view.hidePaginationProgress()
                        })))
    }

    /**
     * Load from first page and clear offset
     * */
    private fun loadHistoryWithReset() {
        offset = 0
        isEndPage = false
        compositeDisposable.add(historyInteractor
                .getHistoryList(offset, LIMIT_LOAD_LIST)
                .doOnSubscribe { isLoading = true }
                .doFinally { isLoading = false }
                .doOnSuccess { updateOffset(it.size) }
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .subscribeWith(wrapViewSingle(view,
                        successFunction = {
                            view.setHistory(it)
                        })))
    }

    /**
     * @param size loaded list size
     * */
    private fun updateOffset(size: Int) {
        /*
        * add offset for next load
        * */
        offset += size

        /*
        * if the result is less than expected, this is the end of the list
        * */
        isEndPage = size < LIMIT_LOAD_LIST
    }

}
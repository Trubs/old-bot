package com.rocks.sportibot.ui.main.details

import com.rocks.sportibot.data.dto.Currency
import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.ui.base.interfaces.ViewExitListener
import com.rocks.sportibot.ui.base.interfaces.ViewProgressListener
import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract

interface BetDetailsContract {

    interface View : BaseContract.View,
            ViewThrowableListener,
            ViewProgressListener,
            ViewExitListener {

        /**
         * Set title of current screen
         * @param title - title of screen
         * */
        fun setTitle(title: String?)

        /**
         * Displaying bet details
         * @param bet - [Bet]
         * */
        fun setBetDetails(bet: Bet)

        /**
         * @hide
         * Set the limits of units for bet that can be entered
         * @param min - minimum number of units
         * @param max - maximum number of units
         * */
        @Deprecated("not relevant")
        fun setLimitUnits(min: Float, max: Float)

        /**
         * @hide
         * Set the limits of odds for bet that can be entered
         * @param min - minimum number of odds
         * @param max - maximum number of odds
         * */
        @Deprecated("not relevant")
        fun setLimitOdds(min: Float, max: Float)

        /**
         * Set the min of amount for bet that can be entered
         * @param min - minimum number of amount
         * @param currency - currency of the amount
         * */
        fun setMinAmount(min: Float, currency: Currency)

        /**
         * Show a message about successful bet
         * */
        fun showPlacedBetSuccessful()

        /**
         * Show a message about successful skip the bet
         * */
        fun showSkippedBetSuccessful()

    }

    interface Presenter : BaseContract.Presenter<View> {

        /**
         * Call when 'place bet' pressed
         * @param amountText - amountText of the bet
         * */
        fun onPlaceBetPressed(amountText: String)

        /**
         * Call when 'skip bet' pressed
         * */
        fun onPlaceSkipPressed()

    }
}
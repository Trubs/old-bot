package com.rocks.sportibot.ui.splash

import com.rocks.sportibot.data.interactors.splash.SplashInteractor
import com.rocks.sportibot.utils.extensions.scheduleIOToMainThread
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

class SplashPresenter(
        private val splashInteractor: SplashInteractor,
        private val compositeDisposable: CompositeDisposable
) : SplashContract.Presenter {

    override lateinit var view: SplashContract.View

    override fun onStart() {
        compositeDisposable.add(splashInteractor
                .tryLoadSession()
                .scheduleIOToMainThread()
                .subscribeBy(
                        onComplete = {
                            view.openMainActivity()
                        },
                        onError = {
                            it.printStackTrace()
                            view.openLoginSignInActivity()
                        }
                ))
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}
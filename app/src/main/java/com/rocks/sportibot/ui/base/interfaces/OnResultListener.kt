package com.rocks.sportibot.ui.base.interfaces

import android.app.Activity
import android.content.Intent

interface OnResultListener {
    companion object {
        val RESULT_OK = Activity.RESULT_OK
        val RESULT_CANCELED = Activity.RESULT_CANCELED
    }

    fun onResult(requestCode: Int, resultCode: Int, data: Intent?)
}
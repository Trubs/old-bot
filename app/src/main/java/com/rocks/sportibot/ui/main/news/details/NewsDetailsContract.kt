package com.rocks.sportibot.ui.main.news.details

import com.rocks.sportibot.ui.base.interfaces.OnNavigationBackListener
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract

interface NewsDetailsContract {
    interface View : BaseContract.View {

        fun onBackPressed()

    }

    interface Presenter : BaseContract.Presenter<View>,
            OnNavigationBackListener {

    }
}
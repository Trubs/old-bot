package com.rocks.sportibot.ui.base.fragment.mvp

import android.app.Dialog
import android.os.Bundle
import android.view.View
import com.rocks.sportibot.R
import com.rocks.sportibot.ui.base.fragment.BaseDialogFragment
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract
import javax.inject.Inject

abstract class BaseMvpDialogFragment<P : BaseContract.Presenter<*>> : BaseDialogFragment() {

    @Inject
    protected lateinit var presenter: P

    protected abstract fun injectPresenter()

    @Deprecated("")
    final override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectPresenter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progressBar = view.findViewById(R.id.progressBar)

        presenter.onCreate()
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

}
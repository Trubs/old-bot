package com.rocks.sportibot.ui.base.dialogs

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import com.rocks.sportibot.R
import kotlinx.android.synthetic.main.dialog_progress.view.*

class ProgressDialog(
        context: Context?,
        cancelable: Boolean = false,
        cancelListener: DialogInterface.OnCancelListener? = null
) : Dialog(context, R.style.DialogTheme) {

    val view = layoutInflater.inflate(R.layout.dialog_progress, null)

    init {

        setContentView(view)

        setMessage(null)

        this.setCanceledOnTouchOutside(cancelable)
        cancelListener?.let { this.setOnCancelListener(it) }

    }

    fun setMessage(message: String?) {
        view.messageTextView.text = message ?: context?.getString(R.string.dialog_label_processing)
    }
}
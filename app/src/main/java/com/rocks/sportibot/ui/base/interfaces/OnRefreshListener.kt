package com.rocks.sportibot.ui.base.interfaces

/**
 * Listener of the refresh data
 * */
interface OnRefreshListener {

    /**
     * Called when the data must be refreshed
     * */
    fun onRefresh()
}
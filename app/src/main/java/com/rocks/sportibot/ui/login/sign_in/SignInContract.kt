package com.rocks.sportibot.ui.login.sign_in

import com.rocks.sportibot.ui.base.interfaces.ViewProgressListener
import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract

interface SignInContract {
    interface View : BaseContract.View,
            ViewProgressListener,
            ViewThrowableListener {

        /**
         * Show dialog [com.rocks.sportibot.ui.login.sign_in.forgot_password.ForgottDialogFragment]
         * */
        fun showDialogForgotPassword(email: String)

        /**
         * Opening [com.rocks.sportibot.ui.main.MainActivity]
         * */
        fun openMainActivity()

        /**
         * Opening [com.rocks.sportibot.ui.login.sign_up.SignUpActivity]
         * */
        fun openRegisterActivity()

    }

    interface Presenter : BaseContract.Presenter<View> {

        fun onRegisterPressed()

        fun onSignPressed(signInDto: SignInDto)

        fun onForgotPasswordPressed(email: String)
    }
}
package com.rocks.sportibot.ui.base.pager

import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter


class FragmentPagerAdapter(
        private val tabLayout: TabLayout,
        private val tabs: List<TabFragmentDto>,
        fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    init {
        tabs.forEach {
            tabLayout.addTab(tabLayout.newTab())
        }
    }

    override fun getItem(position: Int): Fragment {
        return tabs[position].fragment
    }

    override fun getCount(): Int = tabs.size

    fun invalidateTabs() {
        tabs.forEach {
            val tab = tabLayout.getTabAt(tabs.indexOf(it))!!

            tab.icon = it.drawable
            tab.text = it.title
        }
    }

}


package com.rocks.sportibot.ui.main.history

import android.view.ViewGroup
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.history.History
import com.rocks.sportibot.ui.base.interfaces.ViewPaginationProgressListener
import com.rocks.sportibot.ui.base.recycler_adapter.BaseRecyclerAdapter
import com.rocks.sportibot.ui.base.recycler_adapter.BaseRecyclerViewHolder
import com.rocks.sportibot.ui.base.recycler_adapter.inflateLayoutView
import com.rocks.sportibot.utils.extensions.log_e
import com.rocks.sportibot.utils.extensions.toDateTimeFormat
import com.rocks.sportibot.utils.extensions.toDisplayTimeFormat
import kotlinx.android.synthetic.main.item_history.view.*

class HistoryAdapter : BaseRecyclerAdapter(), ViewPaginationProgressListener {
    private val PROGRESS_ITEM = "1"

    override fun getItemViewType(position: Int): Int {
        val viewType = if (getItemOfPosition<Any>(position) is String) ViewType.PROGRESS
        else ViewType.HISTORY

        return viewType.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRecyclerViewHolder =
            when (viewType) {
                ViewType.PROGRESS.ordinal -> ProgressViewHolder(parent)
                else -> HistoryViewHolder(parent)
            }

    override fun onBindViewHolder(holder: BaseRecyclerViewHolder, position: Int) {
        if (holder is HistoryViewHolder) {
            holder.bind(getItemOfPosition(position))
        }
    }

    override fun showPaginationProgress() {
        val indexOfProgress = items.indexOf(PROGRESS_ITEM)

        if (indexOfProgress == -1) {
            addItem(PROGRESS_ITEM)
        } else {
            log_e("progress already shown")
        }


    }

    override fun hidePaginationProgress() {
        val indexOfProgress = items.indexOf(PROGRESS_ITEM)

        if (indexOfProgress == -1) {
            log_e("progress does not exist in the list")
            return
        }

        items.removeAt(indexOfProgress)
        notifyItemRemoved(indexOfProgress)
    }

    class HistoryViewHolder(parent: ViewGroup) : BaseRecyclerViewHolder(
            parent.inflateLayoutView(R.layout.item_history)) {

        fun bind(history: History) {
            with(itemView) {
                labelTextView.text = history.title
                timeTextView.text = history.createdAt?.toDateTimeFormat()?.toDisplayTimeFormat()
                typeTextView.text = history.type?.name
            }
        }
    }

    class ProgressViewHolder(parent: ViewGroup) : BaseRecyclerViewHolder(
            parent.inflateLayoutView(R.layout.item_list_progress_bar))
}

enum class ViewType {
    PROGRESS,
    HISTORY
}
package com.rocks.sportibot.ui.main.news.details

import com.rocks.sportibot.data.interactors.news.NewsInteractor
import io.reactivex.disposables.CompositeDisposable

class NewsDetailsPresenter(
        private val newsInteractor: NewsInteractor,
        private val compositeDisposable: CompositeDisposable
) : NewsDetailsContract.Presenter {

    override lateinit var view: NewsDetailsContract.View

    override fun onNavigationBackPressed(): Boolean {
        view.onBackPressed()
        return true
    }
}
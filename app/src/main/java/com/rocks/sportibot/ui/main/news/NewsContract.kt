package com.rocks.sportibot.ui.main.news

import com.rocks.sportibot.data.dto.news.News
import com.rocks.sportibot.ui.base.interfaces.OnRefreshListener
import com.rocks.sportibot.ui.base.interfaces.ViewProgressListener
import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract

interface NewsContract {

    interface View : BaseContract.View,
            ViewProgressListener,
            ViewThrowableListener {

        fun setNews(list: List<News>)

        fun openNewsDetailsActivity(news: News)
    }

    interface Presenter : BaseContract.Presenter<View>,
            OnRefreshListener {

        fun onNewsPressed(news: News)
    }
}
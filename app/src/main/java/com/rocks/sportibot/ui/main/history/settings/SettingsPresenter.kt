package com.rocks.sportibot.ui.main.history.settings

import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.history.filter.Filter
import com.rocks.sportibot.data.dto.history.filter.ShowBetsHistoryEnum
import com.rocks.sportibot.data.dto.history.filter.SortHistoryEnum
import com.rocks.sportibot.data.interactors.history.HistoryInteractor
import com.rocks.sportibot.utils.extensions.afterIgnoreTime
import com.rocks.sportibot.utils.extensions.beforeIgnoreTime
import com.rocks.sportibot.utils.extensions.getCalendarFromArgs
import java.util.*

class SettingsPresenter(
        private val historyInteractor: HistoryInteractor
) : SettingsContract.Presenter {

    override lateinit var view: SettingsContract.View

    private val filter: Filter = historyInteractor.getFilter()

    override fun onCreate() {
        super.onCreate()
        with(filter) {
            view.setShowBetsLabel(showBetsHistoryEnum)
            view.setDateFromLabel(getDisplayDateFrom())
            view.setDateUntilLabel(getDisplayDateUntil())
            view.setSortByLabel(sortHistoryEnum)
        }
    }

    override fun onShowBetsChanged(showBetsHistoryEnum: ShowBetsHistoryEnum) {
        filter.showBetsHistoryEnum = showBetsHistoryEnum
    }

    override fun onDateFromChanged(year: Int, month: Int, day: Int) {
        val dateFrom: Date = getCalendarFromArgs(year, month, day).time

        if (dateFrom.afterIgnoreTime(filter.dateUntil)) {
            view.showError(R.string.fragment_history_settings_error_date_start_after_end)
            return
        }

        filter.dateFrom = dateFrom
        view.setDateFromLabel(filter.getDisplayDateFrom())
    }

    override fun onDateUntilChanged(year: Int, month: Int, day: Int) {
        val dateUntil: Date = getCalendarFromArgs(year, month, day).time

        if (dateUntil.beforeIgnoreTime(filter.dateFrom)) {
            view.showError(R.string.fragment_history_settings_error_date_end_before_start)
            return
        }

        filter.dateUntil = dateUntil
        view.setDateUntilLabel(filter.getDisplayDateUntil())
    }

    override fun onSortByChanged(sortHistoryEnum: SortHistoryEnum) {
        filter.sortHistoryEnum = sortHistoryEnum
    }

    override fun onDateFromPressed() {
        view.showSelectDateFromDialog(filter.dateFrom, null, filter.dateUntil)
    }

    override fun onDateUntilPressed() {
        view.showSelectDateUntilDialog(filter.dateUntil, filter.dateFrom, null)
    }

    override fun onStop() {
        super.onStop()
        if (historyInteractor.applyFilter(filter)) {
            view.notifyFilterChanged()
        }
    }

}
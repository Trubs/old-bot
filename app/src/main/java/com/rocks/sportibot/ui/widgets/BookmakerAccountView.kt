package com.rocks.sportibot.ui.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.rocks.sportibot.R
import kotlinx.android.synthetic.main.view_button_account.view.*

class BookmakerAccountView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_button_account, this)

        val typedArray = context.theme.obtainStyledAttributes(attrs,
                R.styleable.BookmakerAccountView,
                0,
                0)
        try {
            setBookmaker(typedArray.getString(R.styleable.BookmakerAccountView_bookmaker))
            setLogin(typedArray.getString(R.styleable.BookmakerAccountView_login))
        } finally {
            typedArray.recycle()
        }

    }

    fun setBookmaker(bookmaker: String) {
        bookmakerTextView.text = bookmaker
    }

    fun setLogin(login: String) {
        loginTextView.text = login
    }

}
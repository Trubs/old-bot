package com.rocks.sportibot.ui.base.interfaces.tipster

import com.rocks.sportibot.data.dto.tipster.Tipster

interface OnTipsterClickListener {
    fun onTipsterPressed(tipster: Tipster)
}
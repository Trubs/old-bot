package com.rocks.sportibot.ui.main.news

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.news.News
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.clicklisteners.OnDataClickListener
import com.rocks.sportibot.ui.base.fragment.mvp.BaseMvpFragment
import com.rocks.sportibot.ui.base.recycler_adapter.DrawableDividerDecorator
import com.rocks.sportibot.ui.main.news.details.NewsDetailsActivity
import kotlinx.android.synthetic.main.fragment_news.*

class NewsFragment : BaseMvpFragment<NewsContract.Presenter>(), NewsContract.View {
    override val layoutRes: Int = R.layout.fragment_news

    override fun injectPresenter() {
        ComponentProvider.Main.getNewsComponent().inject(this)
        presenter.view = this
    }

    private val adapter = NewsAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setTitle(R.string.title_news)
        initListeners()
        initRecyclerView()
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener { presenter.onRefresh() }
        adapter.onNewsCLickListener = object : OnDataClickListener<News> {
            override fun onDataClick(data: News) {
                presenter.onNewsPressed(data)
            }
        }
    }

    private fun initRecyclerView() {
        newsRecyclerView.layoutManager = LinearLayoutManager(activity)
        newsRecyclerView.addItemDecoration(DrawableDividerDecorator(activity!!, R.drawable.divider_gray))
        newsRecyclerView.adapter = adapter
    }

    override fun setNews(list: List<News>) {
        adapter.setData(list)
    }

    override fun openNewsDetailsActivity(news: News) {
        startActivity(NewsDetailsActivity.getIntent(activity!!, news))
    }
}
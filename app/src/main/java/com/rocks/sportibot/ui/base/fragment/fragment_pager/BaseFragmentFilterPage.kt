package com.rocks.sportibot.ui.base.fragment.fragment_pager

import com.rocks.sportibot.ui.base.interfaces.OnFilterChangeListener

/**
 * Fragment for the ViewPager with lifecycle [OnFragmentPagerLifecycle]
 * */
abstract class BaseFragmentFilterPage : BaseFragmentPage(), OnFilterChangeListener
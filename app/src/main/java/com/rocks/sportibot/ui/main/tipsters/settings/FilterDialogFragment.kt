package com.rocks.sportibot.ui.main.tipsters.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.tipster.filter.SortTipstersByEnum
import com.rocks.sportibot.data.dto.tipster.filter.TipsterStatusEnum
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.fragment.mvp.BaseMvpDialogFragment
import kotlinx.android.synthetic.main.fragment_tipsters_settings.*

class FilterDialogFragment : BaseMvpDialogFragment<FilterContract.Presenter>(), FilterContract.View {


    override fun injectPresenter() {
        ComponentProvider.Main.getTipsterComponent().inject(this)
        presenter.view = this
    }

    var onFilterChangeListener: (() -> Unit)? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        initListeners()
    }

    private fun initToolbar() {
        toolbar.inflateMenu(R.menu.menu_filter_apply)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tipsters_settings, container, false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_action_apply -> {
                presenter.applyAndClose()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initListeners() {
        toolbar.setNavigationOnClickListener { exit() }
        toolbar.setOnMenuItemClickListener { onOptionsItemSelected(it) }
        sortByRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.betsPlayedRadioButton -> presenter.onSortByChanged(SortTipstersByEnum.BETS_PLAYED)
                R.id.yieldRadioButton -> presenter.onSortByChanged(SortTipstersByEnum.YIELD)
            }
        }
        viewOnlyRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.allRadioButton -> presenter.onViewOnlyChanged(TipsterStatusEnum.ALL)
                R.id.freeRadioButton -> presenter.onViewOnlyChanged(TipsterStatusEnum.FREE)
                R.id.premiumRadioButton -> presenter.onViewOnlyChanged(TipsterStatusEnum.PREMIUM)
            }
        }
    }

    override fun setSortByChecked(sortTipstersByEnum: SortTipstersByEnum) {
        sortByRadioGroup.check(when (sortTipstersByEnum) {
            SortTipstersByEnum.BETS_PLAYED -> R.id.betsPlayedRadioButton
            SortTipstersByEnum.YIELD -> R.id.yieldRadioButton
        })
    }

    override fun setViewOnly(tipsterStatusEnum: TipsterStatusEnum) {
        viewOnlyRadioGroup.check(when (tipsterStatusEnum) {
            TipsterStatusEnum.ALL -> R.id.allRadioButton
            TipsterStatusEnum.FREE -> R.id.freeRadioButton
            TipsterStatusEnum.PREMIUM -> R.id.premiumRadioButton
        })
    }

    override fun notifyOnFilterChangedListener() {
        onFilterChangeListener?.invoke()
    }

}
package com.rocks.sportibot.ui.splash

import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract

interface SplashContract {
    interface View : BaseContract.View {

        fun openMainActivity()

        fun openLoginSignInActivity()

    }

    interface Presenter : BaseContract.Presenter<View> {

    }

}
package com.rocks.sportibot.ui.base.recycler_adapter

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View


class DrawableDividerDecorator(
        context: Context,
        @DrawableRes res: Int,
        private val startPosition: Int = 0
) : RecyclerView.ItemDecoration() {

    private val divider: Drawable = ContextCompat.getDrawable(context, res)!!

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State?) {
        canvas.save()
        var left = 0
        var right = 0

        if (parent.clipToPadding) {
            left = parent.paddingLeft
            right = parent.width - parent.paddingRight
            canvas.clipRect(left, parent.paddingTop, right,
                    parent.height - parent.paddingBottom)
        } else {
            right = parent.width
        }

        val childCount = parent.childCount
        for (i in 0 until childCount) {

            if (i < startPosition) continue

            val child = parent.getChildAt(i)

            val bottom = child.bottom

            val top = bottom - divider.intrinsicHeight

            divider.setBounds(left, top, right, bottom)
            divider.draw(canvas)
        }
        canvas.restore()
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                state: RecyclerView.State?) {
        outRect.set(0, 0, 0, divider.intrinsicHeight)
    }

    override fun getItemOffsets(outRect: Rect?, itemPosition: Int, parent: RecyclerView?) {
        outRect?.set(0, 0, 0, divider.intrinsicHeight)
    }

}
package com.rocks.sportibot.ui.base.activities.mvp

import android.os.Bundle
import com.rocks.sportibot.ui.base.activities.BaseActivity
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract
import javax.inject.Inject

abstract class BaseMvpActivity<P : BaseContract.Presenter<*>> : BaseActivity() {

    @Inject
    protected lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter.onCreate()
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }
}

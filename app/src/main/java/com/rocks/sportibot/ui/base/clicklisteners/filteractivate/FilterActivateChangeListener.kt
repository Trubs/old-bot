package com.rocks.sportibot.ui.base.clicklisteners.filteractivate

import android.view.View

/**
 * Toggle isActivate
 * */
class FilterActivateChangeListener(
        private val onFilterActivateClickListener: OnFilterActivateClickListener
) : View.OnClickListener {

    /**
     * Toggle and notify listener
     * */
    override fun onClick(v: View?) {
        if (v == null) return
        v.isActivated = !v.isActivated
        onFilterActivateClickListener.onFilterActivateChanged(v.isActivated)
    }
}
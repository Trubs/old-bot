package com.rocks.sportibot.ui.main.details

import android.app.DialogFragment
import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.Currency
import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.dialogs.ProgressDialog
import com.rocks.sportibot.ui.base.fragment.mvp.BaseMvpDialogFragment
import com.rocks.sportibot.utils.extensions.addTextChangeEmptyListener
import com.rocks.sportibot.utils.extensions.getTextString
import com.rocks.sportibot.utils.extensions.showToastMessage
import kotlinx.android.synthetic.main.dialog_bet_details.*

class BetDetailsDialogFragment @Deprecated("use newInstance()") constructor()
    : BaseMvpDialogFragment<BetDetailsContract.Presenter>(), BetDetailsContract.View {

    private val progressDialog by lazy { ProgressDialog(context, cancelable = false) }

    companion object {
        private val TAG: String = BetDetailsDialogFragment::class.java.simpleName
        private val EXTRA_BET = "$TAG-EXTRA_BET"

        @Suppress("DEPRECATION")
        fun newInstance(bet: Bet): BetDetailsDialogFragment {
            val betDetailsDialogFragment = BetDetailsDialogFragment()
            val bundle = Bundle()
            bundle.putSerializable(EXTRA_BET, bet)
            betDetailsDialogFragment.arguments = bundle
            return betDetailsDialogFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogTheme_Light)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_bet_details, container, false)
    }

    override fun injectPresenter() {
        ComponentProvider.Main
                .getBetDetailsComponent(arguments!!.getSerializable(EXTRA_BET) as Bet)
                .inject(this)
        presenter.view = this
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() {
        toolbar.setNavigationOnClickListener { exit() }
        placeBetButton.setOnClickListener { presenter.onPlaceBetPressed(amountEditText.getTextString()) }
        skipBetButton.setOnClickListener { presenter.onPlaceSkipPressed() }
        amountEditText.addTextChangeEmptyListener { isNullOrBlank -> placeBetButton.isEnabled = !isNullOrBlank }
    }

    /**
     * Set title of current screen
     * */
    override fun setTitle(title: String?) {
        toolbar.title = title
    }

    /**
     * Displaying bet details
     * @param bet - [Bet]
     * */
    override fun setBetDetails(bet: Bet) {

        betOpenView.bet = bet

    }

    /**
     * Show a message about successful bet
     * */
    override fun showPlacedBetSuccessful() {
        context.showToastMessage(R.string.dialog_bet_details_placed_successful)
    }

    /**
     * Show a message about successful skip the bet
     * */
    override fun showSkippedBetSuccessful() {
        context.showToastMessage(R.string.dialog_bet_details_skipped_successful)
    }

    /**
     * Set the limits of units for bet that can be entered
     * @param min - minimum number of units
     * @param max - maximum number of units
     * */
    override fun setLimitUnits(min: Float, max: Float) {
        //minUnitsTextView.text = min.toString()
        //maxUnitsTextView.text = max.toString()
    }

    /**
     * Set the limits of odds for bet that can be entered
     * @param min - minimum number of odds
     * @param max - maximum number of odds
     * */
    override fun setLimitOdds(min: Float, max: Float) {
        //minOddsTextView.text = min.toString()
        //maxOddsTextView.text = max.toString()
    }

    /**
     * Set the min of amount for bet that can be entered
     * @param min - minimum number of amount
     * @param currency - currency of the amount
     * */
    override fun setMinAmount(min: Float, currency: Currency) {
        minAmountTextView.text = getString(R.string.dialog_bet_details_label_min_,
                min.toString(),
                currency.getSymbol())
    }

    override fun showProgress() {
        progressDialog.show()
    }

    override fun hideProgress() {
        progressDialog.dismiss()
    }

    override fun showError(message: String) {
        showAlert(getString(R.string.dialog_bet_details_label_error),
                message)
    }

    override fun showError(messageRes: Int) {
        showAlert(getString(R.string.dialog_bet_details_label_error),
                getString(messageRes))
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        ComponentProvider.Main.clearBetDetailsComponent()
    }

    private fun showAlert(title: String,
                          message: String) {
        AlertDialog.Builder(context!!)
                .setCancelable(true)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.dialog_ok, { dialog, _ -> dialog.dismiss() })
                .show()
    }

}
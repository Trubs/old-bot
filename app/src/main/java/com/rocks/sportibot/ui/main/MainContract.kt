package com.rocks.sportibot.ui.main

import com.rocks.sportibot.data.dto.Currency
import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract

interface MainContract {

    interface View : BaseContract.View,
            ViewThrowableListener {
        fun bankrollShowProgress()

        fun bankrollHideProgress()

        fun bankrollSetValue(value: Float?, currency: Currency)

        fun exit()
    }

    interface Presenter : BaseContract.Presenter<View> {

        fun onBackrollPressed()
        fun onLiveHelpPressed()
        fun onBackPressed()
    }
}
package com.rocks.sportibot.ui.base.fragment.fragment_pager

import com.rocks.sportibot.ui.base.fragment.BaseFragment
import com.rocks.sportibot.ui.base.interfaces.ViewProgressListener
import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import com.rocks.sportibot.utils.extensions.showToastMessage

/**
 * Fragment for the ViewPager with lifecycle [OnFragmentPagerLifecycle]
 * */
abstract class BaseFragmentPage : BaseFragment(),
        OnFragmentPagerLifecycle,
        ViewProgressListener,
        ViewThrowableListener {

    override fun onShownPage() {
        super.onShownPage()
    }

    override fun onHiddenPage() {
        super.onHiddenPage()
    }

    override fun showError(message: String) {
        context.showToastMessage(message)
    }
}
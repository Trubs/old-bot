package com.rocks.sportibot.ui.main.tipsters.adapter

import android.view.ViewGroup
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.ui.base.recycler_adapter.inflateLayoutView
import com.rocks.sportibot.utils.extensions.formatDecimalAfterPoint2
import com.rocks.sportibot.utils.extensions.getColorInt
import kotlinx.android.synthetic.main.item_tipsters_my.view.*

class TipsterMyViewHolder(parent: ViewGroup) : BaseTipsterViewHolder(parent.inflateLayoutView(R.layout.item_tipsters_my)) {

    override fun bind(tipster: Tipster) {
        with(itemView) {
            //TODO image
            //circleImageView.loadImageInto(tipster.image, R.drawable.tabbar_icon_tipsters)
            nameTextView.text = tipster.user?.username

            //TODO rating
            //ratingBar.rating = tipster.rating

            maxMoneyTextView.text = context.getString(R.string.fragment_tipsters_value_max_money_,
                    tipster.relation?.limit?.formatDecimalAfterPoint2() ?: 0)

            autobetTextView.isActivated = tipster.isAutobet()

            apiNameTextView.text = tipster.relation?.apiName

            stakeTextView.text = tipster.relation?.stake?.formatDecimalAfterPoint2()

            typeTextView.text = tipster.relation?.type

            if (tipster.relation?.alert != null) {
                alertTextView.text = tipster.relation?.alert
            } else {
                alertTextView.text = getString(R.string.fragment_tipsters_label_alert_off)
                alertTextView.setTextColor(context.getColorInt(R.color.gray_light))
            }

            unfollowButton.setOnClickListener { onUnFollowTipsterClickListener?.invoke(tipster, unfollowButton) }
        }

        itemView.setOnClickListener { onTipsterClickListener?.onTipsterPressed(tipster) }
    }
}
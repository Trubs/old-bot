package com.rocks.sportibot.ui.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import com.rocks.sportibot.R
import com.rocks.sportibot.utils.extensions.getTextString
import kotlinx.android.synthetic.main.view_login_edit_text.view.*

class LoginEditText @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_login_edit_text, this)

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.LoginEditText)

            editText.inputType = typedArray.getInt(R.styleable.LoginEditText_android_inputType, EditorInfo.TYPE_TEXT_FLAG_NO_SUGGESTIONS)
            editText.setText(typedArray.getText(R.styleable.LoginEditText_android_text))
            rangeTextView.text = typedArray.getText(R.styleable.LoginEditText_android_hint)

            try {

            } finally {
                typedArray.recycle()
            }
        }

        editText.setOnFocusChangeListener { _, hasFocus ->
            rangeTextView.isActivated = hasFocus
        }
    }

    fun getText(): String = editText.getTextString()

    fun setText(text: String?) {
        editText.setText(text)
    }
}
package com.rocks.sportibot.ui.login.sign_in

import com.rocks.sportibot.data.interactors.login.LoginInteractor
import com.rocks.sportibot.utils.extensions.manageProgressView
import com.rocks.sportibot.utils.extensions.scheduleIOToMainThread
import com.rocks.sportibot.utils.extensions.wrapViewSingle
import io.reactivex.disposables.CompositeDisposable

class SignInPresenter(
        private val loginInteractor: LoginInteractor,
        private val compositeDisposable: CompositeDisposable
) : SignInContract.Presenter {

    override lateinit var view: SignInContract.View

    override fun onSignPressed(signInDto: SignInDto) {

        compositeDisposable.clear()

        compositeDisposable.add(loginInteractor
                .singIn(signInDto)
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .subscribeWith(wrapViewSingle(view, {
                    view.openMainActivity()
                })))
    }

    override fun onRegisterPressed() {
        view.openRegisterActivity()
    }

    override fun onForgotPasswordPressed(email: String) {
        view.showDialogForgotPassword(email)
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}
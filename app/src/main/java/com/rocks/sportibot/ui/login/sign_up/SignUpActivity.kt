package com.rocks.sportibot.ui.login.sign_up

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import com.rocks.sportibot.R
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.activities.mvp.BaseMvpActivity
import com.rocks.sportibot.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.include_check_box_over18.*
import kotlinx.android.synthetic.main.include_check_box_terms.*

class SignUpActivity : BaseMvpActivity<SignUpContract.Presenter>(), SignUpContract.View {

    override fun layoutRes(): Int = R.layout.activity_sign_up

    override fun injectPresenter() {
        ComponentProvider.Login.getLoginComponent().inject(this)
        presenter.view = this
    }

    companion object {
        fun getIntentClearTask(context: Context): Intent {
            return Intent(context, SignUpActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                            or Intent.FLAG_ACTIVITY_CLEAR_TOP
                            or Intent.FLAG_ACTIVITY_NEW_TASK)
        }

        fun getIntent(context: Context): Intent {
            return Intent(context, SignUpActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initListeners()
    }

    override fun initViews() {
        super.initViews()
        termsTextView.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun initListeners() {
        signUpButton.setOnClickListener {
            presenter.onSignUpPressed(SignUpDto(
                    emailEditText.getText(),
                    usernameEditText.getText(),
                    passwordEditText.getText(),
                    confirmPasswordEditText.getText(),
                    ageCheckBox.isChecked,
                    termsCheckBox.isChecked
            ))
        }

        loginButton.setOnClickListener { presenter.onLoginPressed() }
    }

    override fun openMainActivity() {
        startActivity(MainActivity.getIntentClearTask(this))
    }

    override fun openLoginActivity() {
        onBackPressed()
    }
}
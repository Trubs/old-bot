package com.rocks.sportibot.ui.main.news.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.news.News
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.activities.mvp.BaseMvpActivity

class NewsDetailsActivity : BaseMvpActivity<NewsDetailsContract.Presenter>(), NewsDetailsContract.View {
    override fun layoutRes(): Int = R.layout.activity_news_details

    private val news: News by lazy { intent.getSerializableExtra(EXTRA_NEWS) as News }

    override fun injectPresenter() {
        ComponentProvider.Main.getNewsComponent().inject(this)
        presenter.view = this
    }

    companion object {
        val TAG: String = NewsDetailsActivity::class.java.simpleName
        val EXTRA_NEWS = "$TAG-EXTRA_NEWS"

        fun getIntent(context: Context, news: News): Intent {
            return Intent(context, NewsDetailsActivity::class.java)
                    .putExtra(EXTRA_NEWS, news)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun initToolbar() {
        super.initToolbar()
        toolbar?.title = news.title
    }
}
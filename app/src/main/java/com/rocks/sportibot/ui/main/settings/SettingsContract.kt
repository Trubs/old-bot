package com.rocks.sportibot.ui.main.settings

import com.rocks.sportibot.ui.base.interfaces.OnRefreshListener
import com.rocks.sportibot.ui.base.interfaces.ViewBlockProgressListener
import com.rocks.sportibot.ui.base.interfaces.ViewProgressListener
import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract

interface SettingsContract {

    interface View : BaseContract.View,
            ViewProgressListener,
            ViewBlockProgressListener,
            ViewThrowableListener {

        fun openLoginActivity()

        fun showApiUpdatedSuccessful()

        fun clearUserApiFields()
    }

    interface Presenter : BaseContract.Presenter<View>,
            OnRefreshListener {

        fun updateApiPressed(username: String?,
                             password: String?)

        fun onLogoutPressed()
    }

}
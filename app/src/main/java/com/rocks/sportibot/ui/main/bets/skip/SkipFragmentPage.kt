package com.rocks.sportibot.ui.main.bets.skip

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.fragment.fragment_pager.mvp.BaseMvpFragmentPage
import com.rocks.sportibot.ui.base.recycler_adapter.DrawableDividerDecorator
import com.rocks.sportibot.ui.base.recycler_adapter.VerticalSpaceItemDecoration
import com.rocks.sportibot.ui.main.bets.BetAdapter
import kotlinx.android.synthetic.main.fragment_my_bets_open.*

class SkipFragmentPage : BaseMvpFragmentPage<SkipContract.Presenter>(), SkipContract.View {
    override val layoutRes: Int = R.layout.fragment_my_bets_skip

    private val adapter = BetAdapter()

    override fun injectPresenter() {
        ComponentProvider.Main.getMyBetsComponent().inject(this)
        presenter.view = this
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        initRecyclerView()
    }

    private fun initListeners() {
        swipeRefreshLayout!!.setOnRefreshListener { presenter.onRefresh() }
        //TODO("adapter listener")
    }

    private fun initRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(context)

        recyclerView.addItemDecoration(VerticalSpaceItemDecoration(
                resources.getDimensionPixelSize(R.dimen.margin_small),
                DrawableDividerDecorator(activity!!, R.drawable.divider_gray)))

        recyclerView.adapter = adapter
    }

    override fun setBetList(bets: List<Bet>) {
        adapter.setData(bets)
    }

    override fun addBet(bet: Bet) {
        adapter.addItem(bet)
    }
}
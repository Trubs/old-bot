package com.rocks.sportibot.ui.base.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.widget.ProgressBar
import com.rocks.sportibot.ui.base.interfaces.ViewExitListener
import com.rocks.sportibot.ui.base.interfaces.ViewProgressListener
import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import com.rocks.sportibot.utils.extensions.gone
import com.rocks.sportibot.utils.extensions.showToastMessage
import com.rocks.sportibot.utils.extensions.visible

open class BaseDialogFragment : DialogFragment(),
        ViewProgressListener,
        ViewThrowableListener,
        ViewExitListener {

    protected var progressBar: ProgressBar? = null

    var onCloseListener: (() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun showProgress() {
        progressBar.visible()
    }

    override fun hideProgress() {
        progressBar.gone()
    }

    override fun showError(message: String) {
        context.showToastMessage(message)
    }

    override fun showError(messageRes: Int) {
        context.showToastMessage(messageRes)
    }

    override fun exit() {
        dismissAllowingStateLoss()
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        onCloseListener?.invoke()
    }
}
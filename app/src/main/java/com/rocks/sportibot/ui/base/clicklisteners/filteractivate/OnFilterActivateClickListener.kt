package com.rocks.sportibot.ui.base.clicklisteners.filteractivate

interface OnFilterActivateClickListener {
    fun onFilterActivateChanged(isActivated: Boolean)
}
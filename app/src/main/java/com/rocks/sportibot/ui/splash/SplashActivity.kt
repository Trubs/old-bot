package com.rocks.sportibot.ui.splash

import com.rocks.sportibot.R
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.activities.mvp.BaseMvpActivity
import com.rocks.sportibot.ui.login.sign_in.SignInActivity
import com.rocks.sportibot.ui.main.MainActivity

class SplashActivity : BaseMvpActivity<SplashContract.Presenter>(), SplashContract.View {
    override fun layoutRes(): Int = R.layout.activity_splash

    override fun injectPresenter() {
        ComponentProvider.Splash.getSplashComponent().inject(this)
        presenter.view = this
    }

    override fun openMainActivity() {
        startActivity(MainActivity.getIntentClearTask(this))
    }

    override fun openLoginSignInActivity() {
        startActivity(SignInActivity.getIntentClearTask(this))
    }
}
package com.rocks.sportibot.ui.base.interfaces.mvp

import com.rocks.sportibot.utils.extensions.log_i
import com.rocks.sportibot.utils.extensions.tag

interface BaseContract {
    interface View

    interface Presenter<V : View> {
        var view: V

        /**
         * Call from view impl
         * */
        fun onCreate() {
            log_i(tag(), "onCreate()")
        }

        /**
         * Call from view impl
         * */
        fun onStart() {
            log_i(tag(), "onStart()")
        }

        /**
         * Call from view impl
         * */
        fun onResume() {
            log_i(tag(), "onResume()")
        }

        /**
         * Call from view impl
         * */
        fun onStop() {
            log_i(tag(), "onStop()")
        }
    }
}
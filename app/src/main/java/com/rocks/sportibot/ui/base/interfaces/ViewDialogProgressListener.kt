package com.rocks.sportibot.ui.base.interfaces

interface ViewDialogProgressListener {

    fun showDialogProgress(onCancelListener: (() -> Unit) = {})

    fun hideDialogProgress()

}
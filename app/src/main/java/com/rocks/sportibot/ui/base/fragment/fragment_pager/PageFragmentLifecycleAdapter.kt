package com.rocks.sportibot.ui.base.fragment.fragment_pager

import android.support.v4.view.ViewPager

class PageFragmentLifecycleAdapter(private val pages: List<OnFragmentPagerLifecycle>,
                                   private var currentPosition: Int = 0) : ViewPager.OnPageChangeListener {


    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        val currentFragmentPage = pages[currentPosition]
        val newFragmentPage = pages[position]

        newFragmentPage.onShownPage()

        currentFragmentPage.onHiddenPage()

        currentPosition = position
    }
}
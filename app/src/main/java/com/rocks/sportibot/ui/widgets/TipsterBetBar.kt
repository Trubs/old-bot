package com.rocks.sportibot.ui.widgets

import android.content.Context
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.rocks.sportibot.R
import com.rocks.sportibot.utils.extensions.getTimeAgoFrom
import com.rocks.sportibot.utils.extensions.loadImageInto
import com.rocks.sportibot.utils.extensions.toDateTimeFormat
import kotlinx.android.synthetic.main.view_bets_tipster_bar.view.*

class TipsterBetBar @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_bets_tipster_bar, this, true)
    }

    fun setImage(imageUrl: String?) {
        userThumbImageView.loadImageInto(imageUrl, R.drawable.tabbar_icon_tipsters)
    }

    fun setName(name: String?) {
        nameTextView.text = name
    }

    fun setLastOnline(lastOnline: String?) {
        timeAgoTextView.text = lastOnline?.toDateTimeFormat()?.getTimeAgoFrom(context)
    }

    fun setAutoBet(isAutoBet: Boolean) {

        @StringRes
        val text =
                if (isAutoBet) R.string.bet_auto
                else R.string.bet_manual

        @DrawableRes
        val typeIcon =
                if (isAutoBet) R.drawable.ic_autobet
                else R.drawable.ic_manual

        @ColorInt
        val textColor = ContextCompat.getColor(context,
                if (isAutoBet) R.color.type_subscription_auto
                else R.color.type_subscription_manual)

        @ColorRes
        val backgroundColor =
                if (isAutoBet) R.color.type_subscription_auto_bg
                else R.color.type_subscription_manual_bg



        typeOfSubscriptionTextView.setText(text)
        typeOfSubscriptionTextView.setTextColor(textColor)
        typeOfSubscriptionImageView.setImageResource(typeIcon)
        setBackgroundResource(backgroundColor)
    }

}
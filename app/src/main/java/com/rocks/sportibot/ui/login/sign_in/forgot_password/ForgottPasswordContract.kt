package com.rocks.sportibot.ui.login.sign_in.forgot_password

import com.rocks.sportibot.ui.base.interfaces.ViewExitListener
import com.rocks.sportibot.ui.base.interfaces.ViewProgressListener
import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract

interface ForgottPasswordContract {
    interface View : BaseContract.View,
            ViewProgressListener,
            ViewThrowableListener,
            ViewExitListener {

    }

    interface Presenter : BaseContract.Presenter<View> {

        fun onRestorePressed(email: String?)
    }
}
package com.rocks.sportibot.ui.base.recycler_adapter

import android.content.Context
import android.support.annotation.LayoutRes
import android.support.annotation.StringRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

open class BaseRecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    protected val context: Context = view.context

    fun getString(@StringRes stringRes: Int): String = context.getString(stringRes)

}
fun ViewGroup.inflateLayoutView(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}


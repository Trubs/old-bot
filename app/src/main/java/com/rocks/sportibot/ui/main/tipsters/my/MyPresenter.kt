package com.rocks.sportibot.ui.main.tipsters.my

import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.data.interactors.tipster.TipsterInteractor
import com.rocks.sportibot.ui.base.interfaces.ViewEnabledListener
import com.rocks.sportibot.utils.extensions.manageProgressView
import com.rocks.sportibot.utils.extensions.scheduleIOToMainThread
import com.rocks.sportibot.utils.extensions.wrapViewObservable
import com.rocks.sportibot.utils.extensions.wrapViewSingle
import io.reactivex.disposables.CompositeDisposable

class MyPresenter(
        private val tipsterInteractor: TipsterInteractor,
        private val compositeDisposable: CompositeDisposable
) : MyContract.Presenter {

    override lateinit var view: MyContract.View

    override fun onCreate() {
        super.onCreate()
        listenEventOfTipsters()
        loadData()
    }

    override fun onFilterChanged() {
        loadData()
    }

    override fun onRefresh() {
        loadData()
    }

    override fun onTipsterPressed(tipster: Tipster) {
        //TODO("Tipster > Click not implemented")
    }

    override fun onUnfollowTipsterPressed(tipster: Tipster,
                                          viewEnabledListener: ViewEnabledListener) {
        compositeDisposable.add(tipsterInteractor.unFollowTipster(tipster)
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .doOnSubscribe { viewEnabledListener.setEnabled(false) }
                .doFinally { viewEnabledListener.setEnabled(true) }
                .subscribeWith(wrapViewSingle(view,
                        successFunction = {
                            view.removeTipster(it)
                        })))
    }

    private fun listenEventOfTipsters() {
        compositeDisposable.add(tipsterInteractor.registerTipsterUpdate()
                .scheduleIOToMainThread()
                .subscribeWith(wrapViewObservable(view,
                        onNextFunction = {
                            with(view) {
                                if (it.isFollow()) addTipster(it)
                                else removeTipster(it)
                            }
                        })))
    }

    private fun loadData() {
        compositeDisposable.add(tipsterInteractor.getMyTipstersAsync()
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .subscribeWith(wrapViewSingle(view, {
                    view.setTipsterList(it)
                })))
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}

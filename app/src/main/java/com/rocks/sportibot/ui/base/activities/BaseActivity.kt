package com.rocks.sportibot.ui.base.activities

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.widget.ProgressBar
import com.rocks.sportibot.R
import com.rocks.sportibot.ui.base.interfaces.OnNavigationBackListener
import com.rocks.sportibot.ui.base.interfaces.ViewProgressListener
import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import com.rocks.sportibot.utils.extensions.gone
import com.rocks.sportibot.utils.extensions.showToastMessage
import com.rocks.sportibot.utils.extensions.visible

abstract class BaseActivity : AppCompatActivity(),
        ViewProgressListener,
        ViewThrowableListener,
        OnNavigationBackListener {
    @LayoutRes
    abstract fun layoutRes(): Int

    abstract fun injectPresenter()

    protected var toolbar: android.support.v7.widget.Toolbar? = null
    protected var progressBar: ProgressBar? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectPresenter()
        setContentView(layoutRes())

        initViews()
        initToolbar()
    }

    protected open fun initViews() {
        toolbar = findViewById(R.id.toolbar)
        progressBar = findViewById(R.id.progressBar)
    }

    protected open fun initToolbar() {
        toolbar?.apply {
            setNavigationOnClickListener { onNavigationBackPressed() }
        }
    }

    override fun showProgress() {
        progressBar?.visible()
    }

    override fun hideProgress() {
        progressBar?.gone()
    }

    override fun showError(message: String) {
        showToastMessage(message)
    }

    override fun showError(messageRes: Int) {
        showToastMessage(messageRes)
    }

    override fun onNavigationBackPressed(): Boolean {
        onBackPressed()
        return false
    }

    /** @hide */
    final override fun onBackPressed() {
        super.onBackPressed()
    }
}
package com.rocks.sportibot.ui.base.clicklisteners

interface OnDataClickListener<T> {

    fun onDataClick(data: T)

}
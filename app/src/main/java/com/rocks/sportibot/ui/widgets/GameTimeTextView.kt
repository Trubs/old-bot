package com.rocks.sportibot.ui.widgets

import android.content.Context
import android.support.annotation.ColorInt
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.rocks.sportibot.R

/**
 * Custom text view displaying the start time or score of the game
 * */
class GameTimeTextView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {
    @ColorInt
    private var liveTextColor: Int = currentTextColor

    @ColorInt
    private var defaultTextColor: Int = currentTextColor

    /**
     * Parse the custom color of the live game
     * and other if there is
     * */
    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.GameTimeTextView)
        try {
            liveTextColor = typedArray.getColor(R.styleable.GameTimeTextView_liveTextColor, defaultTextColor)
        } finally {
            typedArray.recycle()
        }
    }

    /**
     * To set the text of the game
     * If the game is live [isLive] now - show the score of it and change the color
     * to [liveTextColor]
     *
     * else show the start date of the game [startsDateTime] and change the color
     * to [defaultTextColor]
     *
     * @param isLive - the game is live
     * @param startsDateTime - the start date of the game
     * @param scoreOfGame - the score of the game (e.g. 1:0)
     * */
    fun setLive(isLive: Boolean,
                startsDateTime: String?,
                scoreOfGame: String) {
        text = if (isLive) {
            setTextColor(liveTextColor)
            context.getString(R.string.view_game_time_text_label_live, scoreOfGame)
        } else {
            setTextColor(defaultTextColor)
            startsDateTime
        }
    }

}
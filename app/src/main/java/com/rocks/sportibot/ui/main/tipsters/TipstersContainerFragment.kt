package com.rocks.sportibot.ui.main.tipsters

import android.os.Bundle
import android.view.View
import com.rocks.sportibot.R
import com.rocks.sportibot.ui.base.fragment.BaseFragment
import com.rocks.sportibot.ui.base.fragment.fragment_pager.BaseFragmentFilterPage
import com.rocks.sportibot.ui.base.fragment.fragment_pager.BaseFragmentPage
import com.rocks.sportibot.ui.base.fragment.fragment_pager.PageFragmentLifecycleAdapter
import com.rocks.sportibot.ui.base.pager.FragmentPagerAdapter
import com.rocks.sportibot.ui.base.pager.TabFragmentDto
import com.rocks.sportibot.ui.main.tipsters.all.AllFragmentPage
import com.rocks.sportibot.ui.main.tipsters.my.MyFragmentPage
import com.rocks.sportibot.ui.main.tipsters.settings.FilterDialogFragment
import kotlinx.android.synthetic.main.fragment_tipsters_container.*

class TipstersContainerFragment : BaseFragment() {
    private val TAG: String = TipstersContainerFragment::class.java.simpleName

    private val TAG_FILTER = "${TAG}_filter"

    override val layoutRes: Int = R.layout.fragment_tipsters_container

    private val fragmentPageMy: BaseFragmentFilterPage = MyFragmentPage()
    private val fragmentPageAll: BaseFragmentFilterPage = AllFragmentPage()

    private val fragmentList: List<BaseFragmentFilterPage> = listOf(fragmentPageMy, fragmentPageAll)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setTitle(R.string.title_tipsters)

        initListeners()
        initViewPager()
    }

    private fun initListeners() {
        filterImageButton.setOnClickListener {
            showFilterDialog()
        }
    }

    private fun showFilterDialog() {
        val fragmentFilter = FilterDialogFragment()

        fragmentFilter.onFilterChangeListener = {
            fragmentList.filter { it.isVisible }
                    .forEach {
                        it.onFilterChanged()
                    }
        }

        fragmentFilter.show(childFragmentManager, TAG_FILTER)
    }

    private fun initViewPager() {
        val myPage = createTab(R.string.fragment_tipsters_tab_my, -1, fragmentPageMy)
        val allPage = createTab(R.string.fragment_tipsters_tab_all, -1, fragmentPageAll)
        val pageTabList = listOf(myPage, allPage)

        val adapter = FragmentPagerAdapter(tabLayout, pageTabList, childFragmentManager)

        tabLayout.setupWithViewPager(viewPager)
        viewPager.adapter = adapter
        adapter.invalidateTabs()

        viewPager.addOnPageChangeListener(PageFragmentLifecycleAdapter(pageTabList.map { it.fragment }))
    }

    private fun createTab(stringRes: Int, drawableRes: Int, fragment: BaseFragmentPage): TabFragmentDto {
        return TabFragmentDto.createTab(context!!, stringRes, drawableRes, fragment)
    }
}
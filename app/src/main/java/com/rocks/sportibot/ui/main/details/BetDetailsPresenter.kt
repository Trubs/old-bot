package com.rocks.sportibot.ui.main.details

import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.data.interactors.bets.BetsInteractor
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.utils.extensions.manageProgressView
import com.rocks.sportibot.utils.extensions.scheduleIOToMainThread
import com.rocks.sportibot.utils.extensions.wrapViewCompletable
import io.reactivex.disposables.CompositeDisposable

class BetDetailsPresenter(
        private val bet: Bet,
        private val betsInteractor: BetsInteractor,
        private val sessionRepository: SessionRepository,
        private val compositeDisposable: CompositeDisposable
) : BetDetailsContract.Presenter {
    override lateinit var view: BetDetailsContract.View

    override fun onCreate() {
        super.onCreate()

        view.setTitle(bet.game?.title)

        view.setBetDetails(bet)

        view.setMinAmount(bet.minAmount, sessionRepository.getAccount().getCurrency())
    }

    /**
     * Call when 'place bet' pressed
     * */
    override fun onPlaceBetPressed(amountText: String) {

        val amount: Float = try {
            amountText.toFloat()
        } catch (e: NumberFormatException) {
            view.showError(R.string.dialog_bet_details_label_invalid_amount_format)
            return
        }

        if (amount < bet.minAmount) {
            view.showError(R.string.dialog_bet_details_label_invalid_amount_less_min)
            return
        }

        compositeDisposable.add(betsInteractor
                .placeBet(bet, amount)
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .subscribeWith(wrapViewCompletable(view, {
                    view.showPlacedBetSuccessful()
                    view.exit()
                })))

    }

    /**
     * Call when 'skip bet' pressed
     * */
    override fun onPlaceSkipPressed() {
        compositeDisposable.add(betsInteractor
                .skipBet(bet)
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .subscribeWith(wrapViewCompletable(view, {
                    view.showSkippedBetSuccessful()
                    view.exit()
                })))
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}

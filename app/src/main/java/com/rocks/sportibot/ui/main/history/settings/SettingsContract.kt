package com.rocks.sportibot.ui.main.history.settings

import com.rocks.sportibot.data.dto.history.filter.ShowBetsHistoryEnum
import com.rocks.sportibot.data.dto.history.filter.SortHistoryEnum
import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract
import java.util.*

interface SettingsContract {
    interface View : BaseContract.View,
            ViewThrowableListener {

        fun setShowBetsLabel(showBetsHistoryEnum: ShowBetsHistoryEnum)

        fun setDateFromLabel(date: String)

        fun setDateUntilLabel(date: String)

        fun setSortByLabel(sortHistoryEnum: SortHistoryEnum)

        fun showSelectDateFromDialog(currentDate: Date, minDate: Date?, maxDate: Date?)

        fun showSelectDateUntilDialog(currentDate: Date, minDate: Date?, maxDate: Date?)

        fun notifyFilterChanged()

    }

    interface Presenter : BaseContract.Presenter<View> {

        fun onShowBetsChanged(showBetsHistoryEnum: ShowBetsHistoryEnum)

        fun onDateFromChanged(year: Int, month: Int, day: Int)

        fun onDateUntilChanged(year: Int, month: Int, day: Int)

        fun onSortByChanged(sortHistoryEnum: SortHistoryEnum)

        fun onDateFromPressed()

        fun onDateUntilPressed()

    }
}
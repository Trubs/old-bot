package com.rocks.sportibot.ui.main.tipsters.all

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.fragment.fragment_pager.mvp.BaseMvpFragmentFilterPage
import com.rocks.sportibot.ui.base.recycler_adapter.DrawableDividerDecorator
import com.rocks.sportibot.ui.base.recycler_adapter.VerticalSpaceItemDecoration
import com.rocks.sportibot.ui.main.tipsters.adapter.TipsterAdapter
import com.rocks.sportibot.ui.main.tipsters.adapter.TipsterEnum
import kotlinx.android.synthetic.main.fragment_tipsters_all.*

class AllFragmentPage : BaseMvpFragmentFilterPage<AllContract.Presenter>(), AllContract.View {
    private val adapter: TipsterAdapter = TipsterAdapter(TipsterEnum.ALL)

    override val layoutRes: Int = R.layout.fragment_tipsters_all

    override fun injectPresenter() {
        ComponentProvider.Main.getTipsterComponent().inject(this)
        presenter.view = this
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
        initListeners()
    }

    private fun initRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(activity)

        recyclerView.addItemDecoration(VerticalSpaceItemDecoration(
                resources.getDimensionPixelSize(R.dimen.margin_small),
                DrawableDividerDecorator(activity!!, R.drawable.divider_gray)))

        recyclerView.adapter = adapter
    }

    private fun initListeners() {
        swipeRefreshLayout!!.setOnRefreshListener { presenter.onRefresh() }
        adapter.onTipsterClickListener = presenter
        adapter.onFollowTipsterClickListener = presenter::onFollowTipsterPressed
        adapter.onUnFollowTipsterClickListener = presenter::onUnfollowTipsterPressed
    }

    override fun setTipsterList(tipsters: List<Tipster>) {
        adapter.setTipsters(tipsters)
    }

    override fun updateTipster(tipster: Tipster) {
        adapter.updateItem(tipster)
    }

}
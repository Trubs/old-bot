package com.rocks.sportibot.ui.base.interfaces

import android.content.Intent

interface ViewExitResultListener {
    fun exit(resultCode: Int, data: Intent?)
}
package com.rocks.sportibot.ui.main.bets.skip

import com.rocks.sportibot.data.interactors.bets.BetsInteractor
import com.rocks.sportibot.utils.extensions.manageProgressView
import com.rocks.sportibot.utils.extensions.scheduleIOToMainThread
import com.rocks.sportibot.utils.extensions.wrapViewObservable
import com.rocks.sportibot.utils.extensions.wrapViewSingle
import io.reactivex.disposables.CompositeDisposable

class SkipPresenter(
        private val betsInteractor: BetsInteractor,
        private val compositeDisposable: CompositeDisposable
) : SkipContract.Presenter {

    override lateinit var view: SkipContract.View

    override fun onCreate() {
        super.onCreate()

        loadBets()
    }

    override fun onRefresh() {
        loadBets()
    }

    private fun loadBets() {
        compositeDisposable.add(betsInteractor
                .getBetsSkipAsync()
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .subscribeWith(wrapViewSingle(view, {
                    view.setBetList(it)
                })))

        //todo after stop() must be re-subscribing
        compositeDisposable.add(betsInteractor
                .getSkippedBetObservable()
                .scheduleIOToMainThread()
                .subscribeWith(wrapViewObservable(view, {
                    view.addBet(it)
                })))
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}
package com.rocks.sportibot.ui.main.bets

import android.os.Bundle
import android.view.View
import com.rocks.sportibot.R
import com.rocks.sportibot.ui.base.fragment.BaseFragment
import com.rocks.sportibot.ui.base.fragment.fragment_pager.BaseFragmentPage
import com.rocks.sportibot.ui.base.fragment.fragment_pager.PageFragmentLifecycleAdapter
import com.rocks.sportibot.ui.base.pager.FragmentPagerAdapter
import com.rocks.sportibot.ui.base.pager.TabFragmentDto
import com.rocks.sportibot.ui.main.bets.open.OpenFragmentPage
import com.rocks.sportibot.ui.main.bets.placed.PlacedFragmentPage
import com.rocks.sportibot.ui.main.bets.skip.SkipFragmentPage
import kotlinx.android.synthetic.main.fragment_my_bets_container.*

class MyBetsContainerFragment : BaseFragment() {

    override val layoutRes: Int = R.layout.fragment_my_bets_container

    private val fragmentOpen: BaseFragmentPage = OpenFragmentPage()
    private val fragmentPlaced: BaseFragmentPage = PlacedFragmentPage()
    private val fragmentSkip: BaseFragmentPage = SkipFragmentPage()

    private val fragments: List<BaseFragmentPage> = listOf(fragmentOpen, fragmentPlaced, fragmentSkip)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setTitle(R.string.title_bets)

        initViews()
        initListeners()
        initViewPager()
    }

    private fun initViews() {
    }

    private fun initListeners() {

    }

    private fun initViewPager() {

        val open: TabFragmentDto = createTab(R.string.fragment_bets_tab_open, -1, fragmentOpen)
        val placed: TabFragmentDto = createTab(R.string.fragment_bets_tab_placed, -1, fragmentPlaced)
        val skip: TabFragmentDto = createTab(R.string.fragment_bets_tab_skip, -1, fragmentSkip)

        val fragmentList = listOf(open, placed, skip)

        val adapter = FragmentPagerAdapter(
                tabLayout,
                fragmentList,
                childFragmentManager)

        viewPager.addOnPageChangeListener(PageFragmentLifecycleAdapter(fragments))
        viewPager.adapter = adapter

        tabLayout.setupWithViewPager(viewPager)

        adapter.invalidateTabs()
    }

    private fun createTab(stringRes: Int, drawableRes: Int, fragment: BaseFragmentPage): TabFragmentDto {
        return TabFragmentDto.createTab(context!!, stringRes, drawableRes, fragment)
    }

}
package com.rocks.sportibot.ui.main.tipsters.all

import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.data.interactors.tipster.TipsterInteractor
import com.rocks.sportibot.ui.base.interfaces.ViewEnabledListener
import com.rocks.sportibot.utils.extensions.manageProgressView
import com.rocks.sportibot.utils.extensions.scheduleIOToMainThread
import com.rocks.sportibot.utils.extensions.wrapViewObservable
import com.rocks.sportibot.utils.extensions.wrapViewSingle
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

class AllPresenter(
        private val tipsterInteractor: TipsterInteractor,
        private val compositeDisposable: CompositeDisposable
) : AllContract.Presenter {

    override lateinit var view: AllContract.View

    override fun onCreate() {
        super.onCreate()

        listenEventOfTipsters()
        loadData()
    }

    override fun onRefresh() {
        loadData()
    }

    override fun onFilterChanged() {
        loadData()
    }

    override fun onTipsterPressed(tipster: Tipster) {
        //TODO("Tipster > Click not implemented")
    }

    override fun onFollowTipsterPressed(tipster: Tipster,
                                        viewEnabledListener: ViewEnabledListener) {
        manageFollowTipster(tipsterInteractor.followTipster(tipster), viewEnabledListener)
    }

    override fun onUnfollowTipsterPressed(tipster: Tipster,
                                          viewEnabledListener: ViewEnabledListener) {
        manageFollowTipster(tipsterInteractor.unFollowTipster(tipster), viewEnabledListener)
    }

    private fun listenEventOfTipsters() {
        compositeDisposable.add(tipsterInteractor.registerTipsterUpdate()
                .scheduleIOToMainThread()
                .subscribeWith(wrapViewObservable(view,
                        onNextFunction = {
                            view.updateTipster(it)
                        })))
    }

    private fun manageFollowTipster(function: Single<Tipster>,
                                    viewEnabledListener: ViewEnabledListener) {
        compositeDisposable.add(function
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .doOnSubscribe { viewEnabledListener.setEnabled(false) }
                .doFinally { viewEnabledListener.setEnabled(true) }
                .subscribeWith(wrapViewSingle(view,
                        successFunction = {
                            view.updateTipster(it)
                        })))
    }

    private fun loadData() {
        compositeDisposable.add(tipsterInteractor.getAllTipstersAsync()
                .scheduleIOToMainThread()
                .manageProgressView(view)
                .subscribeWith(wrapViewSingle(view, {
                    view.setTipsterList(it)
                })))
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}
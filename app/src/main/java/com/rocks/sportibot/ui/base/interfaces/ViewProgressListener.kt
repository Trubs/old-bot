package com.rocks.sportibot.ui.base.interfaces

interface ViewProgressListener {

    fun showProgress()
    fun hideProgress()

}
package com.rocks.sportibot.ui.login.sign_in

import java.io.Serializable

data class SignInDto(
        val email: String,
        val password: String
) : Serializable
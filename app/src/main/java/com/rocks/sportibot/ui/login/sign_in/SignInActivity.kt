package com.rocks.sportibot.ui.login.sign_in

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.rocks.sportibot.R
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.activities.mvp.BaseMvpActivity
import com.rocks.sportibot.ui.login.sign_in.forgot_password.ForgottDialogFragment
import com.rocks.sportibot.ui.login.sign_up.SignUpActivity
import com.rocks.sportibot.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : BaseMvpActivity<SignInContract.Presenter>(), SignInContract.View {
    override fun layoutRes(): Int = R.layout.activity_sign_in

    override fun injectPresenter() {
        ComponentProvider.Login.getLoginComponent().inject(this)
        presenter.view = this
    }

    companion object {
        fun getIntentClearTask(context: Context): Intent {
            return Intent(context, SignInActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                            or Intent.FLAG_ACTIVITY_CLEAR_TOP
                            or Intent.FLAG_ACTIVITY_NEW_TASK)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initListeners()
    }

    private fun initListeners() {
        signInButton.setOnClickListener {
            presenter.onSignPressed(SignInDto(emailEditText.getText(), passwordEditText.getText()))
        }
        registerButton.setOnClickListener { presenter.onRegisterPressed() }
        forgotButton.setOnClickListener { presenter.onForgotPasswordPressed(emailEditText.getText()) }

    }

    override fun showDialogForgotPassword(email: String) {
        ForgottDialogFragment.newInstance(email).show(supportFragmentManager, null)
    }

    override fun openMainActivity() {
        startActivity(MainActivity.getIntentClearTask(this))
    }

    override fun openRegisterActivity() {
        startActivity(SignUpActivity.getIntent(this))
    }
}
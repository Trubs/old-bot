package com.rocks.sportibot.ui.main.tipsters.adapter

import android.support.annotation.ColorRes
import android.support.annotation.StringRes
import android.view.ViewGroup
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.ui.base.recycler_adapter.inflateLayoutView
import com.rocks.sportibot.utils.extensions.getColorInt
import kotlinx.android.synthetic.main.item_tipsters_all.view.*

class TipsterAllViewHolder(parent: ViewGroup) : BaseTipsterViewHolder(parent.inflateLayoutView(R.layout.item_tipsters_all)) {

    override fun bind(tipster: Tipster) {
        with(itemView) {
            nameTextView.text = tipster.user?.username
            //TODO image
            //it.circleImageView.loadImageInto(tipster.image, R.drawable.tabbar_icon_tipsters)

            @StringRes val statusLabel =
                    if (tipster.isPremium()) R.string.status_premium
                    else R.string.status_free
            @ColorRes val statusColor =
                    if (tipster.isPremium()) R.color.status_premium
                    else R.color.status_free

            statusTextView.setText(statusLabel)
            statusTextView.setTextColor(itemView.context.getColorInt(statusColor))

            //TODO isFollow
            followButton.isActivated = tipster.isFollow()
            tipster.stats.let { stats ->
                yieldTextView.text = stats?.yield.toString()
                unitsTextView.text = stats?.units.toString()
                followersTextView.text = stats?.followers.toString()
                tipsTextView.text = stats?.tips.toString()
            }

            //TODO comment
            //it.commentTextView.text = tipster.comment

            followButton.setOnClickListener {
                if (it.isActivated) onUnFollowTipsterClickListener?.invoke(tipster, followButton)
                else onFollowTipsterClickListener?.invoke(tipster, followButton)
            }

        }

        itemView.setOnClickListener { onTipsterClickListener?.onTipsterPressed(tipster) }
    }
}
package com.rocks.sportibot.ui.base.fragment.mvp

import android.os.Bundle
import android.view.View
import com.rocks.sportibot.ui.base.fragment.BaseFragment
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract
import javax.inject.Inject

abstract class BaseMvpFragment<P : BaseContract.Presenter<*>> : BaseFragment() {

    @Inject
    protected lateinit var presenter: P

    protected abstract fun injectPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectPresenter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onCreate()
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }
}
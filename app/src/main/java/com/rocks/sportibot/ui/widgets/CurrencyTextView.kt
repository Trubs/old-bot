package com.rocks.sportibot.ui.widgets

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.rocks.sportibot.data.dto.Currency

class CurrencyTextView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {

    fun setText(value: Float, currency: Currency) {
        text = String.format("%s %s", value, currency.getSymbol())
    }

}
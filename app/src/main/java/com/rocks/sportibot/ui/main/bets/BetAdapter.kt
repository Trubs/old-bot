package com.rocks.sportibot.ui.main.bets

import android.view.ViewGroup
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.ui.base.recycler_adapter.BaseRecyclerAdapter
import com.rocks.sportibot.ui.base.recycler_adapter.BaseRecyclerViewHolder
import com.rocks.sportibot.ui.base.recycler_adapter.inflateLayoutView
import kotlinx.android.synthetic.main.item_bet_open.view.*

class BetAdapter : BaseRecyclerAdapter() {
    var onBetItemClickListener: ((Bet) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRecyclerViewHolder {
        return BetOpenViewHolder(parent)
    }

    override fun onBindViewHolder(holder: BaseRecyclerViewHolder, position: Int) {
        (holder as BetOpenViewHolder).bind(getItemOfPosition(position))
    }

    inner class BetOpenViewHolder(parent: ViewGroup)
        : BaseRecyclerViewHolder(parent.inflateLayoutView(R.layout.item_bet_open)) {


        fun bind(bet: Bet) {
            itemView.apply {
                betOpenView.bet = bet
                betOpenView.onBetItemClickListener = {
                    onBetItemClickListener?.invoke(it)
                }
            }
        }
    }
}
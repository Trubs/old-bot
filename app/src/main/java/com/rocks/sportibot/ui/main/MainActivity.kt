package com.rocks.sportibot.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.Currency
import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.base.activities.mvp.BaseMvpActivity
import com.rocks.sportibot.ui.base.interfaces.OnNavigationBackListener
import com.rocks.sportibot.ui.main.bets.MyBetsContainerFragment
import com.rocks.sportibot.ui.main.details.BetDetailsDialogFragment
import com.rocks.sportibot.ui.main.history.HistoryFragment
import com.rocks.sportibot.ui.main.news.NewsFragment
import com.rocks.sportibot.ui.main.settings.SettingsFragment
import com.rocks.sportibot.ui.main.tipsters.TipstersContainerFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseMvpActivity<MainContract.Presenter>(), MainContract.View {
    override fun layoutRes(): Int = R.layout.activity_main

    private val tipstersTabFragment = TipstersContainerFragment()
    private val myBetsTabFragment = MyBetsContainerFragment()
    private val historyTabFragment = HistoryFragment()
    private val settingsTabFragment = SettingsFragment()
    private val newsTabFragment = NewsFragment()

    private val fragmentList = listOf(
            newsTabFragment,
            tipstersTabFragment,
            myBetsTabFragment,
            historyTabFragment,
            settingsTabFragment)

    override fun injectPresenter() {
        ComponentProvider.Login.clearLoginComponent()
        ComponentProvider.Main.getMainComponent().inject(this)
        presenter.view = this
    }

    companion object {
        private val TAG: String = MainActivity::class.java.simpleName
        val EXTRA_PUSH_BET = "$TAG-EXTRA_EXTRA_PUSH_BET"

        fun getIntentClearTask(context: Context) = Intent(context, MainActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                        or Intent.FLAG_ACTIVITY_CLEAR_TOP
                        or Intent.FLAG_ACTIVITY_NEW_TASK)

        fun getIntentExtra(context: Context, bet: Bet) = Intent(context, MainActivity::class.java)
                .putExtra(EXTRA_PUSH_BET, bet)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                        or Intent.FLAG_ACTIVITY_CLEAR_TOP
                        or Intent.FLAG_ACTIVITY_NEW_TASK)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        checkBetFromPush()

        initListeners()
        initFragments()
        initNavigation()

        navigation.selectedItemId = R.id.navigation_bets
    }

    private fun checkBetFromPush() {
        if (intent.hasExtra(EXTRA_PUSH_BET)) {
            val bet = intent.getSerializableExtra(EXTRA_PUSH_BET) as Bet

            val betDialog = BetDetailsDialogFragment.newInstance(bet)
            betDialog.isCancelable = false
            betDialog.show(supportFragmentManager, "")
        }
    }

    private fun initListeners() {
        bankrollView.setOnClickListener { presenter.onBackrollPressed() }
        liveHelpButton.setOnClickListener { presenter.onLiveHelpPressed() }
    }

    private fun initFragments() {
        fragmentList.forEach {
            supportFragmentManager
                    .beginTransaction()
                    .add(containerViewGroup.id, it)
                    .detach(it)
                    .commitAllowingStateLoss()
        }
    }

    private fun initNavigation() {
        val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
            return@OnNavigationItemSelectedListener when (item.itemId) {
//                TODO Remove or uncomment a news
//                R.id.navigation_news -> {
//                    replaceTabFragment(newsTabFragment)
//                    true
//                }
                R.id.navigation_tipsters -> {
                    replaceTabFragment(tipstersTabFragment)
                    true
                }
                R.id.navigation_bets -> {
                    replaceTabFragment(myBetsTabFragment)
                    true
                }
                R.id.navigation_history -> {
                    replaceTabFragment(historyTabFragment)
                    true
                }
                R.id.navigation_settings -> {
                    replaceTabFragment(settingsTabFragment)
                    true
                }
                else -> false
            }
        }

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private fun replaceTabFragment(fragment: Fragment) {
        val lastFragment: Fragment? = supportFragmentManager.findFragmentById(containerViewGroup.id)

        val fragmentTransaction = supportFragmentManager.beginTransaction()

        lastFragment?.let { fragmentTransaction.detach(it) }

        fragmentTransaction.attach(fragment)

        fragmentTransaction.commitAllowingStateLoss()
    }

    override fun bankrollShowProgress() {
        bankrollView.showProgress()
    }

    override fun bankrollHideProgress() {
        bankrollView.hideProgress()
    }

    override fun bankrollSetValue(value: Float?, currency: Currency) {
        bankrollView.setBankroll(value, currency)
    }

    override fun setTitle(title: CharSequence?) {
        super.setTitle(title)
        titleToobarTextView.text = title
    }

    override fun setTitle(titleId: Int) {
        super.setTitle(titleId)
        titleToobarTextView.setText(titleId)
    }

    override fun exit() {
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        ComponentProvider.Main.clearMainComponent()
    }

    override fun onNavigationBackPressed(): Boolean {
        val fragment = supportFragmentManager.findFragmentById(containerViewGroup.id)
        if (fragment != null
                && fragment is OnNavigationBackListener
                && (fragment as OnNavigationBackListener).onNavigationBackPressed()) {
        } else {
            presenter.onBackPressed()
        }
        return true
    }
}

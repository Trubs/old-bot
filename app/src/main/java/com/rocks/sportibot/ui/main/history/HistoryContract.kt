package com.rocks.sportibot.ui.main.history

import com.rocks.sportibot.data.dto.history.History
import com.rocks.sportibot.ui.base.interfaces.*
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract

interface HistoryContract {
    interface View : BaseContract.View,
            ViewFilterWindowPopupListener,
            ViewProgressListener,
            ViewPaginationProgressListener,
            ViewThrowableListener {

        /**
         * Set history list
         * Remove last data
         * */
        fun setHistory(historyList: List<History>)

        /**
         * Adding history list
         * */
        fun addHistory(historyList: List<History>)
    }

    interface Presenter : BaseContract.Presenter<View>,
            OnFilterChangeListener,
            OnRefreshListener {

        /**
         * Load next history
         * */
        fun loadNext()

    }
}
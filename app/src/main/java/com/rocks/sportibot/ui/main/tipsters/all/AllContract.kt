package com.rocks.sportibot.ui.main.tipsters.all

import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.ui.base.fragment.fragment_pager.OnFragmentPagerLifecycle
import com.rocks.sportibot.ui.base.interfaces.*
import com.rocks.sportibot.ui.base.interfaces.mvp.BaseContract
import com.rocks.sportibot.ui.base.interfaces.tipster.OnTipsterClickListener

interface AllContract {
    interface View : BaseContract.View,
            ViewProgressListener,
            ViewThrowableListener {

        fun setTipsterList(tipsters: List<Tipster>)

        fun updateTipster(tipster: Tipster)
    }

    //todo move listeners to base presenter
    interface Presenter : BaseContract.Presenter<View>,
            OnTipsterClickListener,
            OnFilterChangeListener,
            OnFragmentPagerLifecycle,
            OnRefreshListener {

        /**
         * @param tipster - tipster to unsubscribe
         * @param viewEnabledListener - button enable listener
         * */
        fun onFollowTipsterPressed(tipster: Tipster,
                                   viewEnabledListener: ViewEnabledListener)

        /**
         * @param tipster - tipster to unsubscribe
         * @param viewEnabledListener - button enable listener
         * */
        fun onUnfollowTipsterPressed(tipster: Tipster,
                                     viewEnabledListener: ViewEnabledListener)

    }
}
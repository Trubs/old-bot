package com.rocks.sportibot.ui.main.bets

import android.content.Context
import android.support.transition.TransitionManager
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.utils.extensions.setVisibleOrGone
import com.rocks.sportibot.utils.extensions.setVisibleOrInvisible
import kotlinx.android.synthetic.main.view_bet_open.view.*

class BetOpenView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0,
        bet: Bet? = null
) : FrameLayout(context, attrs, defStyleAttr) {

    var bet: Bet? = null
        set(value) {
            field = value

            value?.let { bet ->
                tipsterBar.setName(bet.tipster?.user?.username.orEmpty())
                //TODO image
                //tipsterBar.setImage(bet.tipster.image)

                //TODO last online
                //tipsterBar.setLastOnline(bet.tipster.lastOnline)

                //TODO isAutoBet
                //tipsterBar.setAutoBet(bet.isAutoBet)

                labelTextView.text = bet.game?.getName()
                gameInfoTextView.text = bet.game?.getGameInfo()
                //todo set game score
                gameTimeTextView.setLive(bet.game?.isLive()
                        ?: false, bet.game?.getStartTime(), "?:?")

                overTextView.text = bet.game?.getOver()

                oddsRange.setRange(bet.range)
                oddsRange.setValue(bet.game?.odd)

                unitsRange.setRange(bet.getUnitFractions())
                unitsRange.setValue(bet.units.toString())

                bookmakerTextView.text = bet.game?.bookie

                bet.comment?.let {
                    commentTextView.text = it
                    seeMoreButtonActiveState.setVisibleOrInvisible(true)
                }

//                TODO isRunning
//                connectionTextActiveState.isActivated = bet.isRunning

                seeMoreButtonActiveState.setOnClickListener { button ->
                    button.isActivated = !button.isActivated

                    TransitionManager.beginDelayedTransition(containerViewGroup)

                    commentContainer.setVisibleOrGone(button.isActivated)

                    TransitionManager.endTransitions(containerViewGroup)
                }

                setOnClickListener {
                    onBetItemClickListener?.invoke(bet)
                }
            }

        }

    init {
        LayoutInflater.from(context).inflate(R.layout.view_bet_open, this)
        this.bet = bet
    }

    public var onBetItemClickListener: ((Bet) -> Unit)? = null

}
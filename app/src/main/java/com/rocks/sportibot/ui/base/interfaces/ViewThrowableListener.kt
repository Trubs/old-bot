package com.rocks.sportibot.ui.base.interfaces

import android.support.annotation.StringRes

interface ViewThrowableListener {

    fun showError(message: String)

    fun showError(@StringRes messageRes: Int)

}
package com.voodoo.rocks.aged_calendar.model.data.exception

class ApiResponseException : RuntimeException {

    constructor(detailMessage: String) : super(detailMessage)

    constructor(throwable: Throwable?) : super(throwable)

    constructor(detailMessage: String, throwable: Throwable) : super(detailMessage, throwable)
}

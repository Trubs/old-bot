package com.rocks.sportibot.data.request.bets

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.request.TokenRequest
import java.io.Serializable

class BetsRequest(accessToken: String?,
                  @SerializedName("state") val state: State
) : TokenRequest(accessToken), Serializable {

    override fun toString(): String {
        return "BetsRequest(state='$state')"
    }

    enum class State(val value: String) {
        @SerializedName("placed")
        PLACED("placed"),
        @SerializedName("skipped")
        SKIP("skipped"),
        @SerializedName("open")
        OPEN("open")
    }
}



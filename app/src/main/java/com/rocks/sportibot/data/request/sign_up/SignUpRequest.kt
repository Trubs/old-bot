package com.rocks.sportibot.data.request.sign_up

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SignUpRequest(
        @SerializedName("user") val userRegister: UserRegister,
        @SerializedName("timezone") val timezone: Int
) : Serializable {
    override fun toString(): String {
        return "SignUpRequest(userRegister=$userRegister, timezone=$timezone)"
    }
}
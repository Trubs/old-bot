package com.rocks.sportibot.data.interactors.news

import com.rocks.sportibot.data.dto.news.News
import com.rocks.sportibot.data.repository.news.NewsRepository
import io.reactivex.Single

class NewsInteractorImpl(
        private val newsRepository: NewsRepository
) : NewsInteractor {

    override fun getNewsAsync(): Single<List<News>> {
        return newsRepository.getNewsAsync()
    }
}
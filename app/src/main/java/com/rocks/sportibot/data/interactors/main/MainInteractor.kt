package com.rocks.sportibot.data.interactors.main

import com.rocks.sportibot.data.dto.Balance
import io.reactivex.Observable

interface MainInteractor {

    fun getBalance(update: Boolean): Observable<Balance>

}
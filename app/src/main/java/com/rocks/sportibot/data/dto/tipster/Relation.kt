package com.rocks.sportibot.data.dto.tipster

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.BettingMode
import com.rocks.sportibot.data.dto.Currency
import java.io.Serializable

/**
 * @param id id of the relation
 * @param customerId id of the customer. Model [com.rocks.sportibot.data.dto.user.User]
 * @param tipsterId id of the tipster. Model [com.rocks.sportibot.data.dto.user.User]
 * @param startsAt starts date. Format is [com.rocks.sportibot.utils.extensions.serverDatePattern]
 * @param endsAt ends date. Format is [com.rocks.sportibot.utils.extensions.serverDatePattern]
 * @param limit ?
 * @param stake stake per Unit?
 * @param type ?
 * @param currency `getCurrency()` of money
 * @param alert alert message
 * @param bettingMode bet mode
 * @param createdAt created relation date. Format is [com.rocks.sportibot.utils.extensions.serverDatePattern]
 * */
data class Relation(
        @SerializedName("id") val id: Long,
        @SerializedName("customerId", alternate = ["customer_id"]) val customerId: Long,
        @SerializedName("tipsterId", alternate = ["tipster_id"]) val tipsterId: Long,
        @SerializedName("startsAt", alternate = ["starts_at"]) val startsAt: String?,
        @SerializedName("endsAt", alternate = ["ends_at"]) val endsAt: String?,
        @SerializedName("limit") val limit: Double?,
        @SerializedName("stake") val stake: Double?,
        @SerializedName("type") val type: String?,
        @SerializedName("currency") val currency: Currency?,
        @SerializedName("alert") val alert: String?,
        @SerializedName("bettingMode") val bettingMode: BettingMode?,
        @SerializedName("createdAt", alternate = ["created_at"]) val createdAt: String?
) : Serializable {

    @SerializedName("apiName", alternate = ["api_name"])
    val apiName: String? = null
        get() = field ?: "Asian Odds" //todo check the default api name
}
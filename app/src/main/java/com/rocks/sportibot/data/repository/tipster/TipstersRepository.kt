package com.rocks.sportibot.data.repository.tipster

import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.data.dto.user.User
import io.reactivex.Single

interface TipstersRepository {

    /**
     * Return all tipsters list
     * */
    fun getMyTipstersAsync(): Single<List<Tipster>>

    /**
     * Return my tipsters list
     * */
    fun getAllTipstersAsync(): Single<List<Tipster>>

    /**
     * Unsubscribe the tipster
     * @param tipster to unsubscribe
     * @return [Tipster]
     * */
    fun followTipster(tipster: Tipster): Single<Tipster>

    /**
     * Unsubscribe the tipster
     * @param tipster to unsubscribe
     * @return [Tipster]
     * */
    fun unfollowTipster(tipster: Tipster): Single<Tipster>

    /**
     * @param tipsterId - user id from tipster [User.id]
     * @return [Tipster] by id
     * */
    fun getTipster(tipsterId: Long): Single<Tipster>

}
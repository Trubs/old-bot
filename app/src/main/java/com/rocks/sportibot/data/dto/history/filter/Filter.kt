package com.rocks.sportibot.data.dto.history.filter

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.utils.extensions.minus
import com.rocks.sportibot.utils.extensions.toDateDisplayFormat
import com.rocks.sportibot.utils.extensions.toDateFormat
import com.rocks.sportibot.utils.extensions.toServerFormat
import java.io.Serializable
import java.util.*

data class Filter(
        @SerializedName("showBets") var showBetsHistoryEnum: ShowBetsHistoryEnum,
        @SerializedName("dateFrom") private var dateFromString: String,
        @SerializedName("dateUntil") private var dateUntilString: String,
        @SerializedName("sortBy") var sortHistoryEnum: SortHistoryEnum
) : Serializable {
    var dateFrom
        get() = dateFromString.toDateFormat()
        set(value) {
            dateFromString = value.toServerFormat()
        }

    var dateUntil: Date
        get() = dateUntilString.toDateFormat()
        set(value) {
            dateUntilString = value.toServerFormat()
        }

    companion object {

        fun default(): Filter {
            val currentDate = Date()
            val minusMonthDate = currentDate.minus(Calendar.MONTH, 1)

            return Filter(ShowBetsHistoryEnum.RUNNING,
                    minusMonthDate.toServerFormat(),
                    currentDate.toServerFormat(),
                    SortHistoryEnum.DATE)
        }
    }

    fun getDisplayDateFrom(): String = dateFrom.toDateDisplayFormat()
    fun getDisplayDateUntil(): String = dateUntil.toDateDisplayFormat()
}
package com.rocks.sportibot.data.request.bets

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.request.TokenRequest

class PlaceBetRequest(
        accessToken: String?,
        @SerializedName("id") val betId: Long,
        @SerializedName("amount") val amount: Float
) : TokenRequest(accessToken)
package com.rocks.sportibot.data.dto

import com.google.gson.annotations.SerializedName

enum class BettingMode {

    @SerializedName("manual")
    MANUAL,

    @SerializedName("auto")
    AUTO
}
package com.rocks.sportibot.data.request.user

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.request.TokenRequest

class UserGetRequest(accessToken: String?,
                     @SerializedName("id") val userId: Long
) : TokenRequest(accessToken) {

    override fun toString(): String {
        return "UserGetRequest(userId=$userId)"
    }
}
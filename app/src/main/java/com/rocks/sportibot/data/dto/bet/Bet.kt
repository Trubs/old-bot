package com.rocks.sportibot.data.dto.bet

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.Currency
import com.rocks.sportibot.data.dto.game.Game
import com.rocks.sportibot.data.dto.tipster.Tipster
import java.io.Serializable

open class Bet(
        @SerializedName("id") val id: Long,
        @SerializedName("range") val range: String?,
        @SerializedName("odd") val odd: Float,
        @SerializedName("amount") val amount: Float,
        @SerializedName("units") val units: Float,
        @SerializedName("currency") val currency: Currency? = Currency.USD,
        @SerializedName("state") val state: State?,
        @SerializedName("createdAt") val createdAt: String?,//"yyyy-MM-dd hh:mm:ss"
        @SerializedName("game") val game: Game?,
        @SerializedName("tipster") val tipster: Tipster?,
        @SerializedName("comment") val comment: String?
) : Serializable {

    @SerializedName("minAmount")
    val minAmount: Float = 0.0f
        get() = if (field == 0.0f) 5.0f else field

    fun getUnitFractions() = String.format("%s/%s", units, (amount / units))

    fun getRangePair(): Pair<Float, Float> {
        val defaultPair = Pair(0.0f, 0.0f)

        val arrayRange = range?.split("-") ?: return defaultPair

        return if (arrayRange.size > 1) {
            val min = try {
                arrayRange[0].toFloat()
            } catch (e: NumberFormatException) {
                e.printStackTrace()
                0.0f
            }

            val max = try {
                arrayRange[1].toFloat()
            } catch (e: NumberFormatException) {
                e.printStackTrace()
                0.0f
            }

            Pair(min, max)
        } else {
            defaultPair
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Bet

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }


}
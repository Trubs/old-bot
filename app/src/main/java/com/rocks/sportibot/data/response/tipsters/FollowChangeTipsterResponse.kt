package com.rocks.sportibot.data.response.tipsters

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.data.response.ResultResponse
import java.io.Serializable

class FollowChangeTipsterResponse(
        @SerializedName("tipster") val tipster: Tipster
) : ResultResponse(),
        Serializable
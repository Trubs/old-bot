package com.rocks.sportibot.data.response.bets

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.data.response.ResultResponse

class BetsResponse(
        @SerializedName("bets") val list: List<Bet>
) : ResultResponse()
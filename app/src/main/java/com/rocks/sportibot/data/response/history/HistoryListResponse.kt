package com.rocks.sportibot.data.response.history

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.history.History
import com.rocks.sportibot.data.response.ResultResponse

class HistoryListResponse(
        @SerializedName("records") private val records: List<History>?
) : ResultResponse() {

    fun getHistoryList() = records.orEmpty()
}

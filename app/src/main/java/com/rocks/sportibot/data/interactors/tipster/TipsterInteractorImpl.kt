package com.rocks.sportibot.data.interactors.tipster

import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.data.dto.tipster.filter.Filter
import com.rocks.sportibot.data.dto.tipster.filter.TipsterStatusEnum
import com.rocks.sportibot.data.managers.preferences.PreferenceManager
import com.rocks.sportibot.data.repository.tipster.TipstersRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

class TipsterInteractorImpl(
        private val tipstersRepository: TipstersRepository,
        private val preferenceManager: PreferenceManager
) : TipsterInteractor {
    private val TAG: String = TipsterInteractorImpl::class.java.simpleName
    private val KEY_TIPSTER_FILTER = "$TAG-EXTRA_KEY_TIPSTER_FILTER"
    private val tipsterStatePubSubject = PublishSubject.create<Tipster>()

    private var filterLocal: Filter
        get() = preferenceManager.getData(KEY_TIPSTER_FILTER, Filter(), Filter::class.java)
        set(value) {
            preferenceManager.setData(KEY_TIPSTER_FILTER, value)
        }

    override fun getFilter(): Filter {
        return preferenceManager.getData(KEY_TIPSTER_FILTER, Filter(), Filter::class.java)
    }

    override fun getMyTipstersAsync(): Single<List<Tipster>> {
        return tipstersRepository.getMyTipstersAsync()
                .map { getFilteredTipsters(it) }
    }

    override fun getAllTipstersAsync(): Single<List<Tipster>> {
        return tipstersRepository.getAllTipstersAsync()
                .map { getFilteredTipsters(it) }
    }

    override fun followTipster(tipster: Tipster): Single<Tipster> {
        return manageTipsterFollow(tipstersRepository.followTipster(tipster))
    }

    override fun unFollowTipster(tipster: Tipster): Single<Tipster> {
        return manageTipsterFollow(tipstersRepository.unfollowTipster(tipster))
    }

    override fun registerTipsterUpdate(): Observable<Tipster> {
        return tipsterStatePubSubject
    }

    override fun applyFilter(filter: Filter): Boolean {
        val isChanged = this.filterLocal != filter

        this.filterLocal = filter

        return isChanged
    }

    private fun manageTipsterFollow(function: Single<Tipster>): Single<Tipster> {
        return function.map {

            tipsterStatePubSubject.onNext(it)

            return@map it
        }
    }

    private fun getFilteredTipsters(it: List<Tipster>): List<Tipster>? {
        return it.filter {
            when (filterLocal.status) {
                TipsterStatusEnum.ALL -> true
                TipsterStatusEnum.FREE -> !it.isPremium()
                TipsterStatusEnum.PREMIUM -> it.isPremium()
            }
        }

    }
}
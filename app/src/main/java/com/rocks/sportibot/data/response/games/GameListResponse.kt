package com.rocks.sportibot.data.response.games

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.game.Game
import com.rocks.sportibot.data.response.ResultResponse

class GameListResponse(
        @SerializedName("games") private val games: List<Game>?
) : ResultResponse() {
    fun getGameList() = games.orEmpty()
}

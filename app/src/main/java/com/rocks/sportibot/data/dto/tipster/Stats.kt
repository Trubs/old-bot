package com.rocks.sportibot.data.dto.tipster

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Stats(
        @SerializedName("followers") val followers: Int,
        @SerializedName("units") val units: Int,
        @SerializedName("tips") val tips: Int,
        @SerializedName("yield") val yield: Int,
        @SerializedName("averageProfit", alternate = ["average_profit"]) val averageProfit: Int
) : Serializable
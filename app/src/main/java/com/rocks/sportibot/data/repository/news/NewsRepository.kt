package com.rocks.sportibot.data.repository.news

import com.rocks.sportibot.data.dto.news.News
import io.reactivex.Single

interface NewsRepository {

    /**
     * Return news list
     * */
    fun getNewsAsync(): Single<List<News>>

}
package com.rocks.sportibot.data.dto.user

data class UserApiData(
        val username: String,
        val password: String
)
package com.rocks.sportibot.data.mapper

import com.google.gson.Gson
import com.rocks.sportibot.data.managers.HttpUnauthorizionManager
import com.rocks.sportibot.data.response.ResultResponse
import com.rocks.sportibot.utils.ConnectionUtil
import com.voodoo.rocks.aged_calendar.model.data.exception.ApiResponseException
import com.voodoo.rocks.aged_calendar.model.data.exception.NoInternetException
import com.voodoo.rocks.aged_calendar.model.data.exception.UnknownException
import com.voodoo.rocks.aged_calendar.model.dto.apiexception.ApiException
import retrofit2.Response
import retrofit2.adapter.rxjava2.Result

class ApiResponseMapper
constructor(
        private val httpUnauthorizionManager: HttpUnauthorizionManager,
        private val connectionUtil: ConnectionUtil
) {

    private fun handleInternetException(throwable: Throwable?) {
        if (!connectionUtil.isOnline) {
            throw NoInternetException()
        } else {
            throw throwable ?: ApiResponseException(throwable)
        }
    }

    private fun <R : ResultResponse> handleHttpException(response: Response<R>?) {

        httpUnauthorizionManager.handleException(response)

        var errorMessage: String
        try {
            val json = response!!.errorBody()!!.string()
            val gson = Gson()
            val resultResponse = gson.fromJson(json, ResultResponse::class.java)
            errorMessage = resultResponse.exception!!.message ?: UnknownException.ERROR
        } catch (e: Exception) {
            // There is no message in error body. Take raw message.
            errorMessage = String.format("%d %s", response!!.code(), response.raw().message())
        }

        throw ApiResponseException(errorMessage)
    }

    private fun handleResultException(exception: ApiException?) {
        throw ApiResponseException(exception?.message ?: UnknownException.ERROR)
    }

    fun <R : ResultResponse> apply(responseResult: Result<R>): R {

        if (responseResult.isError) {
            // No internet, etc.
            handleInternetException(responseResult.error())
        } else if (!responseResult.response()!!.isSuccessful) {
            // HTTP Codes != (200 - 299)
            handleHttpException(responseResult.response())
        } else if (!responseResult.response()!!.body()!!.isSuccess) {
            // Soft api exceptions
            handleResultException(responseResult.response()!!.body()!!.exception)
        }
        return responseResult.response()?.body()!!
    }
}

package com.rocks.sportibot.data.dto.user

import com.google.gson.annotations.SerializedName

enum class Role {
    @SerializedName("customer")
    CUSTOMER
}
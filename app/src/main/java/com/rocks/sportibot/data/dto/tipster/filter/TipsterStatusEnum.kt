package com.rocks.sportibot.data.dto.tipster.filter

import com.google.gson.annotations.SerializedName

enum class TipsterStatusEnum {
    @SerializedName("all")
    ALL,
    @SerializedName("free")
    FREE,
    @SerializedName("premium")
    PREMIUM
}
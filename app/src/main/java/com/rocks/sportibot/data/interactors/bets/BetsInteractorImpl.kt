package com.rocks.sportibot.data.interactors.bets

import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.data.managers.notifications.AppNotificationManager
import com.rocks.sportibot.data.repository.bet.BetsRepository
import com.rocks.sportibot.data.repository.user.UserRepository
import com.rocks.sportibot.data.request.bets.BetsRequest
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

class BetsInteractorImpl(
        private val betsRepository: BetsRepository,
        private val userRepository: UserRepository,
        private val appNotificationManager: AppNotificationManager
) : BetsInteractor {


    override fun getBetsOpenAsync(): Single<List<Bet>> {
        return betsRepository.getBetsAsync(BetsRequest.State.OPEN)
    }

    override fun getBetsPlacedAsync(): Single<List<Bet>> {
        return betsRepository.getBetsAsync(BetsRequest.State.PLACED)
    }

    override fun getBetsSkipAsync(): Single<List<Bet>> {
        return betsRepository.getBetsAsync(BetsRequest.State.SKIP)
    }

    override fun placeBet(bet: Bet, amount: Float): Completable {
        return Completable.fromSingle(betsRepository
                .placeBet(bet, amount)
                .doOnSuccess { appNotificationManager.cancel(it.id.toInt()) }
                .flatMap { userRepository.processedToUpdateBalance(it) })
    }

    override fun skipBet(bet: Bet): Completable {
        return Completable.fromSingle(betsRepository
                .skipBet(bet)
                .doOnSuccess { appNotificationManager.cancel(it.id.toInt()) })
    }

    /**
     * @return bet that has been placed
     * */
    override fun getPlacedBetObservable(): Observable<Bet> = betsRepository.getPlacedBetObservable()

    /**
     * @return bet that has been skipped
     * */
    override fun getSkippedBetObservable(): Observable<Bet> = betsRepository.getSkippedBetObservable()

    /**
     * @return bet that has been added
     * */
    override fun getOpenBetAddedObservable(): Observable<Bet> = betsRepository.getOpenBetObservable()

}
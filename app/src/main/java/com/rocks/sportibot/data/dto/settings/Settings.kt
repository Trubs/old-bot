package com.rocks.sportibot.data.dto.settings

import com.rocks.sportibot.data.dto.Customer
import java.io.Serializable

data class Settings(
        val customer: Customer
) : Serializable
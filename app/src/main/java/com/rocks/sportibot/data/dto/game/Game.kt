package com.rocks.sportibot.data.dto.game

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.event.Event
import com.rocks.sportibot.utils.extensions.toDateTimeFormat
import com.rocks.sportibot.utils.extensions.toSimpleFormat
import java.io.Serializable

data class Game(
        @SerializedName("id") val id: Long,
        @SerializedName("reference") val reference: String?,
        @SerializedName("market") val market: String?,
        @SerializedName("key") val key: String?,
        @SerializedName("type") val type: String?,
        @SerializedName("odd") val odd: String?,
        @SerializedName("bookie") val bookie: String?,
        @SerializedName("isLive") val isLive: Int, // 0 or 1
        @SerializedName("title") val title: String?,
        @SerializedName("event") val event: Event?

) : Serializable {
    fun getName() = String
            .format("%s v %s",
                    event?.home.orEmpty(),
                    event?.away.orEmpty())

    fun getMarketFormatted(): String? {
        return when (market) {
            "O" -> "Goals"
            "H" -> "Handicap"
            else -> ""
        }
    }

    fun getOver(): CharSequence? {
        if (type == "Away") return "OneXTwo Away"
        return "$type $key ${getMarketFormatted()}"
    }

    fun isLive() = isLive == 1

    fun getGameInfo() = event?.league

    fun getStartTime() = event?.startsAt?.toDateTimeFormat()?.toSimpleFormat("dd.MM.yy - HH:mm")
}
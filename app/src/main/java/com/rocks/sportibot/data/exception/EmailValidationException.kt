package com.rocks.sportibot.data.exception

import com.voodoo.rocks.aged_calendar.model.data.exception.BaseException

class EmailValidationException : BaseException("Invalid email address")
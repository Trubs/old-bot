package com.rocks.sportibot.data.response.tipsters

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.data.response.ResultResponse

class TipstersAllResponse(
        @SerializedName("tipsters") val tipsters: List<Tipster>
) : ResultResponse()
package com.rocks.sportibot.data.dto.history.filter

import com.google.gson.annotations.SerializedName

enum class SortHistoryEnum {
    @SerializedName("date")
    DATE,
    @SerializedName("profit")
    PROFIT,
    @SerializedName("yield")
    YIELD
}
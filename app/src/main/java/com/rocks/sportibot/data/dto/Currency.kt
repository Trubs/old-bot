package com.rocks.sportibot.data.dto

import com.google.gson.annotations.SerializedName

enum class Currency {
    @SerializedName("eur", alternate = ["EUR"])
    EUR {
        override fun getSymbol(): String = "\u20ac"
    },
    @SerializedName("usd", alternate = ["USD"])
    USD {
        override fun getSymbol(): String = "$"
    },
    @SerializedName("gbp", alternate = ["GBP"])
    GBP {
        override fun getSymbol(): String = "\u00a3"
    },
    UNKNOWN {
        override fun getSymbol(): String = "?"
    };

    abstract fun getSymbol(): String

}
package com.rocks.sportibot.data.request.games

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.request.TokenRequest

class GamesListRequest(
        accessToken: String?,
        @SerializedName("query") val query: String? = null
) : TokenRequest(accessToken) {
    override fun toString(): String {
        return super.toString() + "GamesListRequest(query=$query)"
    }
}

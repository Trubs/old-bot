package com.rocks.sportibot.data.managers.preferences

import android.content.Context
import android.content.Context.MODE_PRIVATE
import com.google.gson.GsonBuilder
import java.io.Serializable

class PreferenceManagerImpl(context: Context) : PreferenceManager {

    val TAG: String = PreferenceManagerImpl::class.java.simpleName
    val PREFERENCES_NAME = "$TAG-SPORTIBOT_PREFERENCES"

    private val sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE)
    private val gson = GsonBuilder().create()

    override fun isContainsData(key: String): Boolean {
        return sharedPreferences.contains(key)
    }

    override fun <T> getData(key: String, clazz: Class<T>): T? {
        val dataString: String? = sharedPreferences.getString(key, null)
        return if (dataString != null) gson.fromJson(dataString, clazz)
        else null
    }

    override fun <T> getData(key: String, default: T, clazz: Class<T>): T {
        return getData(key, clazz) ?: default
    }

    override fun getFloat(key: String, default: Float): Float {
        return sharedPreferences.getFloat(key, default)
    }

    override fun setData(key: String, data: Serializable) {
        sharedPreferences.edit().putString(key, gson.toJson(data)).apply()
    }

    override fun setFloat(key: String, value: Float) {
        sharedPreferences.edit().putFloat(key, value).apply()
    }

    override fun clear(key: String) {
        sharedPreferences.edit().remove(key).apply()
    }

    override fun clearAll() {
        sharedPreferences.edit().clear().apply()
    }
}
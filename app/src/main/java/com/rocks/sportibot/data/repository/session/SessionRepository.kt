package com.rocks.sportibot.data.repository.session

import com.rocks.sportibot.data.dto.Customer

interface SessionRepository {

    /**
     * @return true if accessToken & user non null
     * otherwise false
     * */
    fun isSessionExist(): Boolean

    /**
     * Call only if isSessionExist()
     * @return [String] session token
     * otherwise NullPointerException
     * */
    fun getAccessToken(): String

    /**
     * if isSessionExist()
     * @return [Customer]
     * otherwise NullPointerException
     * */
    fun getAccount(): Customer

    /**
     * Save session to preferences
     * @param accessToken must not be null
     * @param customer must not be null
     * */
    fun setSession(accessToken: String, customer: Customer)

    /**
     * Save customer to preferences
     * @param customer must not be null
     * */
    fun setAccount(customer: Customer)

    /**
     * Logout user and clear data of current user
     * */
    fun logout()
}
package com.voodoo.rocks.aged_calendar.model.data.exception

class TestException(message: String) : RuntimeException(message) {
    init {
        /*ignore print to stack for testing*/
        System.setErr(java.io.PrintStream(object : java.io.OutputStream() {
            override fun write(i: Int) {}
        }))
    }
}

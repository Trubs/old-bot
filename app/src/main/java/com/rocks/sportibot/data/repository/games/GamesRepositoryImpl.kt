package com.rocks.sportibot.data.repository.games

import com.rocks.sportibot.data.api.Api
import com.rocks.sportibot.data.dto.game.Game
import com.rocks.sportibot.data.exception.SessionExistException
import com.rocks.sportibot.data.mapper.ApiResponseMapper
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.data.request.games.GamesListRequest
import io.reactivex.Single

class GamesRepositoryImpl(
        private val api: Api,
        private val apiResponseMapper: ApiResponseMapper,
        private val sessionRepository: SessionRepository
) : GamesRepository {

    /**
     * Return games list
     * @param query for search of the games may be null
     * */
    override fun getGamesList(query: String?): Single<List<Game>> {
        return checkAndMapSessionExists {
            api.getGamesList(GamesListRequest(sessionRepository.getAccessToken(), query))
                    .map { apiResponseMapper.apply(it) }
                    .map { it.getGameList() }
        }
    }

    /**
     * Check session exist
     * @param function return result with existing session
     * @return [SessionExistException] If session not exist
     * otherwise return result from the function
     * */
    private fun <T> checkAndMapSessionExists(function: () -> Single<T>): Single<T> {
        return if (!sessionRepository.isSessionExist()) Single.error(SessionExistException())
        else function.invoke()
    }
}
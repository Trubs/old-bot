package com.rocks.sportibot.data.repository.tipster

import com.rocks.sportibot.data.api.Api
import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.data.dto.user.User
import com.rocks.sportibot.data.exception.SessionExistException
import com.rocks.sportibot.data.mapper.ApiResponseMapper
import com.rocks.sportibot.data.repository.BaseRepository
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.data.request.tipsters.AllTipstersRequest
import com.rocks.sportibot.data.request.tipsters.FollowChangeTipsterRequest
import com.rocks.sportibot.data.request.tipsters.MyTipstersRequest
import com.rocks.sportibot.data.request.tipsters.TipsterGetRequest
import io.reactivex.Single

class TipstersRepositoryImpl(api: Api,
                             private val apiResponseMapper: ApiResponseMapper,
                             private val sessionRepository: SessionRepository
) : BaseRepository(api), TipstersRepository {

    /**
     * Return all tipsters list
     * */
    override fun getAllTipstersAsync(): Single<List<Tipster>> {
        return checkAndMapSessionExists {
            api.getAllTipsters(AllTipstersRequest(sessionRepository.getAccessToken()))
                    .map { apiResponseMapper.apply(it) }
                    .map { it.tipsters }
        }
    }

    /**
     * Return my tipsters list
     * */
    override fun getMyTipstersAsync(): Single<List<Tipster>> {
        return checkAndMapSessionExists {
            api.getMyTipsters(MyTipstersRequest(sessionRepository.getAccessToken()))
                    .map { apiResponseMapper.apply(it) }
                    .map { it.tipsters }
        }
    }

    /**
     * Unsubscribe the tipster
     * @param tipster to unsubscribe
     * @return [Tipster]
     * */
    override fun followTipster(tipster: Tipster): Single<Tipster> {
        return checkAndMapSessionExists {
            api.followTipster(FollowChangeTipsterRequest(sessionRepository.getAccessToken(), tipster.user?.id))
                    .map { apiResponseMapper.apply(it) }
                    .map { it.tipster }
        }
    }

    /**
     * Unsubscribe the tipster
     * @param tipster to unsubscribe
     * @return [Tipster]
     * */
    override fun unfollowTipster(tipster: Tipster): Single<Tipster> {
        return checkAndMapSessionExists {
            api.unfollowTipster(FollowChangeTipsterRequest(sessionRepository.getAccessToken(), tipster.user?.id))
                    .map { apiResponseMapper.apply(it) }
                    .map {
                        tipster.relation = null
                        tipster
                    }
        }
    }

    /**
     * @param tipsterId - user id from tipster [User.id]
     * @return [Tipster] by id
     * */
    override fun getTipster(tipsterId: Long): Single<Tipster> {
        return checkAndMapSessionExists {
            api.getTipsterById(TipsterGetRequest(sessionRepository.getAccessToken(), tipsterId))
                    .map { apiResponseMapper.apply(it) }
                    .map { it.tipster }
        }
    }

    /**
     * Check session exist
     * @param function return result with existing session
     * @return [SessionExistException] If session not exist
     * otherwise return result from the function
     * */
    private fun <T> checkAndMapSessionExists(function: () -> Single<T>): Single<T> {
        return if (!sessionRepository.isSessionExist()) Single.error(SessionExistException())
        else function.invoke()
    }
}
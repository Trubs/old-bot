package com.rocks.sportibot.data.interactors.settings

import com.rocks.sportibot.data.dto.Currency
import com.rocks.sportibot.data.dto.Customer
import com.rocks.sportibot.data.dto.user.UserApiData
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.data.repository.user.UserRepository
import io.reactivex.Completable
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class SettingsInteractorImpl(
        private val userRepository: UserRepository,
        private val sessionRepository: SessionRepository
) : SettingsInteractor {

    //todo save customer

    override fun getAccount(): Customer {
        return sessionRepository.getAccount()
    }

    override fun getAccountAsync(): Observable<Customer> {
        return userRepository.getAccount()
    }

    override fun onCurrencyChanged(currency: Currency) {
        val account = getAccount()
        account.setCurrency(currency)
        sessionRepository.setAccount(account)
    }

    override fun updateApiUser(userApiData: UserApiData): Completable {
        val single = userRepository
                .updateUserApi(userApiData)
                .flatMap {
                    userRepository.processedToUpdateBalance(it)
                }
        return Completable.fromSingle(single)
    }

    override fun logout(): Completable {
        return Completable.fromAction {
            sessionRepository.logout()
        }.delay(1, TimeUnit.SECONDS)
    }

}
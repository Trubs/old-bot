package com.rocks.sportibot.data.dto

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.user.User
import java.io.Serializable

class Customer(
        @SerializedName("timezone") val timezone: Int,
        @SerializedName("balance") val balance: Float,
        @SerializedName("currency") private var currency: Currency?,
        @SerializedName("isRestricted") val isRestricted: Int,
        @SerializedName("user") val user: User
) : Serializable {

    fun getCurrency(): Currency = if (currency == null) Currency.UNKNOWN else currency!!
    fun setCurrency(currency: Currency) {
        this.currency = currency
    }

    fun isActive() = isRestricted == 1
}
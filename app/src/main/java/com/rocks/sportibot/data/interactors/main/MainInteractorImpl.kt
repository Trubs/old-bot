package com.rocks.sportibot.data.interactors.main

import com.rocks.sportibot.data.dto.Balance
import com.rocks.sportibot.data.repository.user.UserRepository
import io.reactivex.Observable

class MainInteractorImpl(
        private val userRepository: UserRepository
) : MainInteractor {

    override fun getBalance(update: Boolean): Observable<Balance> {
        return userRepository.getBalance(update)
    }
}
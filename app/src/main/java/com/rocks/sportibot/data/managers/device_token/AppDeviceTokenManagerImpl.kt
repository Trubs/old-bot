package com.rocks.sportibot.data.managers.device_token

import com.crashlytics.android.Crashlytics
import com.rocks.sportibot.data.api.Api
import com.rocks.sportibot.data.mapper.ApiResponseMapper
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.data.request.user.RegisterDeviceRequest
import com.rocks.sportibot.utils.extensions.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

class AppDeviceTokenManagerImpl(
        private val api: Api,
        private val apiResponseMapper: ApiResponseMapper,
        private val sessionRepository: SessionRepository,
        private val compositeDisposable: CompositeDisposable
) : AppDeviceTokenManager {

    private var deviceToken: String? = null

    override fun onDeviceTokenUpdated(deviceToken: String?) {
        log_d("On device token updated: $deviceToken")

        this.deviceToken = deviceToken

        tryRegisterDevice()
    }

    override fun tryRegisterDevice() {
        registerDevice(deviceToken)
    }

    override fun registerDevice(deviceToken: String?) {
        if (deviceToken == null) {
            log_i(tag(), "device did'n registered, device token is NULL")
            return
        }

        if (!sessionRepository.isSessionExist()) {
            log_i("AccessToken doesn't exist")
            return
        }

        val accessToken: String = sessionRepository.getAccessToken()

        compositeDisposable.add(api
                .registerDevice(RegisterDeviceRequest(accessToken, deviceToken))
                .map { apiResponseMapper.apply(it) }
                .doOnSubscribe { log_i("Start updating device token") }
                .doOnSuccess { log_i("Device token updated successful") }
                .doOnError { log_e("Device token update error: ${it.message}") }
                .scheduleIOToMainThread()
                .subscribeBy(
                        onError = {
                            it.printStackTrace()
                            Crashlytics.logException(it)
                        }))

    }
}
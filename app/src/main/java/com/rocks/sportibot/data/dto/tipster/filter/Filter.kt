package com.rocks.sportibot.data.dto.tipster.filter

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Filter(
        @SerializedName("sortBy") var sortBy: SortTipstersByEnum = SortTipstersByEnum.BETS_PLAYED,
        @SerializedName("type") var status: TipsterStatusEnum = TipsterStatusEnum.ALL
) : Serializable {

    override fun toString(): String {
        return "Filter(sortBy=$sortBy, status=$status)"
    }
}
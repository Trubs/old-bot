package com.rocks.sportibot.data.repository.history

import com.rocks.sportibot.data.dto.history.History
import io.reactivex.Single

interface HistoryRepository {
    /**
     * Return history list
     * @param offset start position
     * @param limit max list size
     * */
    fun getHistoryList(offset: Int,
                       limit: Int): Single<List<History>>
}
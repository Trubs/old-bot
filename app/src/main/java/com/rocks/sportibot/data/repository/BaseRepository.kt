package com.rocks.sportibot.data.repository

import com.rocks.sportibot.data.api.Api

open class BaseRepository(val api: Api)
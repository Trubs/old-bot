package com.rocks.sportibot.data.request.tipsters

import com.rocks.sportibot.data.request.TokenRequest

abstract class TipstersRequest(
        accessToken: String?
) : TokenRequest(accessToken)
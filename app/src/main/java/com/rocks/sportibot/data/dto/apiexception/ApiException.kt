package com.voodoo.rocks.aged_calendar.model.dto.apiexception


import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ApiException : Serializable {
    @SerializedName("name")
    var name: String? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("code")
    var code: Int? = -1
    @SerializedName("type")
    var type: String? = null
    @SerializedName("file")
    var file: String? = null
    @SerializedName("line")
    var line: Int? = -1
    @SerializedName("stackTrace")
    var stackTrace: List<String>? = null


    override fun toString(): String {
        return "ApiException{" +
                "name='" + name + '\'' +
                ", message='" + message + '\'' +
                ", code=" + code +
                ", type='" + type + '\'' +
                ", file='" + file + '\'' +
                ", line=" + line +
                ", stackTrace=" + stackTrace +
                '}'
    }
}

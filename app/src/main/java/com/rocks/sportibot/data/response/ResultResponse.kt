package com.rocks.sportibot.data.response

import com.google.gson.annotations.SerializedName
import com.voodoo.rocks.aged_calendar.model.dto.apiexception.ApiException

open class ResultResponse {
    @SerializedName("success")
    var isSuccess: Boolean = false
        private set

    @SerializedName("exception")
    var exception: ApiException? = null

    constructor()

    constructor(success: Boolean) {
        this.isSuccess = success
    }

    constructor(success: Boolean, exception: ApiException) {
        this.isSuccess = success
        this.exception = exception
    }

}

package com.rocks.sportibot.data.interactors.news

import com.rocks.sportibot.data.dto.news.News
import io.reactivex.Single

interface NewsInteractor {

    fun getNewsAsync(): Single<List<News>>

}
package com.rocks.sportibot.data.repository.bet

import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.data.request.bets.BetsRequest
import io.reactivex.Observable
import io.reactivex.Single

interface BetsRepository {

    fun getBetsAsync(state: BetsRequest.State): Single<List<Bet>>

    fun placeBet(bet: Bet, amount: Float): Single<Bet>

    fun skipBet(bet: Bet): Single<Bet>

    /**
     * @return bet that has been placed
     * */
    fun getPlacedBetObservable(): Observable<Bet>

    /**
     * @return bet that has been skipped
     * */
    fun getSkippedBetObservable(): Observable<Bet>

    /**
     * @return bet that has been added
     * */
    fun getOpenBetObservable(): Observable<Bet>


    fun onBetAdded(bet: Bet)
}
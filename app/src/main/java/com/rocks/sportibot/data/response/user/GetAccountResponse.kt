package com.rocks.sportibot.data.response.user

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.Customer
import com.rocks.sportibot.data.response.ResultResponse
import java.io.Serializable

class GetAccountResponse(
        @SerializedName("customer") val customer: Customer
) : ResultResponse(), Serializable
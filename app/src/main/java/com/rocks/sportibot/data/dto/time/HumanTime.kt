package com.rocks.sportibot.data.dto.time

import java.io.Serializable

class HumanTime(
        val days: Int,
        val hours: Int,
        val minutes: Int,
        val seconds: Int
) : Serializable {
    fun isMoreThanMonth() = days > 30

    override fun toString(): String {
        return "HumanTime(days=$days, hours=$hours, minutes=$minutes, seconds=$seconds)"
    }
}
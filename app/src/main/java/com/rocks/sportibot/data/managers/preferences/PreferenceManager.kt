package com.rocks.sportibot.data.managers.preferences

import java.io.Serializable

interface PreferenceManager {

    /**
     * This method check data in the 'preferences' by the key
     * @param key - the key of value
     * @return true if contains
     * otherwise false
     * */
    fun isContainsData(key: String): Boolean

    /**
     * This method returned data from the 'preferences' by the key
     * @param key - the key of value
     * @return data if contains
     * otherwise null
     * */
    fun <T> getData(key: String, clazz: Class<T>): T?

    /**
     * This method returned data from the 'preferences' by the key
     * @param key - the key of value
     * @param default - the default data if the 'preferences' not contains data by the key
     * @return data if contains
     * otherwise returned default
     * */
    fun <T> getData(key: String, default: T, clazz: Class<T>): T


    /**
     * This method returned float from the 'preferences' by the key
     * */
    fun getFloat(key: String, default: Float): Float

    /**
     * This method saved data in the 'preferences' by the key
     * @param key - the key of value
     * @param data - serializable object
     * */
    fun setData(key: String, data: Serializable)

    /**
     * This method saved float in the 'preferences' by the key
     * @param key - the key of value
     * */
    fun setFloat(key: String, value: Float)

    /**
     * This method clear data in the 'preferences' by the key
     * @param key - the key of value
     * Cleared if contains
     * otherwise do nothing
     * */
    fun clear(key: String)

    /**
     * This method clear all data in the 'preferences'
     * Cleared if contains
     * otherwise do nothing
     * */
    fun clearAll()
}
package com.rocks.sportibot.data.dto.user

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class User(
        @SerializedName("id") val id: Long,
        @SerializedName("email") val email: String,
        @SerializedName("fullName", alternate = ["full_name"]) val username: String?,
        @SerializedName("role") val role: Role,
        @SerializedName("createdAt", alternate = ["created_at"]) val createdAt: String?,
        @SerializedName("isActive", alternate = ["is_active"]) val isActive: Int
) : Serializable {

    fun isActive(): Boolean = isActive == 1


}
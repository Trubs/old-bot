package com.rocks.sportibot.data.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SignInRequest(
        @SerializedName("email") val login: String,
        @SerializedName("password") val password: String
) : Serializable {
    override fun toString(): String {
        return "SignInRequest(email='$login', password='$password')"
    }
}
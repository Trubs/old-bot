package com.rocks.sportibot.data.request.tipsters

import java.io.Serializable

class AllTipstersRequest(
        accessToken: String?
) : TipstersRequest(accessToken), Serializable

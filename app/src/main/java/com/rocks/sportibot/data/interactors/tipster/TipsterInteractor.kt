package com.rocks.sportibot.data.interactors.tipster

import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.data.dto.tipster.filter.Filter
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.DisposableObserver

interface TipsterInteractor {

    fun getMyTipstersAsync(): Single<List<Tipster>>

    fun getAllTipstersAsync(): Single<List<Tipster>>

    fun followTipster(tipster: Tipster): Single<Tipster>

    fun unFollowTipster(tipster: Tipster): Single<Tipster>

    /**
     * Listen changes of the tipster
     * Notify [DisposableObserver.onNext] every time when a tipster has been updated
     * E.g. The user subscribes to the tipster -> [Tipster.isFollow] must be updated
     * @return Observable<Tipster>
     * */
    fun registerTipsterUpdate(): Observable<Tipster>

    /**
     * @return current [Filter] of bets
     * */
    fun getFilter(): Filter

    /**
     * Save getFilterLocal to preferences
     * @return true if the getFilterLocal has been changed
     * otherwise false
     * */
    fun applyFilter(filter: Filter): Boolean

}
package com.rocks.sportibot.data.repository.bet

import com.rocks.sportibot.data.api.Api
import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.data.exception.SessionExistException
import com.rocks.sportibot.data.mapper.ApiResponseMapper
import com.rocks.sportibot.data.repository.BaseRepository
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.data.request.bets.BetsRequest
import com.rocks.sportibot.data.request.bets.PlaceBetRequest
import com.rocks.sportibot.data.request.bets.SkipBetRequest
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

class BetsRepositoryImpl(api: Api,
                         private val apiResponseMapper: ApiResponseMapper,
                         private val sessionRepository: SessionRepository)
    : BaseRepository(api),
        BetsRepository {

    private val skippedBetSubject = PublishSubject.create<Bet>()

    private val placedBetSubject = PublishSubject.create<Bet>()

    private val openBetSubject = PublishSubject.create<Bet>()

    override fun getBetsAsync(state: BetsRequest.State): Single<List<Bet>> {
        return checkAndMapSessionExists {
            api.getBets(BetsRequest(sessionRepository.getAccessToken(), state))
                    .map { apiResponseMapper.apply(it) }
                    .map { it.list }
        }
    }

    override fun placeBet(bet: Bet, amount: Float): Single<Bet> {
        return checkAndMapSessionExists {
            api.placeBet(PlaceBetRequest(sessionRepository.getAccessToken(), bet.id, amount))
                    .map { apiResponseMapper.apply(it) }
                    .map { bet }
                    .doOnSuccess { placedBetSubject.onNext(it) }
        }
    }

    override fun skipBet(bet: Bet): Single<Bet> {
        return checkAndMapSessionExists {
            api.skipBet(SkipBetRequest(sessionRepository.getAccessToken(), bet.id))
                    .map { apiResponseMapper.apply(it) }
                    .map { bet }
                    .doOnSuccess { skippedBetSubject.onNext(it) }
        }
    }

    /**
     * @return bet that has been skipped
     * */
    override fun getPlacedBetObservable(): PublishSubject<Bet> = placedBetSubject

    /**
     * @return bet that has been skipped
     * */
    override fun getSkippedBetObservable(): PublishSubject<Bet> = skippedBetSubject

    /**
     * @return bet that has been added
     * */
    override fun getOpenBetObservable(): Observable<Bet> = openBetSubject

    override fun onBetAdded(bet: Bet) {
        openBetSubject.onNext(bet)
    }

    /**
     * Check session exist
     * @param function return result with existing session
     * @return [SessionExistException] If session not exist
     * otherwise return result from the function
     * */
    private fun <T> checkAndMapSessionExists(function: () -> Single<T>): Single<T> {
        return if (!sessionRepository.isSessionExist()) Single.error(SessionExistException())
        else function.invoke()
    }
}
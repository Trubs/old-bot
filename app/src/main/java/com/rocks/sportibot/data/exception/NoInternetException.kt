package com.voodoo.rocks.aged_calendar.model.data.exception

class NoInternetException : BaseException(ERROR_NO_INTERNET) {
    companion object {
        val ERROR_NO_INTERNET = "There is no internet connection."
    }
}

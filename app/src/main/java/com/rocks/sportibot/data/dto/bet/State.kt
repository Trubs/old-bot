package com.rocks.sportibot.data.dto.bet

import com.google.gson.annotations.SerializedName

enum class State {
    @SerializedName("open")
    OPEN,
    @SerializedName("placed")
    PLACED,
    @SerializedName("skipped")
    SKIP
}
package com.rocks.sportibot.data.dto.event

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.utils.extensions.toDateFormat
import com.rocks.sportibot.utils.extensions.toSimpleFormat
import java.io.Serializable

class Event(
        @SerializedName("id") val id: Long,
        @SerializedName("home") val home: String,
        @SerializedName("away") val away: String,
        @SerializedName("goal") val goal: Any?,
        @SerializedName("sport") val sport: Int, //0 or 1
        @SerializedName("league") val league: String,
        @SerializedName("marketType") val marketType: Int,
        @SerializedName("startsAt", alternate = ["starts_at"]) val startsAt: String, //"yyyy-MM-dd hh:mm:ss"
        @SerializedName("createdAt", alternate = ["created_at"]) val createdAt: String, //"yyyy-MM-dd hh:mm:ss"
        @SerializedName("reference") val reference: String,
        @SerializedName("isActive") val isActive: Int //0 or 1
) : Serializable {
    fun getFormattedStartsTime() = startsAt.toDateFormat().toSimpleFormat("dd.MM.yy - HH:mm")
}
package com.rocks.sportibot.data.managers

import android.content.Context
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.data.response.ResultResponse
import com.rocks.sportibot.ui.login.sign_in.SignInActivity
import retrofit2.Response

class HttpUnauthorizionManager constructor(
        private val context: Context,
        private val sessionManager: SessionRepository
) {

    fun <R : ResultResponse> handleException(response: Response<R>?) {
        if (response?.code() == HTTP_CODE_UNAUTHORIZED) {
            sessionManager.logout()
            context.startActivity(SignInActivity.getIntentClearTask(context))
            throw Exception("User is not authorize")
        }
    }

    companion object {
        private val HTTP_CODE_UNAUTHORIZED = 401
        private val HTTP_CODE_FORBIDDEN = 403
    }

}
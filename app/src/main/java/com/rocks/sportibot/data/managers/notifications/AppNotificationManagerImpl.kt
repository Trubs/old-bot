package com.rocks.sportibot.data.managers.notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.support.annotation.DrawableRes
import android.support.v4.app.NotificationCompat
import com.rocks.sportibot.R

class AppNotificationManagerImpl(
        private val context: Context
) : AppNotificationManager {
    val TAG: String = AppNotificationManagerImpl::class.java.simpleName
    val CHANNEL_ID = "$TAG-SPORTIBOT"

    private val notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    override fun sendNotification(id: Int,
                                  channelType: String,
                                  title: String,
                                  message: String,
                                  intent: Intent) {
        val icon = R.drawable.logo

        notify(id, channelType, title, message, icon, icon, intent)
    }

    override fun sendNotification(
            id: Int,
            channelType: String,
            title: String,
            message: String,
            @DrawableRes iconSmall: Int,
            @DrawableRes iconLarge: Int,
            intent: Intent
    ) {
        notify(id, channelType, title, message, iconSmall, iconLarge, intent)
    }

    private fun notify(
            id: Int,
            channelType: String,
            title: String,
            message: String,
            @DrawableRes iconSmall: Int,
            @DrawableRes iconLarge: Int,
            intent: Intent
    ) {
        val builder = NotificationCompat.Builder(context, CHANNEL_ID)

        val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        builder.setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(iconSmall)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, iconLarge))
                .setContentIntent(pendingIntent)
                .setFullScreenIntent(pendingIntent, true)
                .setAutoCancel(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var channel = notificationManager.getNotificationChannel(channelType)
            if (channel == null) {
                channel = NotificationChannel(channelType, "", NotificationManager.IMPORTANCE_HIGH)
                notificationManager.createNotificationChannel(channel)
            }
        }

        notificationManager.notify(id, builder.build())
    }

    override fun cancel(id: Int) {
        notificationManager.cancel(id)
    }
}
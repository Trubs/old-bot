package com.rocks.sportibot.data.dto.tipster.filter

import com.google.gson.annotations.SerializedName

enum class SortTipstersByEnum {
    @SerializedName("betsPlayed")
    BETS_PLAYED,
    @SerializedName("yield")
    YIELD
}
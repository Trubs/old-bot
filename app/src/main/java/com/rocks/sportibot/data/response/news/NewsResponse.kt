package com.rocks.sportibot.data.response.news

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.news.News
import com.rocks.sportibot.data.response.ResultResponse

data class NewsResponse(
        @SerializedName("news") val news: List<News>
) : ResultResponse()
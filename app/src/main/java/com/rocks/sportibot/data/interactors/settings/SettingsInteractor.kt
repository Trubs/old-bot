package com.rocks.sportibot.data.interactors.settings

import com.rocks.sportibot.data.dto.Currency
import com.rocks.sportibot.data.dto.Customer
import com.rocks.sportibot.data.dto.user.UserApiData
import io.reactivex.Completable
import io.reactivex.Observable

interface SettingsInteractor {

    fun getAccount(): Customer

    fun getAccountAsync(): Observable<Customer>

    fun onCurrencyChanged(currency: Currency)

    fun logout(): Completable

    fun updateApiUser(userApiData: UserApiData): Completable
}
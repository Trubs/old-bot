package com.rocks.sportibot.data.repository.user

import com.rocks.sportibot.data.dto.Balance
import com.rocks.sportibot.data.dto.Customer
import com.rocks.sportibot.data.dto.user.UserApiData
import com.rocks.sportibot.data.response.ResultResponse
import io.reactivex.Observable
import io.reactivex.Single

interface UserRepository {

    fun signIn(login: String, password: String): Single<Customer>

    fun signUp(email: String, username: String, password: String): Single<Customer>

    fun restorePassword(email: String): Single<Boolean>

    /**
     * If [com.rocks.sportibot.data.repository.session.SessionRepository.isSessionExist]
     * @return [Customer]
     * otherwise [Throwable]
     * */
    fun getAccount(): Observable<Customer>

    /**
     * @param update flag to update or subscribe to changes
     * */
    fun getBalance(update: Boolean): Observable<Balance>

    /**
     * Method starts the balance update, and then continues without modifying
     * the chain.
     * Calls [UserRepository.getAccount]
     * Updates are made through subscriptions to [UserRepository.getBalance]
     * @param data - object to be returned
     * @return data wrapped in Single
     * */
    fun <T> processedToUpdateBalance(data: T): Single<T>

    fun updateUserApi(userApiData: UserApiData): Single<ResultResponse>
}
package com.rocks.sportibot.data.interactors.login

import com.rocks.sportibot.data.dto.user.User
import com.rocks.sportibot.ui.login.sign_in.SignInDto
import com.rocks.sportibot.ui.login.sign_up.SignUpDto
import io.reactivex.Single

interface LoginInteractor {

    fun singIn(signInDto: SignInDto): Single<User>

    fun singUp(signUpDto: SignUpDto): Single<User>

    fun restorePassword(email: String?): Single<Boolean>

}
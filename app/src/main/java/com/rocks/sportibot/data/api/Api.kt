package com.rocks.sportibot.data.api

import com.rocks.sportibot.data.request.SignInRequest
import com.rocks.sportibot.data.request.TokenRequest
import com.rocks.sportibot.data.request.bets.BetsRequest
import com.rocks.sportibot.data.request.bets.PlaceBetRequest
import com.rocks.sportibot.data.request.bets.SkipBetRequest
import com.rocks.sportibot.data.request.games.GamesListRequest
import com.rocks.sportibot.data.request.history.HistoryListRequest
import com.rocks.sportibot.data.request.sign_up.SignUpRequest
import com.rocks.sportibot.data.request.tipsters.AllTipstersRequest
import com.rocks.sportibot.data.request.tipsters.FollowChangeTipsterRequest
import com.rocks.sportibot.data.request.tipsters.MyTipstersRequest
import com.rocks.sportibot.data.request.tipsters.TipsterGetRequest
import com.rocks.sportibot.data.request.user.RegisterDeviceRequest
import com.rocks.sportibot.data.request.user.RestorePasswordRequest
import com.rocks.sportibot.data.request.user.UpdateApiDataRequest
import com.rocks.sportibot.data.request.user.UserGetRequest
import com.rocks.sportibot.data.response.ResultResponse
import com.rocks.sportibot.data.response.bets.BetsResponse
import com.rocks.sportibot.data.response.games.GameListResponse
import com.rocks.sportibot.data.response.history.HistoryListResponse
import com.rocks.sportibot.data.response.news.NewsResponse
import com.rocks.sportibot.data.response.tipsters.FollowChangeTipsterResponse
import com.rocks.sportibot.data.response.tipsters.TipsterGetResponse
import com.rocks.sportibot.data.response.tipsters.TipstersAllResponse
import com.rocks.sportibot.data.response.tipsters.TipstersMyResponse
import com.rocks.sportibot.data.response.user.GetAccountResponse
import com.rocks.sportibot.data.response.user.RefreshBalanceResponse
import com.rocks.sportibot.data.response.user.SignInResponse
import com.rocks.sportibot.data.response.user.SignUpResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.adapter.rxjava2.Result
import retrofit2.http.Body
import retrofit2.http.POST

interface Api {
    /*TODO add request methods*/

    /*News*/
    @POST("api/news/list")
    fun getNews(@Body tokenRequest: TokenRequest): Single<Result<NewsResponse>>

    /*Customers*/
    @POST("api/customers/sign-in")
    fun singIn(@Body signInRequest: SignInRequest): Single<Result<SignInResponse>>

    @POST("api/customers/register")
    fun singUp(@Body signUpRequest: SignUpRequest): Single<Result<SignUpResponse>>

    @POST("api/customers/get")
    fun getUser(@Body userGetRequest: UserGetRequest): Observable<Result<GetAccountResponse>>

    @POST("api/customers/restore-password")
    fun restorePassword(@Body restorePasswordRequest: RestorePasswordRequest): Single<Result<ResultResponse>>

    @POST("api/customers/refresh-balance")
    fun refreshBalance(@Body tokenRequest: TokenRequest): Single<Result<RefreshBalanceResponse>>

    @POST("api/customers/update")
    fun updateApi(@Body updateApiDataRequest: UpdateApiDataRequest): Single<Result<ResultResponse>>

    /*Devices*/
    @POST("api/devices/register")
    fun registerDevice(@Body registerDeviceRequest: RegisterDeviceRequest): Single<Result<ResultResponse>>

    @POST("api/devices/unregister")
    fun unregisterDevice(@Body tokenRequest: TokenRequest): Single<Result<ResultResponse>>

    /*Bets*/
    @POST("api/bets/list")
    fun getBets(@Body betsOpenRequest: BetsRequest): Single<Result<BetsResponse>>

    @POST("api/bets/place")
    fun placeBet(@Body placeBetRequest: PlaceBetRequest): Single<Result<ResultResponse>>

    @POST("api/bets/skip")
    fun skipBet(@Body skipBetRequest: SkipBetRequest): Single<Result<ResultResponse>>

    /*Tipsters*/
    @POST("api/tipsters/list")
    fun getAllTipsters(@Body allTipstersRequest: AllTipstersRequest): Single<Result<TipstersAllResponse>>

    @POST("api/tipsters/following")
    fun getMyTipsters(@Body myTipstersRequest: MyTipstersRequest): Single<Result<TipstersMyResponse>>

    @POST("api/tipsters/follow")
    fun followTipster(@Body followTipsterRequest: FollowChangeTipsterRequest): Single<Result<FollowChangeTipsterResponse>>

    @POST("api/tipsters/unfollow")
    fun unfollowTipster(@Body followChangeTipsterRequest: FollowChangeTipsterRequest): Single<Result<ResultResponse>>

    @POST("api/tipsters/get")
    fun getTipsterById(@Body tipsterGetRequest: TipsterGetRequest): Single<Result<TipsterGetResponse>>

    /*History*/
    @POST("api/history/list")
    fun getHistoryList(@Body historyListRequest: HistoryListRequest): Single<Result<HistoryListResponse>>

    /*Games*/
    @POST("api/games/list")
    fun getGamesList(@Body gamesListRequest: GamesListRequest): Single<Result<GameListResponse>>
}
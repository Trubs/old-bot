package com.rocks.sportibot.data.managers

import com.rocks.sportibot.ui.base.clicklisteners.filteractivate.OnFilterActivateClickListener
import com.rocks.sportibot.ui.base.interfaces.ViewFilterWindowPopupListener

class OnFilterActivateWindow(
        private val viewFilterWindowPopupListener: ViewFilterWindowPopupListener
) : OnFilterActivateClickListener {

    override fun onFilterActivateChanged(isActivated: Boolean) {
        if (isActivated) viewFilterWindowPopupListener.showPopupFilterWindow()
        else viewFilterWindowPopupListener.hidePopupFilterWindow()
    }
}
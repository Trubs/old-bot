package com.rocks.sportibot.data.interactors.history

import com.rocks.sportibot.data.dto.history.History
import com.rocks.sportibot.data.dto.history.filter.Filter
import io.reactivex.Single

interface HistoryInteractor {
    /**
     * @return current [Filter] of history
     * */
    fun getFilter(): Filter

    /**
     * Save getFilterLocal to preferences
     * @return true if the getFilterLocal has been changed
     * otherwise false
     * */
    fun applyFilter(filter: Filter): Boolean

    /**
     * Return history list
     * @param offset start position
     * @param limit max list size
     * */
    fun getHistoryList(offset: Int,
                       limit: Int): Single<List<History>>
}
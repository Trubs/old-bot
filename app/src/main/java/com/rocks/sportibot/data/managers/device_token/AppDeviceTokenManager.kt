package com.rocks.sportibot.data.managers.device_token

interface AppDeviceTokenManager {

    fun onDeviceTokenUpdated(deviceToken: String?)

    fun tryRegisterDevice()

    fun registerDevice(deviceToken: String?)
}
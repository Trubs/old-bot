package com.rocks.sportibot.data.exception

import com.voodoo.rocks.aged_calendar.model.data.exception.BaseException

class AgreementException : BaseException("You have to accept the terms")
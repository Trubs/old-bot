package com.voodoo.rocks.aged_calendar.model.data.exception

class UnknownException : BaseException(ERROR) {
    companion object {
        val ERROR = "Unknown error"
    }
}

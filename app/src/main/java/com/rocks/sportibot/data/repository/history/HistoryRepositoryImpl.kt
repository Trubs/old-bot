package com.rocks.sportibot.data.repository.history

import com.rocks.sportibot.data.api.Api
import com.rocks.sportibot.data.dto.history.History
import com.rocks.sportibot.data.exception.SessionExistException
import com.rocks.sportibot.data.mapper.ApiResponseMapper
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.data.request.history.HistoryListRequest
import io.reactivex.Single

class HistoryRepositoryImpl(
        private val api: Api,
        private val apiResponseMapper: ApiResponseMapper,
        private val sessionRepository: SessionRepository
) : HistoryRepository {

    /**
     * Return history list
     * @param offset start position
     * @param limit max list size
     * */
    override fun getHistoryList(offset: Int, limit: Int): Single<List<History>> {

        return checkAndMapSessionExists {
            api.getHistoryList(HistoryListRequest(sessionRepository.getAccessToken(),
                    offset, limit))
                    .map { apiResponseMapper.apply(it) }
                    .map { it.getHistoryList() }
        }
    }

    /**
     * Check session exist
     * @param function return result with existing session
     * @return [SessionExistException] If session not exist
     * otherwise return result from the function
     * */
    private fun <T> checkAndMapSessionExists(function: () -> Single<T>): Single<T> {
        return if (!sessionRepository.isSessionExist()) Single.error(SessionExistException())
        else function.invoke()
    }
}
package com.rocks.sportibot.data.dto

import java.io.Serializable

data class Balance(
        val balance: Float,
        var currency: Currency
) : Serializable
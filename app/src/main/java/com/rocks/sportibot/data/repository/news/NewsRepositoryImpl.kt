package com.rocks.sportibot.data.repository.news

import com.rocks.sportibot.data.api.Api
import com.rocks.sportibot.data.dto.news.News
import com.rocks.sportibot.data.exception.SessionExistException
import com.rocks.sportibot.data.mapper.ApiResponseMapper
import com.rocks.sportibot.data.repository.BaseRepository
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.data.request.TokenRequest
import io.reactivex.Single

class NewsRepositoryImpl(
        api: Api,
        private val apiResponseMapper: ApiResponseMapper,
        private val sessionRepository: SessionRepository
) : BaseRepository(api), NewsRepository {

    /**
     * Return news list
     * */
    override fun getNewsAsync(): Single<List<News>> {
        return checkAndMapSessionExists {
            api.getNews(TokenRequest(sessionRepository.getAccessToken()))
                    .map { apiResponseMapper.apply(it) }
                    .map { it.news }
        }
    }

    /**
     * Check session exist
     * @param function return result with existing session
     * @return [SessionExistException] If session not exist
     * otherwise return result from the function
     * */
    private fun <T> checkAndMapSessionExists(function: () -> Single<T>): Single<T> {
        return if (!sessionRepository.isSessionExist()) Single.error(SessionExistException())
        else function.invoke()
    }
}
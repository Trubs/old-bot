package com.rocks.sportibot.data.managers.notifications

import android.content.Intent
import android.support.annotation.DrawableRes

interface AppNotificationManager {


    fun sendNotification(
            id: Int,
            channelType: String,
            title: String,
            message: String,
            intent: Intent
    )

    fun sendNotification(
            id: Int,
            channelType: String,
            title: String,
            message: String,
            @DrawableRes iconSmall: Int,
            @DrawableRes iconLarge: Int,
            intent: Intent
    )

    fun cancel(id: Int)
}
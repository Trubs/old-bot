package com.rocks.sportibot.data.request.tipsters

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.request.TokenRequest
import java.io.Serializable

class FollowChangeTipsterRequest(
        accessToken: String,
        @SerializedName("id") val id: Long?
) : TokenRequest(accessToken), Serializable {
    override fun toString(): String {
        return super.toString() + "FollowChangeTipsterRequest(id=$id)"
    }
}

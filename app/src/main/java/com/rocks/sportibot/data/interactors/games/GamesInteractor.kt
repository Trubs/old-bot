package com.rocks.sportibot.data.interactors.games

import com.rocks.sportibot.data.dto.game.Game
import io.reactivex.Single

interface GamesInteractor {

    /**
     * Return games list
     * @param query for search of the games may be null
     * */
    fun getGamesList(query: String? = null): Single<List<Game>>

}
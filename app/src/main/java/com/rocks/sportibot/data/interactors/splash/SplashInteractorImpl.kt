package com.rocks.sportibot.data.interactors.splash

import com.rocks.sportibot.data.repository.user.UserRepository
import io.reactivex.Completable

class SplashInteractorImpl(
        private val userRepository: UserRepository
) : SplashInteractor {

    override fun tryLoadSession(): Completable {
        return Completable.fromObservable(userRepository.getAccount())
    }
}
package com.rocks.sportibot.data.request.history

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.request.TokenRequest

class HistoryListRequest(
        accessToken: String?,
        @SerializedName("offset") val offset: Int,
        @SerializedName("limit") val limit: Int
) : TokenRequest(accessToken)

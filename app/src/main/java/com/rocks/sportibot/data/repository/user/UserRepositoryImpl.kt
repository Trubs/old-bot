package com.rocks.sportibot.data.repository.user

import com.rocks.sportibot.data.api.Api
import com.rocks.sportibot.data.dto.Balance
import com.rocks.sportibot.data.dto.Customer
import com.rocks.sportibot.data.dto.user.UserApiData
import com.rocks.sportibot.data.exception.SessionExistException
import com.rocks.sportibot.data.managers.device_token.AppDeviceTokenManager
import com.rocks.sportibot.data.mapper.ApiResponseMapper
import com.rocks.sportibot.data.repository.BaseRepository
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.data.request.SignInRequest
import com.rocks.sportibot.data.request.TokenRequest
import com.rocks.sportibot.data.request.sign_up.SignUpRequest
import com.rocks.sportibot.data.request.sign_up.UserRegister
import com.rocks.sportibot.data.request.user.RestorePasswordRequest
import com.rocks.sportibot.data.request.user.UpdateApiDataRequest
import com.rocks.sportibot.data.request.user.UserGetRequest
import com.rocks.sportibot.data.response.ResultResponse
import com.rocks.sportibot.utils.extensions.getTimeZoneOffsetInMinutes
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject

class UserRepositoryImpl(
        api: Api,
        private val apiResponseMapper: ApiResponseMapper,
        private val sessionRepository: SessionRepository,
        private val appDeviceTokenManager: AppDeviceTokenManager
) : BaseRepository(api), UserRepository {

    private val balanceSubject = BehaviorSubject.create<Balance>()

    override fun signIn(login: String, password: String): Single<Customer> {
        return api.singIn(SignInRequest(login, password))
                .map { apiResponseMapper.apply(it) }
                .map {
                    sessionRepository.setSession(it.accessToken, it.customer)
                    it.customer
                }
                .doOnSuccess { appDeviceTokenManager.tryRegisterDevice() }
    }

    override fun signUp(email: String, username: String, password: String): Single<Customer> {
        val user = UserRegister(email, username, password)
        return api.singUp(SignUpRequest(user, getTimeZoneOffsetInMinutes()))
                .map { apiResponseMapper.apply(it) }
                .map {
                    sessionRepository.setSession(it.accessToken, it.customer)
                    it.customer
                }
                .doOnSuccess { appDeviceTokenManager.tryRegisterDevice() }
    }

    override fun getAccount(): Observable<Customer> {
        if (!sessionRepository.isSessionExist()) return Observable.error(SessionExistException())

        val token = sessionRepository.getAccessToken()
        val userId = sessionRepository.getAccount().user.id

        return api.getUser(UserGetRequest(token, userId))
                .map { apiResponseMapper.apply(it) }
                .map { it.customer }
                .doOnNext {
                    balanceSubject.onNext(Balance(it.balance, it.getCurrency()))
                }
    }

    override fun getBalance(update: Boolean): Observable<Balance> {
        return if (update) balanceSubject.startWith(refreshBalance())
        else balanceSubject
    }

    override fun restorePassword(email: String): Single<Boolean> {
        return api.restorePassword(RestorePasswordRequest(email))
                .map { apiResponseMapper.apply(it) }
                .map { it.isSuccess }
    }

    /**
     * Method starts the balance update, and then continues without modifying
     * the chain.
     * Updates are made through subscriptions to [UserRepository.getBalance]
     * @param data - object to be returned
     * @return data wrapped in Single
     * */
    override fun <T> processedToUpdateBalance(data: T): Single<T> {
        return Single.fromObservable(getAccount())
                .map { data }
    }

    override fun updateUserApi(userApiData: UserApiData): Single<ResultResponse> {
        val updateApiDataRequest = UpdateApiDataRequest(sessionRepository.getAccessToken(), userApiData)

        return api.updateApi(updateApiDataRequest)
                .map { apiResponseMapper.apply(it) }
    }

    private fun refreshBalance(): Observable<Balance> {
        val tokenRequest = TokenRequest(sessionRepository.getAccessToken())
        return api.refreshBalance(tokenRequest)
                .map { apiResponseMapper.apply(it) }
                .map { it.customer }
                .map { Balance(it.balance, it.getCurrency()) }
                .toObservable()
    }
}
package com.rocks.sportibot.data.dto.news

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class News(
        @SerializedName("title") val title: String,
        @SerializedName("message") val message: String,
        @SerializedName("image") val image: String?,
        @SerializedName("createdAt") val createdAt: String?
) : Serializable
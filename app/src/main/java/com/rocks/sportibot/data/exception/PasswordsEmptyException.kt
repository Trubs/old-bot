package com.rocks.sportibot.data.exception

import com.voodoo.rocks.aged_calendar.model.data.exception.BaseException

class PasswordsEmptyException : BaseException("The password can't be empty")
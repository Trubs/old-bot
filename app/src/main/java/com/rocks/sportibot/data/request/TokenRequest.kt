package com.rocks.sportibot.data.request

import com.google.gson.annotations.SerializedName

open class TokenRequest(
        @SerializedName("accessToken")
        val accessToken: String?
)
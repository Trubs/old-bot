package com.rocks.sportibot.data.request.user

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.request.TokenRequest
import java.io.Serializable

class RegisterDeviceRequest(
        accessToken: String?,
        @SerializedName("deviceToken") val deviceToken: String?,
        @SerializedName("platform") val platform: String = "android"
) : TokenRequest(accessToken), Serializable

package com.rocks.sportibot.data.response.tipsters

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.tipster.Tipster
import com.rocks.sportibot.data.response.ResultResponse
import java.io.Serializable

class TipstersMyResponse(
        @SerializedName("tipsters") val tipsters: List<Tipster>
) : ResultResponse(), Serializable

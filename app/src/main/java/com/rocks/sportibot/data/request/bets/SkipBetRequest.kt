package com.rocks.sportibot.data.request.bets

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.request.TokenRequest

class SkipBetRequest(
        accessToken: String?,
        @SerializedName("id") val betId: Long
) : TokenRequest(accessToken)
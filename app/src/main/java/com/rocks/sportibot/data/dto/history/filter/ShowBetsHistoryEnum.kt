package com.rocks.sportibot.data.dto.history.filter

import com.google.gson.annotations.SerializedName

enum class ShowBetsHistoryEnum {
    @SerializedName("running")
    RUNNING,
    @SerializedName("open")
    OPEN
}
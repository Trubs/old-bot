package com.rocks.sportibot.data.interactors.history

import com.rocks.sportibot.data.dto.history.History
import com.rocks.sportibot.data.dto.history.filter.Filter
import com.rocks.sportibot.data.interactors.settings.SettingsInteractorImpl
import com.rocks.sportibot.data.managers.preferences.PreferenceManager
import com.rocks.sportibot.data.repository.history.HistoryRepository
import io.reactivex.Single

class HistoryInteractorImpl(
        private val preferenceManager: PreferenceManager,
        private val historyRepository: HistoryRepository
) : HistoryInteractor {
    val TAG: String = SettingsInteractorImpl::class.java.simpleName
    val KEY_HISTORY_FILTER = "$TAG-KEY_HISTORY_FILTER"

    var localFilter: Filter
        get() = preferenceManager.getData(KEY_HISTORY_FILTER, Filter.default(), Filter::class.java)
        set(value) {
            preferenceManager.setData(KEY_HISTORY_FILTER, value)
        }

    /**
     * @return current [Filter] of history
     * */
    override fun getFilter(): Filter {
        return localFilter
    }

    /**
     * Save getFilterLocal to preferences
     * @return true if the getFilterLocal has been changed
     * otherwise false
     * */
    override fun applyFilter(filter: Filter): Boolean {
        val isChanged = this.localFilter != filter

        this.localFilter = filter

        return isChanged
    }

    /**
     * Return history list
     * @param offset start position
     * @param limit max list size
     * */
    override fun getHistoryList(offset: Int, limit: Int): Single<List<History>> =
            historyRepository.getHistoryList(offset, limit)
}
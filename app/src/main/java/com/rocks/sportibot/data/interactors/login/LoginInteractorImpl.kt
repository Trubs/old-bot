package com.rocks.sportibot.data.interactors.login

import com.rocks.sportibot.data.dto.user.User
import com.rocks.sportibot.data.exception.AgreementException
import com.rocks.sportibot.data.exception.EmailValidationException
import com.rocks.sportibot.data.exception.PasswordsEmptyException
import com.rocks.sportibot.data.exception.PasswordsMatchException
import com.rocks.sportibot.data.repository.user.UserRepository
import com.rocks.sportibot.ui.login.sign_in.SignInDto
import com.rocks.sportibot.ui.login.sign_up.SignUpDto
import com.rocks.sportibot.utils.extensions.isValidEmail
import io.reactivex.Single

class LoginInteractorImpl(
        private val userRepository: UserRepository
) : LoginInteractor {

    override fun singIn(signInDto: SignInDto): Single<User> {
        if (!signInDto.email.isValidEmail()) return Single.error(EmailValidationException())
        if (signInDto.password.isBlank()) return Single.error(PasswordsEmptyException())
        return userRepository.signIn(signInDto.email, signInDto.password)
                .map { it.user }
    }

    override fun singUp(signUpDto: SignUpDto): Single<User> {
        if (!signUpDto.email.isValidEmail()) return Single.error(EmailValidationException())
        if (!signUpDto.isCorrectPasswords()) return Single.error(PasswordsMatchException())
        if (!signUpDto.isAgreedTerms()) return Single.error(AgreementException())
        return userRepository.signUp(signUpDto.email, signUpDto.username, signUpDto.password)
                .map { it.user }
    }

    override fun restorePassword(email: String?): Single<Boolean> {
        if (email.isNullOrBlank()) return Single.error(EmailValidationException())
        return userRepository.restorePassword(email!!)
    }
}
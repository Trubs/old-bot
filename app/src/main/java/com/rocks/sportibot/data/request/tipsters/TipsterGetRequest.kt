package com.rocks.sportibot.data.request.tipsters

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.request.TokenRequest
import java.io.Serializable

class TipsterGetRequest(
        accessToken: String?,
        @SerializedName("id") val id: Long
) : TokenRequest(accessToken),
        Serializable
package com.rocks.sportibot.data.request.sign_up

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class UserRegister(
        @SerializedName("email") val email: String,
        @SerializedName("fullName") val username: String?,
        @SerializedName("password") val password: String?
) : Serializable
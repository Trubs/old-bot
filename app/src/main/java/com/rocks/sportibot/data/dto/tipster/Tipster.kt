package com.rocks.sportibot.data.dto.tipster

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.BettingMode
import com.rocks.sportibot.data.dto.user.User
import java.io.Serializable

open class Tipster(
        @SerializedName("isPremium", alternate = ["is_premium"]) private val isPremium: Int,
        @SerializedName("user") val user: User?,
        @SerializedName("stats") val stats: Stats?,
        @SerializedName("relation") var relation: Relation?
) : Serializable {

    fun isFollow(): Boolean {
        return relation != null
    }

    fun isPremium(): Boolean = isPremium == 1

    fun isAutobet(): Boolean = relation?.bettingMode == BettingMode.AUTO

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Tipster

        if (user != other.user) return false

        return true
    }

    override fun hashCode(): Int {
        return user?.hashCode() ?: 0
    }


}

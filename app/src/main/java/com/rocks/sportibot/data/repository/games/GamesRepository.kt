package com.rocks.sportibot.data.repository.games

import com.rocks.sportibot.data.dto.game.Game
import io.reactivex.Single

interface GamesRepository {

    /**
     * Return games list
     * @param query for search of the games may be null
     * */
    fun getGamesList(query: String?): Single<List<Game>>
}
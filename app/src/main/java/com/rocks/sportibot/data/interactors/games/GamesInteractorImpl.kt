package com.rocks.sportibot.data.interactors.games

import com.rocks.sportibot.data.dto.game.Game
import com.rocks.sportibot.data.repository.games.GamesRepository
import io.reactivex.Single

class GamesInteractorImpl(
        private val gamesRepository: GamesRepository
) : GamesInteractor {

    /**
     * Return games list
     * @param query for search of the games may be null
     * */
    override fun getGamesList(query: String?): Single<List<Game>> =
            gamesRepository.getGamesList(query)
}
package com.rocks.sportibot.data.response.user

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.response.ResultResponse

data class BankrollResponse(
        @SerializedName("bankroll") val bankroll: Float
) : ResultResponse()
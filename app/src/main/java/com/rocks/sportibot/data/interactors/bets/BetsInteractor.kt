package com.rocks.sportibot.data.interactors.bets

import com.rocks.sportibot.data.dto.bet.Bet
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface BetsInteractor {

    fun getBetsOpenAsync(): Single<List<Bet>>

    fun getBetsPlacedAsync(): Single<List<Bet>>

    fun getBetsSkipAsync(): Single<List<Bet>>

    fun placeBet(bet: Bet, amount: Float): Completable

    fun skipBet(bet: Bet): Completable

    /**
     * @return bet that has been placed
     * */
    fun getPlacedBetObservable(): Observable<Bet>

    /**
     * @return bet that has been skipped
     * */
    fun getSkippedBetObservable(): Observable<Bet>

    /**
     * @return bet that has been added
     * */
    fun getOpenBetAddedObservable(): Observable<Bet>

}
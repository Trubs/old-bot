package com.rocks.sportibot.data.interactors.splash

import io.reactivex.Completable

interface SplashInteractor {

    /**
     * Trying to load and update the user
     * if the session exists and the server response is successful
     * return onComplete
     * otherwise an exception (connection error, session error, etc.)
     * */
    fun tryLoadSession(): Completable

}
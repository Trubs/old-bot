package com.rocks.sportibot.data.repository.session

import com.rocks.sportibot.data.dto.Customer
import com.rocks.sportibot.data.managers.preferences.PreferenceManager

class SessionRepositoryImpl(
        private val preferenceManager: PreferenceManager
) : SessionRepository {
    val TAG: String = SessionRepositoryImpl::class.java.simpleName
    val KEY_ACCESS_TOKEN = "$TAG-KEY_ACCESS_TOKEN"
    val KEY_ACCOUNT = "$TAG-KEY_ACCOUNT"

    /**
     * @return true if accessToken & user non null
     * otherwise false
     * */
    override fun isSessionExist(): Boolean {
        return preferenceManager.isContainsData(KEY_ACCOUNT) && preferenceManager.isContainsData(KEY_ACCESS_TOKEN)
    }

    /**
     * Call only if isSessionExist()
     * @return [String] session token
     * otherwise NullPointerException
     * */
    override fun getAccessToken(): String {
        return preferenceManager.getData(KEY_ACCESS_TOKEN, String::class.java)!!
    }

    /**
     * if isSessionExist()
     * @return [Customer]
     * otherwise NullPointerException
     * */
    override fun getAccount(): Customer {
        return preferenceManager.getData(KEY_ACCOUNT, Customer::class.java)!!
    }

    /**
     * Save session to preferences
     * @param accessToken must not be null
     * @param customer must not be null
     * */
    override fun setSession(accessToken: String, customer: Customer) {
        preferenceManager.setData(KEY_ACCESS_TOKEN, accessToken)
        preferenceManager.setData(KEY_ACCOUNT, customer)
    }

    /**
     * Save customer to preferences
     * @param customer must not be null
     * */
    override fun setAccount(customer: Customer) {
        preferenceManager.setData(KEY_ACCOUNT, customer)
    }

    /**
     * Logout user and clear data of current user
     * */
    override fun logout() {
        preferenceManager.clearAll()
    }
}
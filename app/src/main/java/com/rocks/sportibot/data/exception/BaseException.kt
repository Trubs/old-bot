package com.voodoo.rocks.aged_calendar.model.data.exception

open class BaseException(ERROR: String) : RuntimeException(ERROR)

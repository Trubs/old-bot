package com.rocks.sportibot.data.request.user

import com.google.gson.annotations.SerializedName
import com.rocks.sportibot.data.dto.user.UserApiData
import com.rocks.sportibot.data.request.TokenRequest
import java.io.Serializable

class UpdateApiDataRequest(
        accessToken: String?,
        @SerializedName("api") val api: Api
) : TokenRequest(accessToken), Serializable {

    constructor(accessToken: String?, userApiData: UserApiData)
            : this(accessToken, Api(userApiData))

    class Api(
            @SerializedName("username") val username: String,
            @SerializedName("password") val password: String
    ) : Serializable {

        constructor(apiData: UserApiData) : this(apiData.username, apiData.password)
    }
}

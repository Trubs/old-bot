package com.rocks.sportibot.data.dto.history

import com.google.gson.annotations.SerializedName

class History(
        @SerializedName("id") val id: Long,
        @SerializedName("userId", alternate = ["user_id"]) val userId: Long,
        @SerializedName("type") val type: Type?,
        @SerializedName("title") val title: String?,
        @SerializedName("createdAt", alternate = ["created_at"]) val createdAt: String?
) {
    enum class Type {
        @SerializedName("invited")
        INVITED,

        @SerializedName("skipped")
        SKIPPED,

        @SerializedName("placed")
        PLACED
    }
}

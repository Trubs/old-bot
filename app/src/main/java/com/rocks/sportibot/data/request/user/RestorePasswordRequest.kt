package com.rocks.sportibot.data.request.user

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class RestorePasswordRequest(
        @SerializedName("email") val email: String
) : Serializable

package com.rocks.sportibot.di.subcomponents.main.news

import com.rocks.sportibot.data.interactors.news.NewsInteractor
import com.rocks.sportibot.data.interactors.news.NewsInteractorImpl
import com.rocks.sportibot.data.repository.news.NewsRepository
import com.rocks.sportibot.ui.main.news.NewsContract
import com.rocks.sportibot.ui.main.news.NewsPresenter
import com.rocks.sportibot.ui.main.news.details.NewsDetailsContract
import com.rocks.sportibot.ui.main.news.details.NewsDetailsPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@NewsScope
@Module
class NewsModule {

    //Presenters
    @NewsScope
    @Provides
    fun provideNewsPresenter(newsInteractor: NewsInteractor,
                             compositeDisposable: CompositeDisposable): NewsContract.Presenter {
        return NewsPresenter(newsInteractor, compositeDisposable)
    }

    @NewsScope
    @Provides
    fun provideNewsDetailsPresenter(newsInteractor: NewsInteractor,
                                    compositeDisposable: CompositeDisposable): NewsDetailsContract.Presenter {
        return NewsDetailsPresenter(newsInteractor, compositeDisposable)
    }

    //Interactors

    @NewsScope
    @Provides
    fun provideNewsInteractor(
            newsRepository: NewsRepository
    ): NewsInteractor {
        return NewsInteractorImpl(newsRepository)
    }
}
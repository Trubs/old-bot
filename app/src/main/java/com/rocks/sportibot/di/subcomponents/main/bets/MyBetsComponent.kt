package com.rocks.sportibot.di.subcomponents.main.bets

import com.rocks.sportibot.di.subcomponents.main.bets.details.BetDetailsComponent
import com.rocks.sportibot.di.subcomponents.main.bets.details.BetDetailsModule
import com.rocks.sportibot.ui.main.bets.open.OpenFragmentPage
import com.rocks.sportibot.ui.main.bets.placed.PlacedFragmentPage
import com.rocks.sportibot.ui.main.bets.skip.SkipFragmentPage
import dagger.Subcomponent

@MyBetsScope
@Subcomponent(modules = [
    (MyBetsModule::class)
])
interface MyBetsComponent {

    fun inject(openFragmentPage: OpenFragmentPage)

    fun inject(placedFragmentPage: PlacedFragmentPage)

    fun inject(skipFragmentPage: SkipFragmentPage)

    fun plusBetDetailsComponent(betDetailsModule: BetDetailsModule): BetDetailsComponent

}
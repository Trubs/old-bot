package com.rocks.sportibot.di.subcomponents.main.settings

import com.rocks.sportibot.ui.main.settings.SettingsFragment
import dagger.Subcomponent

@SettingScope
@Subcomponent(modules = [
    (SettingModule::class)
])
interface SettingsComponent {

    fun inject(settingsFragment: SettingsFragment)

}
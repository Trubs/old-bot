package com.rocks.sportibot.di.subcomponents.splash

import com.rocks.sportibot.ui.splash.SplashActivity
import dagger.Subcomponent

@SplashScope
@Subcomponent(modules = [
    (SplashModule::class)
])
interface SplashComponent {

    fun inject(splasActivity: SplashActivity)
}
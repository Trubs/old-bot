package com.rocks.sportibot.di.api


import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.rocks.sportibot.BuildConfig
import com.rocks.sportibot.R
import com.rocks.sportibot.data.api.Api
import com.rocks.sportibot.data.managers.HttpUnauthorizionManager
import com.rocks.sportibot.data.mapper.ApiResponseMapper
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.utils.ConnectionUtil
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Singleton
@Module
open class ApiModule {

    @Singleton
    @Provides
    protected fun provideHttpUnauthorizionManager(
            context: Context,
            sessionRepository: SessionRepository
    ): HttpUnauthorizionManager {
        return HttpUnauthorizionManager(context, sessionRepository)
    }

    @Singleton
    @Provides
    protected fun provideApiInterface(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }
//    @Singleton
//    @Provides
//    protected fun provideApiInterface(retrofit: Retrofit): Api {
//        return ApiDebug()
//    }

    @Singleton
    @Provides
    protected fun provideRetrofit(context: Context, gson: Gson, client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(context.getString(R.string.base_url))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
    }

    @Singleton
    @Provides
    protected fun provideGson(): Gson {
        return GsonBuilder().create()
    }

    @Singleton
    @Provides
    protected fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .connectTimeout(CONNECT_TIMEOUT_SECONDS.toLong(), TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT_SECONDS.toLong(), TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT_SECONDS.toLong(), TimeUnit.SECONDS)
                .apply {
                    if (BuildConfig.DEBUG) {
                        addInterceptor(loggingInterceptor)
                    }
                }
                .build()
    }

    @Singleton
    @Provides
    fun provideApiResponseMapper(httpUnauthorizionManager: HttpUnauthorizionManager,
                                 connectionUtil: ConnectionUtil): ApiResponseMapper {
        return ApiResponseMapper(httpUnauthorizionManager, connectionUtil)
    }

    companion object {
        private val TAG = "API"
        private val CONNECT_TIMEOUT_SECONDS = 60
        private val READ_TIMEOUT_SECONDS = 60
        private val WRITE_TIMEOUT_SECONDS = 60
    }

    @Singleton
    @Provides
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }
}

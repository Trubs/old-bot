package com.rocks.sportibot.di.subcomponents.main.bets.details

import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.data.interactors.bets.BetsInteractor
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.ui.main.details.BetDetailsContract
import com.rocks.sportibot.ui.main.details.BetDetailsPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@BetDetailsScope
@Module
class BetDetailsModule(
        private val bet: Bet
) {

    @BetDetailsScope
    @Provides
    fun provideBet(): Bet {
        return bet
    }

    @BetDetailsScope
    @Provides
    fun provideBetDetailsPresenter(bet: Bet,
                                   betsInteractor: BetsInteractor,
                                   sessionRepository: SessionRepository,
                                   compositeDisposable: CompositeDisposable): BetDetailsContract.Presenter {
        return BetDetailsPresenter(bet, betsInteractor, sessionRepository, compositeDisposable)
    }
}
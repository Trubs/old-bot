package com.rocks.sportibot.di.subcomponents.main.history

import com.rocks.sportibot.data.interactors.history.HistoryInteractor
import com.rocks.sportibot.data.interactors.history.HistoryInteractorImpl
import com.rocks.sportibot.data.managers.preferences.PreferenceManager
import com.rocks.sportibot.data.repository.history.HistoryRepository
import com.rocks.sportibot.ui.main.history.HistoryContract
import com.rocks.sportibot.ui.main.history.HistoryPresenter
import com.rocks.sportibot.ui.main.history.settings.SettingsContract
import com.rocks.sportibot.ui.main.history.settings.SettingsPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@HistoryScope
@Module
class HistoryModule {

    @HistoryScope
    @Provides
    fun provideHistoryPresenter(historyInteractor: HistoryInteractor,
                                compositeDisposable: CompositeDisposable): HistoryContract.Presenter {
        return HistoryPresenter(historyInteractor, compositeDisposable)
    }

    @HistoryScope
    @Provides
    fun provideSettingsPresenter(historyInteractor: HistoryInteractor): SettingsContract.Presenter {
        return SettingsPresenter(historyInteractor)
    }

    @HistoryScope
    @Provides
    fun provideHistoryInteractor(preferenceManager: PreferenceManager,
                                 historyRepository: HistoryRepository): HistoryInteractor {
        return HistoryInteractorImpl(preferenceManager, historyRepository)
    }
}
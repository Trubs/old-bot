package com.rocks.sportibot.di.subcomponents.main.history

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class HistoryScope
package com.rocks.sportibot.di.subcomponents.main.settings

import com.rocks.sportibot.data.interactors.settings.SettingsInteractor
import com.rocks.sportibot.data.interactors.settings.SettingsInteractorImpl
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.data.repository.user.UserRepository
import com.rocks.sportibot.ui.main.settings.SettingsContract
import com.rocks.sportibot.ui.main.settings.SettingsPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@SettingScope
@Module
class SettingModule {

    @SettingScope
    @Provides
    fun provideSettingsPresenter(
            settingsInteractor: SettingsInteractor,
            compositeDisposable: CompositeDisposable
    ): SettingsContract.Presenter {
        return SettingsPresenter(settingsInteractor, compositeDisposable)
    }

    //Interactors
    @SettingScope
    @Provides
    fun provideSettingsInteractor(
            userRepository: UserRepository,
            sessionRepository: SessionRepository
    ): SettingsInteractor {
        return SettingsInteractorImpl(userRepository, sessionRepository)
    }
}
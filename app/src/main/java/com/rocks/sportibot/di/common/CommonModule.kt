package com.rocks.sportibot.di.common

import android.content.Context
import com.rocks.sportibot.data.managers.preferences.PreferenceManager
import com.rocks.sportibot.data.managers.preferences.PreferenceManagerImpl
import com.rocks.sportibot.utils.ConnectionUtil
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton


@Module
class CommonModule {

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }

    @Singleton
    @Provides
    fun provideConnectionUtil(context: Context): ConnectionUtil {
        return ConnectionUtil(context)
    }

    @Singleton
    @Provides
    fun providePreferenceManager(context: Context): PreferenceManager {
        return PreferenceManagerImpl(context)
    }
}
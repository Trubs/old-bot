package com.rocks.sportibot.di.subcomponents.main

import com.rocks.sportibot.data.interactors.main.MainInteractor
import com.rocks.sportibot.data.interactors.main.MainInteractorImpl
import com.rocks.sportibot.data.repository.user.UserRepository
import com.rocks.sportibot.ui.main.MainContract
import com.rocks.sportibot.ui.main.MainPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@MainScope
@Module
class MainModule {

    //Presenters
    @MainScope
    @Provides
    fun provideMainPresenter(mainInteractor: MainInteractor,
                             compositeDisposable: CompositeDisposable): MainContract.Presenter {
        return MainPresenter(mainInteractor, compositeDisposable)
    }

    //Interactors
    @MainScope
    @Provides
    fun provideMainInteractor(userRepository: UserRepository): MainInteractor {
        return MainInteractorImpl(userRepository)
    }
}
package com.rocks.sportibot.di.subcomponents.main.history

import com.rocks.sportibot.ui.main.history.HistoryFragment
import com.rocks.sportibot.ui.main.history.settings.SettingsFragment
import dagger.Subcomponent


@HistoryScope
@Subcomponent(modules = [
    (HistoryModule::class)
])
interface HistoryComponent {

    fun inject(historyFragment: HistoryFragment)
    fun inject(settingsFragment: SettingsFragment)
}
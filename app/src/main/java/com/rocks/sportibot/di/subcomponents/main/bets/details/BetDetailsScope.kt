package com.rocks.sportibot.di.subcomponents.main.bets.details

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class BetDetailsScope
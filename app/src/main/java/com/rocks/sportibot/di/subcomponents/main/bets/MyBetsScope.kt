package com.rocks.sportibot.di.subcomponents.main.bets

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MyBetsScope
package com.rocks.sportibot.di.subcomponents.services

import dagger.Module

@ServicesScope
@Module
class ServicesModule
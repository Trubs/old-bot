package com.rocks.sportibot.di.subcomponents.services

import com.rocks.sportibot.services.MessagingService
import dagger.Subcomponent

@ServicesScope
@Subcomponent(modules = [
    (ServicesModule::class)
])
interface ServicesComponent {

    fun inject(messagingService: MessagingService)

}
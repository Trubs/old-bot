package com.rocks.sportibot.di.subcomponents.splash

import com.rocks.sportibot.data.interactors.splash.SplashInteractor
import com.rocks.sportibot.data.interactors.splash.SplashInteractorImpl
import com.rocks.sportibot.data.repository.user.UserRepository
import com.rocks.sportibot.ui.splash.SplashContract
import com.rocks.sportibot.ui.splash.SplashPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@SplashScope
@Module
class SplashModule {

    @SplashScope
    @Provides
    fun provideMainPresenter(
            splashInteractor: SplashInteractor,
            compositeDisposable: CompositeDisposable

    ): SplashContract.Presenter {

        return SplashPresenter(splashInteractor, compositeDisposable)
    }

    @SplashScope
    @Provides
    fun provideSplashInteractor(userRepository: UserRepository): SplashInteractor {
        return SplashInteractorImpl(userRepository)
    }
}
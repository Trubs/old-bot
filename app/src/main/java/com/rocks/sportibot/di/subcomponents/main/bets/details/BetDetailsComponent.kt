package com.rocks.sportibot.di.subcomponents.main.bets.details

import com.rocks.sportibot.ui.main.details.BetDetailsDialogFragment
import dagger.Subcomponent

@BetDetailsScope
@Subcomponent(modules = [
    (BetDetailsModule::class)
])
interface BetDetailsComponent {

    fun inject(betDetailsDialogFragment: BetDetailsDialogFragment)

}
package com.rocks.sportibot.di.subcomponents.main

import com.rocks.sportibot.di.subcomponents.main.bets.MyBetsComponent
import com.rocks.sportibot.di.subcomponents.main.bets.MyBetsModule
import com.rocks.sportibot.di.subcomponents.main.history.HistoryComponent
import com.rocks.sportibot.di.subcomponents.main.history.HistoryModule
import com.rocks.sportibot.di.subcomponents.main.news.NewsComponent
import com.rocks.sportibot.di.subcomponents.main.news.NewsModule
import com.rocks.sportibot.di.subcomponents.main.settings.SettingModule
import com.rocks.sportibot.di.subcomponents.main.settings.SettingsComponent
import com.rocks.sportibot.di.subcomponents.main.tipsters.TipsterComponent
import com.rocks.sportibot.di.subcomponents.main.tipsters.TipsterModule
import com.rocks.sportibot.ui.main.MainActivity
import dagger.Subcomponent

@MainScope
@Subcomponent(modules = [
    (MainModule::class)
])
interface MainComponent {

    fun inject(mainActivity: MainActivity)

    fun plusNewsComponent(newsModule: NewsModule): NewsComponent

    fun plusTipstersComponent(tipsterModule: TipsterModule): TipsterComponent

    fun plusMyBetsComponent(myBetsModule: MyBetsModule): MyBetsComponent

    fun plusHistoryComponent(historyModule: HistoryModule): HistoryComponent

    fun plusSettingsComponent(settingModule: SettingModule): SettingsComponent
}
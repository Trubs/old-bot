package com.rocks.sportibot.di.subcomponents.main.news

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class NewsScope
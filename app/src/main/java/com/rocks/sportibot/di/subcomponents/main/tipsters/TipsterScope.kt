package com.rocks.sportibot.di.subcomponents.main.tipsters

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class TipsterScope
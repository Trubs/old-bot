package com.rocks.sportibot.di.subcomponents.login

import com.rocks.sportibot.ui.login.sign_in.SignInActivity
import com.rocks.sportibot.ui.login.sign_in.forgot_password.ForgottDialogFragment
import com.rocks.sportibot.ui.login.sign_up.SignUpActivity
import dagger.Subcomponent

@LoginScope
@Subcomponent(modules = [
    (LoginModule::class)
])
interface LoginComponent {

    fun inject(signUpActivity: SignUpActivity)

    fun inject(signInActivity: SignInActivity)

    fun inject(forgottDialogFragment: ForgottDialogFragment)

}
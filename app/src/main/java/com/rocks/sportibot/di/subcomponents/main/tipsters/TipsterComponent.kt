package com.rocks.sportibot.di.subcomponents.main.tipsters

import com.rocks.sportibot.ui.main.tipsters.all.AllFragmentPage
import com.rocks.sportibot.ui.main.tipsters.my.MyFragmentPage
import com.rocks.sportibot.ui.main.tipsters.settings.FilterDialogFragment
import dagger.Subcomponent

@TipsterScope
@Subcomponent(modules = [
    (TipsterModule::class)
])
interface TipsterComponent {

    fun inject(myFragmentPage: MyFragmentPage)

    fun inject(allFragmentPage: AllFragmentPage)

    fun inject(filterDialogFragmentPage: FilterDialogFragment)
}
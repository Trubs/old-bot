package com.rocks.sportibot.di.subcomponents.main.settings

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class SettingScope
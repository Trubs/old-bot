package com.rocks.sportibot.di.providers

import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.di.app.AppComponent
import com.rocks.sportibot.di.subcomponents.login.LoginComponent
import com.rocks.sportibot.di.subcomponents.login.LoginModule
import com.rocks.sportibot.di.subcomponents.main.MainComponent
import com.rocks.sportibot.di.subcomponents.main.MainModule
import com.rocks.sportibot.di.subcomponents.main.bets.MyBetsComponent
import com.rocks.sportibot.di.subcomponents.main.bets.MyBetsModule
import com.rocks.sportibot.di.subcomponents.main.bets.details.BetDetailsComponent
import com.rocks.sportibot.di.subcomponents.main.bets.details.BetDetailsModule
import com.rocks.sportibot.di.subcomponents.main.history.HistoryComponent
import com.rocks.sportibot.di.subcomponents.main.history.HistoryModule
import com.rocks.sportibot.di.subcomponents.main.news.NewsComponent
import com.rocks.sportibot.di.subcomponents.main.news.NewsModule
import com.rocks.sportibot.di.subcomponents.main.settings.SettingModule
import com.rocks.sportibot.di.subcomponents.main.settings.SettingsComponent
import com.rocks.sportibot.di.subcomponents.main.tipsters.TipsterComponent
import com.rocks.sportibot.di.subcomponents.main.tipsters.TipsterModule
import com.rocks.sportibot.di.subcomponents.services.ServicesComponent
import com.rocks.sportibot.di.subcomponents.services.ServicesModule
import com.rocks.sportibot.di.subcomponents.splash.SplashComponent
import com.rocks.sportibot.di.subcomponents.splash.SplashModule

object ComponentProvider {

    private lateinit var appComponent: AppComponent


    fun init(appComponent: AppComponent) {

        ComponentProvider.appComponent = appComponent
    }

    fun getAppComponent(): AppComponent {
        return checkInitialized(appComponent)
    }

    private fun <T : Any> checkInitialized(component: T?): T {
        if (component == null) {
            throw ComponentProviderNotInitializedException()
        }
        return component
    }

    object Services {
        private var servicesComponent: ServicesComponent? = null

        @Synchronized
        fun getServicesComponent(): ServicesComponent {
            if (servicesComponent == null) {
                servicesComponent = getAppComponent().plusServicesModule(ServicesModule())
            }
            return servicesComponent!!
        }
    }

    object Splash {

        private var splashComponent: SplashComponent? = null

        @Synchronized
        fun getSplashComponent(): SplashComponent {
            if (splashComponent == null) {
                splashComponent = getAppComponent().plusSplashModule(SplashModule())
            }
            return splashComponent!!
        }

        fun clearSplashComponent() {
            splashComponent = null
        }

    }

    object Login {
        private var loginComponent: LoginComponent? = null

        @Synchronized
        fun getLoginComponent(): LoginComponent {
            if (loginComponent == null) {
                loginComponent = getAppComponent().plusLoginModule(LoginModule())
            }
            return loginComponent!!
        }

        fun clearLoginComponent() {
            loginComponent = null
        }
    }

    object Main {
        private var mainComponent: MainComponent? = null
        private var tipsterComponent: TipsterComponent? = null
        private var myBetsComponent: MyBetsComponent? = null
        private var historyComponent: HistoryComponent? = null
        private var settingsComponent: SettingsComponent? = null
        private var newsComponent: NewsComponent? = null
        private var betDetailsComponent: BetDetailsComponent? = null


        @Synchronized
        fun getMainComponent(): MainComponent {
            if (mainComponent == null) {
                mainComponent = ComponentProvider.getAppComponent().plusMainModule(MainModule())
            }
            return mainComponent!!
        }

        @Synchronized
        fun getNewsComponent(): NewsComponent {
            if (newsComponent == null) {
                newsComponent = getMainComponent().plusNewsComponent(NewsModule())
            }
            return newsComponent!!
        }

        @Synchronized
        fun getTipsterComponent(): TipsterComponent {
            if (tipsterComponent == null) {
                tipsterComponent = getMainComponent().plusTipstersComponent(TipsterModule())
            }
            return tipsterComponent!!
        }

        @Synchronized
        fun getMyBetsComponent(): MyBetsComponent {
            if (myBetsComponent == null) {
                myBetsComponent = getMainComponent().plusMyBetsComponent(MyBetsModule())
            }
            return myBetsComponent!!
        }

        @Synchronized
        fun getBetDetailsComponent(bet: Bet): BetDetailsComponent {
            if (betDetailsComponent == null) {
                betDetailsComponent = getMyBetsComponent().plusBetDetailsComponent(BetDetailsModule(bet))
            }
            return betDetailsComponent!!
        }

        @Synchronized
        fun getHistoryComponent(): HistoryComponent {
            if (historyComponent == null) {
                historyComponent = getMainComponent().plusHistoryComponent(HistoryModule())
            }
            return historyComponent!!
        }

        @Synchronized
        fun getSettingsComponent(): SettingsComponent {
            if (settingsComponent == null) {
                settingsComponent = getMainComponent().plusSettingsComponent(SettingModule())
            }
            return settingsComponent!!
        }

        fun clearBetDetailsComponent() {
            betDetailsComponent = null
        }

        fun clearMainComponent() {
            mainComponent = null
            tipsterComponent = null
            myBetsComponent = null
            historyComponent = null
            settingsComponent = null
        }
    }
}

package com.rocks.sportibot.di.subcomponents.main.tipsters

import com.rocks.sportibot.data.interactors.tipster.TipsterInteractor
import com.rocks.sportibot.data.interactors.tipster.TipsterInteractorImpl
import com.rocks.sportibot.data.managers.preferences.PreferenceManager
import com.rocks.sportibot.data.repository.tipster.TipstersRepository
import com.rocks.sportibot.ui.main.tipsters.all.AllContract
import com.rocks.sportibot.ui.main.tipsters.all.AllPresenter
import com.rocks.sportibot.ui.main.tipsters.my.MyContract
import com.rocks.sportibot.ui.main.tipsters.my.MyPresenter
import com.rocks.sportibot.ui.main.tipsters.settings.FilterContract
import com.rocks.sportibot.ui.main.tipsters.settings.FilterPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@TipsterScope
@Module
class TipsterModule {

    /*PRESENTERS*/
    @TipsterScope
    @Provides
    fun provideMyPresenter(tipsterInteractor: TipsterInteractor,
                           compositeDisposable: CompositeDisposable
    ): MyContract.Presenter {
        return MyPresenter(tipsterInteractor, compositeDisposable)
    }

    @TipsterScope
    @Provides
    fun provideAllPresenter(tipsterInteractor: TipsterInteractor,
                            compositeDisposable: CompositeDisposable
    ): AllContract.Presenter {
        return AllPresenter(tipsterInteractor, compositeDisposable)
    }

    //    @TipsterScope
    @Provides
    fun provideSettingsPresenter(tipsterInteractor: TipsterInteractor): FilterContract.Presenter {
        return FilterPresenter(tipsterInteractor)
    }
    /**/

    /*INTERACTORS*/
    @TipsterScope
    @Provides
    fun provideTipsterInteractor(tipstersRepository: TipstersRepository,
                                 preferenceManager: PreferenceManager): TipsterInteractor {
        return TipsterInteractorImpl(tipstersRepository, preferenceManager)
    }
    /**/
}
package com.rocks.sportibot.di.app

import com.rocks.sportibot.App
import com.rocks.sportibot.di.api.ApiModule
import com.rocks.sportibot.di.common.CommonModule
import com.rocks.sportibot.di.repository.RepositoryModule
import com.rocks.sportibot.di.subcomponents.login.LoginComponent
import com.rocks.sportibot.di.subcomponents.login.LoginModule
import com.rocks.sportibot.di.subcomponents.main.MainComponent
import com.rocks.sportibot.di.subcomponents.main.MainModule
import com.rocks.sportibot.di.subcomponents.services.ServicesComponent
import com.rocks.sportibot.di.subcomponents.services.ServicesModule
import com.rocks.sportibot.di.subcomponents.splash.SplashComponent
import com.rocks.sportibot.di.subcomponents.splash.SplashModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    (AppModule::class),
    (ApiModule::class),
    (RepositoryModule::class),
    (CommonModule::class)
])

interface AppComponent {

    fun inject(app: App)

    fun plusMainModule(mainModule: MainModule): MainComponent

    fun plusSplashModule(splashModule: SplashModule): SplashComponent

    fun plusLoginModule(loginModule: LoginModule): LoginComponent

    fun plusServicesModule(servicesModule: ServicesModule): ServicesComponent

}

package com.rocks.sportibot.di.subcomponents.login

import com.rocks.sportibot.data.interactors.login.LoginInteractor
import com.rocks.sportibot.data.interactors.login.LoginInteractorImpl
import com.rocks.sportibot.data.repository.user.UserRepository
import com.rocks.sportibot.ui.login.sign_in.SignInContract
import com.rocks.sportibot.ui.login.sign_in.SignInPresenter
import com.rocks.sportibot.ui.login.sign_in.forgot_password.ForgotPasswordPresenter
import com.rocks.sportibot.ui.login.sign_in.forgot_password.ForgottPasswordContract
import com.rocks.sportibot.ui.login.sign_up.SignUpContract
import com.rocks.sportibot.ui.login.sign_up.SignUpPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@LoginScope
@Module
class LoginModule {

    @LoginScope
    @Provides
    fun provideSignInPresenter(loginInteractor: LoginInteractor,
                               compositeDisposable: CompositeDisposable): SignInContract.Presenter {
        return SignInPresenter(loginInteractor, compositeDisposable)
    }

    @LoginScope
    @Provides
    fun provideSignUpPresenter(loginInteractor: LoginInteractor,
                               compositeDisposable: CompositeDisposable): SignUpContract.Presenter {
        return SignUpPresenter(loginInteractor, compositeDisposable)
    }

    @LoginScope
    @Provides
    fun provideLoginInteractor(userRepository: UserRepository): LoginInteractor {
        return LoginInteractorImpl(userRepository)
    }

    @Provides
    fun provideForgotPresenter(loginInteractor: LoginInteractor,
                               compositeDisposable: CompositeDisposable): ForgottPasswordContract.Presenter {
        return ForgotPasswordPresenter(loginInteractor, compositeDisposable)
    }

}
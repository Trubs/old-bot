package com.rocks.sportibot.di.repository

import com.rocks.sportibot.data.api.Api
import com.rocks.sportibot.data.managers.device_token.AppDeviceTokenManager
import com.rocks.sportibot.data.managers.preferences.PreferenceManager
import com.rocks.sportibot.data.mapper.ApiResponseMapper
import com.rocks.sportibot.data.repository.bet.BetsRepository
import com.rocks.sportibot.data.repository.bet.BetsRepositoryImpl
import com.rocks.sportibot.data.repository.games.GamesRepository
import com.rocks.sportibot.data.repository.games.GamesRepositoryImpl
import com.rocks.sportibot.data.repository.history.HistoryRepository
import com.rocks.sportibot.data.repository.history.HistoryRepositoryImpl
import com.rocks.sportibot.data.repository.news.NewsRepository
import com.rocks.sportibot.data.repository.news.NewsRepositoryImpl
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.data.repository.session.SessionRepositoryImpl
import com.rocks.sportibot.data.repository.tipster.TipstersRepository
import com.rocks.sportibot.data.repository.tipster.TipstersRepositoryImpl
import com.rocks.sportibot.data.repository.user.UserRepository
import com.rocks.sportibot.data.repository.user.UserRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideSessionRepository(preferenceManager: PreferenceManager): SessionRepository {
        return SessionRepositoryImpl(preferenceManager)
    }

    @Singleton
    @Provides
    fun provideTipstersRepository(api: Api,
                                  apiResponseMapper: ApiResponseMapper,
                                  sessionRepository: SessionRepository): TipstersRepository {
        return TipstersRepositoryImpl(api, apiResponseMapper, sessionRepository)
    }

    @Singleton
    @Provides
    fun provideBetsRepository(api: Api,
                              apiResponseMapper: ApiResponseMapper,
                              sessionRepository: SessionRepository): BetsRepository {
        return BetsRepositoryImpl(api, apiResponseMapper, sessionRepository)
    }

    @Singleton
    @Provides
    fun provideNewsRepository(
            api: Api,
            apiResponseMapper: ApiResponseMapper,
            sessionRepository: SessionRepository
    ): NewsRepository {
        return NewsRepositoryImpl(api, apiResponseMapper, sessionRepository)
    }

    @Singleton
    @Provides
    fun provideUserRepository(api: Api,
                              apiResponseMapper: ApiResponseMapper,
                              sessionRepository: SessionRepository,
                              appDeviceTokenManager: AppDeviceTokenManager): UserRepository {
        return UserRepositoryImpl(api, apiResponseMapper, sessionRepository, appDeviceTokenManager)
    }

    @Singleton
    @Provides
    fun provideHistoryRepository(api: Api,
                                 apiResponseMapper: ApiResponseMapper,
                                 sessionRepository: SessionRepository): HistoryRepository {
        return HistoryRepositoryImpl(api, apiResponseMapper, sessionRepository)
    }

    @Singleton
    @Provides
    fun provideGamesRepository(api: Api,
                               apiResponseMapper: ApiResponseMapper,
                               sessionRepository: SessionRepository): GamesRepository {
        return GamesRepositoryImpl(api, apiResponseMapper, sessionRepository)
    }
}
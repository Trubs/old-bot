package com.rocks.sportibot.di.providers

internal class ComponentProviderNotInitializedException : RuntimeException(ERROR) {
    companion object {
        private val ERROR = "DaggerManager not initialized yet. " + "You must call DaggerManager.getInstance().init(Application) before you can use it."
    }
}

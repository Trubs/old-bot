package com.rocks.sportibot.di.app

import android.app.Application
import android.content.Context
import com.rocks.sportibot.data.api.Api
import com.rocks.sportibot.data.managers.device_token.AppDeviceTokenManager
import com.rocks.sportibot.data.managers.device_token.AppDeviceTokenManagerImpl
import com.rocks.sportibot.data.managers.notifications.AppNotificationManager
import com.rocks.sportibot.data.managers.notifications.AppNotificationManagerImpl
import com.rocks.sportibot.data.mapper.ApiResponseMapper
import com.rocks.sportibot.data.repository.session.SessionRepository
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@Singleton
@Module
class AppModule constructor(private val application: Application) {

    @Singleton
    @Provides
    protected fun provideApplication(): Application {
        return application
    }

    @Singleton
    @Provides
    protected fun provideContext(): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    fun provideAppNotificationManager(context: Context): AppNotificationManager {
        return AppNotificationManagerImpl(context)
    }

    @Singleton
    @Provides
    fun provideAppDeviceTokenManager(api: Api,
                                     apiResponseMapper: ApiResponseMapper,
                                     sessionRepository: SessionRepository,
                                     compositeDisposable: CompositeDisposable): AppDeviceTokenManager {

        return AppDeviceTokenManagerImpl(api, apiResponseMapper, sessionRepository, compositeDisposable)
    }
}

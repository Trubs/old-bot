package com.rocks.sportibot.di.subcomponents.splash

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class SplashScope
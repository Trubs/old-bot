package com.rocks.sportibot.di.subcomponents.services

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ServicesScope
package com.rocks.sportibot.di.subcomponents.main.bets

import com.rocks.sportibot.data.interactors.bets.BetsInteractor
import com.rocks.sportibot.data.interactors.bets.BetsInteractorImpl
import com.rocks.sportibot.data.managers.notifications.AppNotificationManager
import com.rocks.sportibot.data.repository.bet.BetsRepository
import com.rocks.sportibot.data.repository.user.UserRepository
import com.rocks.sportibot.ui.main.bets.open.OpenContract
import com.rocks.sportibot.ui.main.bets.open.OpenPresenter
import com.rocks.sportibot.ui.main.bets.placed.PlacedContract
import com.rocks.sportibot.ui.main.bets.placed.PlacedPresenter
import com.rocks.sportibot.ui.main.bets.skip.SkipContract
import com.rocks.sportibot.ui.main.bets.skip.SkipPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@MyBetsScope
@Module
class MyBetsModule {

    /*PRESENTERS*/
    @MyBetsScope
    @Provides
    fun provideOpenPresenter(
            betsInteractor: BetsInteractor,
            compositeDisposable: CompositeDisposable
    ): OpenContract.Presenter {
        return OpenPresenter(betsInteractor,
                compositeDisposable)
    }

    @MyBetsScope
    @Provides
    fun providePlacedPresenter(
            betsInteractor: BetsInteractor,
            compositeDisposable: CompositeDisposable
    ): PlacedContract.Presenter {
        return PlacedPresenter(betsInteractor, compositeDisposable)
    }

    @MyBetsScope
    @Provides
    fun provideSkipPresenter(
            betsInteractor: BetsInteractor,
            compositeDisposable: CompositeDisposable
    ): SkipContract.Presenter {
        return SkipPresenter(betsInteractor, compositeDisposable)
    }
    /**/

    /*INTERACTORS*/

    @MyBetsScope
    @Provides
    fun provideBetInteractor(betsRepository: BetsRepository,
                             userRepository: UserRepository,
                             appNotificationManager: AppNotificationManager): BetsInteractor {
        return BetsInteractorImpl(betsRepository, userRepository, appNotificationManager)
    }
}
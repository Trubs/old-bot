package com.rocks.sportibot.di.subcomponents.main.news

import com.rocks.sportibot.ui.main.news.NewsFragment
import com.rocks.sportibot.ui.main.news.details.NewsDetailsActivity
import dagger.Subcomponent

@NewsScope
@Subcomponent(modules = [
    (NewsModule::class)
])
interface NewsComponent {

    fun inject(newsFragment: NewsFragment)

    fun inject(newsFragment: NewsDetailsActivity)
}
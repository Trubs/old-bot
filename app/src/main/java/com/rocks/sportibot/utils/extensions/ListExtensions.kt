package com.rocks.sportibot.utils.extensions

fun <E> MutableList<E>.replaceAll(list: List<E>) {
    clear()
    addAll(list)
}
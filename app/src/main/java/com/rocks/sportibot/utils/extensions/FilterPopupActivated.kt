package com.rocks.sportibot.utils.extensions

import com.rocks.sportibot.ui.base.interfaces.ViewFilterWindowPopupListener

fun ViewFilterWindowPopupListener.managePopupFilter(isActivated: Boolean) {
    if (isActivated) this.showPopupFilterWindow()
    else this.hidePopupFilterWindow()
}
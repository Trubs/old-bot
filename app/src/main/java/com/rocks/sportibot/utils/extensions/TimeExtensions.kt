package com.rocks.sportibot.utils.extensions

import android.content.Context
import com.rocks.sportibot.R
import com.rocks.sportibot.data.dto.time.HumanTime
import java.util.*

fun Date.getTimeAgoFrom(fromDate: Date): HumanTime {
    var different = this.getDifferenceFromDate(fromDate)

    val secondsInMilli = 1000
    val minutesInMilli = secondsInMilli * 60
    val hoursInMilli = minutesInMilli * 60
    val daysInMilli = hoursInMilli * 24

    val elapsedDays = different / daysInMilli
    different %= daysInMilli

    val elapsedHours = different / hoursInMilli
    different %= hoursInMilli

    val elapsedMinutes = different / minutesInMilli
    different %= minutesInMilli

    val elapsedSeconds = different / secondsInMilli

    return HumanTime(
            elapsedDays.toInt(),
            elapsedHours.toInt(),
            elapsedMinutes.toInt(),
            elapsedSeconds.toInt())
}

/**
 * From now
 * */
fun Date.getTimeAgoFrom(): HumanTime {
    return getTimeAgoFrom(Date())
}

fun Date.getTimeAgoFrom(context: Context): String? {
    val resources = context.resources

    val humanTime = this.getTimeAgoFrom()


    humanTime.apply {
        if (days > 30) {
            return resources.getString(R.string.time_more_than_month_ago)
        }
        if (days > 0) {
            return resources.getQuantityString(R.plurals.time_days_param_ago, days, days)
        }
        if (hours > 0) {
            return resources.getQuantityString(R.plurals.time_hours_param_ago, hours, hours)
        }
        if (minutes > 0) {
            return resources.getQuantityString(R.plurals.time_minutes_param_ago, minutes, minutes)
        }
        if (seconds > 0) {
            return resources.getQuantityString(R.plurals.time_seconds_param_ago, seconds, seconds)
        }
    }
    return null
}


/**
 * Calculate difference in millis between two dates
 * @return millis
 * */
fun Date.getDifferenceFromDate(date: Date): Long {

    val millis1 = this.time
    val millis2 = date.time

    return if (millis1 > millis2)
        millis1 - millis2
    else
        millis2 - millis1
}
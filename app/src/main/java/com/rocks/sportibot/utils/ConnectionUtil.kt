package com.rocks.sportibot.utils

import android.content.Context
import android.net.ConnectivityManager

class ConnectionUtil constructor(private val context: Context) {

    val isOnline: Boolean
        get() = isOnline(context)

    companion object {
        /**
         * Static method check the internet connection
         * @param context - context of the application ( activity or global context )
         * @return true if the device connected to the internet ( wifi, EDGE and etc. )
         * otherwise false
         * */
        fun isOnline(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val info = connectivityManager.activeNetworkInfo
            return info != null && info.isConnectedOrConnecting
        }
    }
}

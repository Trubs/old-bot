package com.rocks.sportibot.utils.extensions

import android.util.Log

fun Any.tag(): String = this::class.java.simpleName

fun Any.log_e(message: String?) = Log.e(tag(), message)
fun Any.log_e(tag: String, message: String?) = Log.e(tag, message)

fun Any.log_d(message: String?) = Log.d(tag(), message)
fun Any.log_d(tag: String, message: String?) = Log.d(tag, message)

fun Any.log_w(message: String?) = Log.w(tag(), message)
fun Any.log_w(tag: String, message: String?) = Log.w(tag, message)

fun Any.log_i(message: String?) = Log.i(tag(), message)
fun Any.log_i(tag: String, message: String?) = Log.i(tag, message)
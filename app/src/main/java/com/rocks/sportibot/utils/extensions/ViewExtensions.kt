package com.rocks.sportibot.utils.extensions

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rocks.sportibot.R

fun View?.setVisibleOrGone(visible: Boolean) {
    if (visible) this.visible()
    else this.gone()
}

fun View?.setVisibleOrInvisible(visible: Boolean) {
    if (visible) this.visible()
    else this.invisible()
}

fun View?.visible() {
    this?.visibility = VISIBLE
}

fun View?.invisible() {
    this?.visibility = GONE
}

fun View?.gone() {
    this?.visibility = GONE
}

fun ImageView.loadImageInto(image: String?,
                            @DrawableRes resPlaceHolder: Int? = R.drawable.tabbar_icon_tipsters
) {
    val placeholder = resPlaceHolder?.let { ContextCompat.getDrawable(this.context, resPlaceHolder) }
            ?: ColorDrawable(Color.LTGRAY)

    Glide.with(this.context)
            .load(image)
            .apply(RequestOptions()
                    .placeholder(placeholder)
                    .error(placeholder))
            .into(this)
}

fun ImageView.loadImageInto(image: String?) {
    loadImageInto(image, null)
}

fun TextView.addTextChangeEmptyListener(listener: ((Boolean) -> Unit)): Unit {
    val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            listener.invoke(s.isNullOrBlank())
        }
    }
    this.addTextChangedListener(textWatcher)
}

package com.rocks.sportibot.utils.extensions

import android.content.Context
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.widget.Toast

fun Context?.showToastMessage(message: String, duration: Int = Toast.LENGTH_SHORT) {
    this?.let { Toast.makeText(it, message, duration).show() }
}

fun Context?.showToastMessage(@StringRes stringRes: Int, duration: Int = Toast.LENGTH_SHORT) {
    this?.let { Toast.makeText(it, stringRes, duration).show() }
}

@ColorInt
fun Context.getColorInt(@ColorRes colorRes: Int): Int {
    return ContextCompat.getColor(this, colorRes)
}
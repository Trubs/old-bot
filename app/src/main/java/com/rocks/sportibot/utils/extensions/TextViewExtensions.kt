package com.rocks.sportibot.utils.extensions

import android.widget.TextView

fun TextView.getTextString() = this.text.toString()
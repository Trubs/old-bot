package com.rocks.sportibot.utils.extensions


import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

const val serverDatePattern: String = "yyyy-MM-dd"
const val serverDateTimePattern: String = "yyyy-MM-dd hh:mm:ss"

const val displayDatePattern: String = "dd.MM.yyyy"
const val displayTimeDatePattern: String = "dd MMM yyyy hh:mm:ss"

private val dateFormat = SimpleDateFormat(serverDatePattern, Locale.ENGLISH)
private val dateTimeFormat = SimpleDateFormat(serverDateTimePattern, Locale.ENGLISH)

fun String.toDateFormat(): Date {
    return dateFormat.parse(this)
}

fun String.toDateTimeFormat(): Date {
    return dateTimeFormat.parse(this)
}

fun String.toSimpleFormat(dateFormat: DateFormat): Date {
    return dateFormat.parse(this)
}

/**
 * First String param should be as [serverDateTimePattern]
 * @return display date as [displayDatePattern]
 * */
fun String.toDateDisplayFormat(): String {
    return this.toDateFormat().toSimpleFormat(displayDatePattern)
}

/**
 * @return display date as [displayDatePattern]
 * */
fun Date.toDateDisplayFormat(): String {
    return this.toSimpleFormat(displayDatePattern)
}

/**
 * @return display date as [displayTimeDatePattern]
 * */
fun Date.toDisplayTimeFormat(): String {
    return this.toSimpleFormat(displayTimeDatePattern)
}

fun Date.toServerFormat(): String {
    return toSimpleFormat(serverDateTimePattern)
}

fun Date.toSimpleFormat(pattern: String): String {
    return SimpleDateFormat(pattern, Locale.getDefault()).format(this)
}

fun Date.toCalendar(ignoreTime: Boolean = false): Calendar {
    return Calendar.getInstance().apply {
        time = this@toCalendar
        if (ignoreTime) {
            clear(Calendar.HOUR_OF_DAY)
            clear(Calendar.HOUR)
            clear(Calendar.MINUTE)
            clear(Calendar.SECOND)
            clear(Calendar.MILLISECOND)
            clear(Calendar.AM_PM)
        }
    }
}

/**
 * Not recommended to use in the chain.
 * For wrapViewObservable use
 * This is the same method as the [Calendar.add]
 * @param field - field of date e.g. YEAR, MONTH, DAY_OF_MONTH
 * @see Calendar field number e.g. [Calendar.YEAR]
 * @param value - the units of date or time to be subtract to the field.
 * */
fun Date.minus(field: Int, value: Int): Date {
    return this.toCalendar().apply {
        roll(field, -value)
    }.time
}

/**
 * Not recommended to use in the chain.
 * For wrapViewObservable use
 * This is the same method as the [Calendar.add]
 * @param field - field of date e.g. YEAR, MONTH, DAY_OF_MONTH
 * @see Calendar field number e.g. [Calendar.YEAR]
 * @param value - the units of date or time to be added to the field.
 * */
fun Date.plus(field: Int, value: Int): Date {
    return this.toCalendar().apply {
        roll(field, +value)
    }.time
}

/**
 * @param year - is year
 * @param month - is month
 * @param dayOfMonth - is day of month
 * @return Calendar [Calendar] with date of params
 * */
fun getCalendarFromArgs(year: Int, month: Int, dayOfMonth: Int): Calendar {
    val calendar = Calendar.getInstance()
    calendar.set(year, month, dayOfMonth)
    return calendar
}

fun Date.equalsIgnoreTime(date: Date): Boolean {
    val calendar1 = this.toCalendar(true)
    val calendar2 = date.toCalendar(true)
    return calendar1 == calendar2
}

fun Date.afterIgnoreTime(date: Date): Boolean {
    val calendar1 = this.toCalendar(true)
    val calendar2 = date.toCalendar(true)
    return calendar1.after(calendar2)
}

fun Date.beforeIgnoreTime(date: Date): Boolean {
    val calendar1 = this.toCalendar(true)
    val calendar2 = date.toCalendar(true)
    return calendar1.before(calendar2)
}

fun Calendar.equalsDateIgnoreTime(calendar: Calendar): Boolean {
    return this[Calendar.YEAR] == calendar[Calendar.YEAR]
            && this[Calendar.MONTH] == calendar[Calendar.MONTH]
            && this[Calendar.DAY_OF_MONTH] == calendar[Calendar.DAY_OF_MONTH]
}

fun getTimeZoneOffsetInMinutes(): Int {
    val timezone = TimeZone.getDefault()
    val secondsInMinute = 60
    val millisecondsInSecond = 1000
    return timezone.rawOffset / (secondsInMinute * millisecondsInSecond) +
            if (timezone.inDaylightTime(Date()))
                timezone.dstSavings / (secondsInMinute * millisecondsInSecond)
            else 0
}

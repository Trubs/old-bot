package com.rocks.sportibot.utils.extensions

import com.rocks.sportibot.R
import com.rocks.sportibot.data.exception.EmailValidationException
import com.rocks.sportibot.data.exception.PasswordsMatchException
import com.rocks.sportibot.ui.base.interfaces.ViewBlockProgressListener
import com.rocks.sportibot.ui.base.interfaces.ViewProgressListener
import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.net.SocketTimeoutException


fun <T : Any> Observable<T>.scheduleIOToMainThread(): Observable<T> = this
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

fun <T : Any> Single<T>.scheduleIOToMainThread(): Single<T> = this
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

fun Completable.scheduleIOToMainThread(): Completable = this
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

fun <T> Single<T>.manageProgressView(viewProgressListener: ViewProgressListener): Single<T> {
    return this.doOnSubscribe { viewProgressListener.showProgress() }
            .doFinally { viewProgressListener.hideProgress() }
}

fun <T> Single<T>.manageBlockProgressView(viewProgressListener: ViewBlockProgressListener): Single<T> {
    return this.doOnSubscribe { viewProgressListener.showBlockProgress() }
            .doFinally { viewProgressListener.hideBlockProgress() }
}

fun <T> Observable<T>.manageProgressView(viewProgressListener: ViewProgressListener): Observable<T> {
    return this.doOnSubscribe { viewProgressListener.showProgress() }
            .doFinally { viewProgressListener.hideProgress() }
}

fun <T> Observable<T>.manageBlockProgressView(viewProgressListener: ViewBlockProgressListener): Observable<T> {
    return this.doOnSubscribe { viewProgressListener.showBlockProgress() }
            .doFinally { viewProgressListener.hideBlockProgress() }
}

fun Completable.manageProgressView(viewProgressListener: ViewProgressListener): Completable {
    return this.doOnSubscribe { viewProgressListener.showProgress() }
            .doFinally { viewProgressListener.hideProgress() }
}

fun Completable.manageBlockProgressView(viewProgressListener: ViewBlockProgressListener): Completable {
    return this.doOnSubscribe { viewProgressListener.showBlockProgress() }
            .doFinally { viewProgressListener.hideBlockProgress() }
}

fun Throwable.manageThrowableView(view: ViewThrowableListener?) {
    this.printStackTrace()
    when (this) {
        is PasswordsMatchException -> view?.showError(R.string.exception_passwords_match)
        is EmailValidationException -> view?.showError(R.string.exception_email_invalid)
        is SocketTimeoutException -> view?.showError(R.string.exception_timeout)
        else -> view?.showError(localizedMessage)
    }
}

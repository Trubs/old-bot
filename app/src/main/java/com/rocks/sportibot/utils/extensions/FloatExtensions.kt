package com.rocks.sportibot.utils.extensions

import java.text.DecimalFormat

/**
 * the numbers after the point will be clipped to 2
 * @return String
 * */
fun Float.formatDecimalAfterPoint2(): String = this.formatDecimal(2)

/**
 * the numbers after the point will be clipped to 2
 * @return String
 * */
fun Double.formatDecimalAfterPoint2(): String = this.formatDecimal(2)

/**
 * the numbers after the point will be clipped to [afterDot]
 * @return String
 * */
fun Float.formatDecimal(afterDot: Int): String = String.format("%.${afterDot}f", this)

/**
 * the numbers after the point will be clipped to [afterDot]
 * @return String
 * */
fun Double.formatDecimal(afterDot: Int): String = String.format("%.${afterDot}f", this)

/**
 * @see [DecimalFormat]
 *
 * "Creates a DecimalFormat using the given pattern and the symbols
 * for the default {@link java.util.Locale.Category#FORMAT FORMAT} locale.
 * This is a convenient way to obtain a
 * DecimalFormat when internationalization is not the main concern.
 * <p>
 * To obtain standard formats for a given locale, use the factory methods
 * on NumberFormat such as getNumberInstance. These factories will
 * return the most appropriate sub-class of NumberFormat for a given
 * locale."
 *
 * @param pattern a non-localized pattern string. * */
fun Float.formatDecimal(pattern: String): String = DecimalFormat(pattern).format(this)

/**
 * @see [DecimalFormat]
 *
 * "Creates a DecimalFormat using the given pattern and the symbols
 * for the default {@link java.util.Locale.Category#FORMAT FORMAT} locale.
 * This is a convenient way to obtain a
 * DecimalFormat when internationalization is not the main concern.
 * <p>
 * To obtain standard formats for a given locale, use the factory methods
 * on NumberFormat such as getNumberInstance. These factories will
 * return the most appropriate sub-class of NumberFormat for a given
 * locale."
 *
 * @param pattern a non-localized pattern string. * */
fun Double.formatDecimal(pattern: String): String = DecimalFormat(pattern).format(this)
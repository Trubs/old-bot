package com.rocks.sportibot.utils.extensions

import com.rocks.sportibot.ui.base.interfaces.ViewThrowableListener
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver

/**
 * Processed errors and responses and returns the result to the functions
 * More ofter for parsing errors
 * If the 'onError' function was implemented then the standard implementation is not used
 * @param view implementation [ViewThrowableListener] for displaying the errors
 * @param successFunction called when [DisposableSingleObserver] calls [DisposableSingleObserver.onSuccess]
 * @param onErrorFunction called when [DisposableSingleObserver] calls [DisposableSingleObserver.onError]
 * @return DisposableSingleObserver
 * */
fun <T> wrapViewSingle(view: ViewThrowableListener,
                       successFunction: ((T) -> Unit) = {},
                       onErrorFunction: ((throwable: Throwable) -> Unit)? = null
): DisposableSingleObserver<T> {
    return object : DisposableSingleObserver<T>() {

        override fun onSuccess(t: T) {
            successFunction.invoke(t)
        }

        override fun onError(e: Throwable) {
            if (onErrorFunction != null) {
                onErrorFunction.invoke(e)
            } else {
                e.manageThrowableView(view)
            }
        }

    }
}

/**
 * Processed errors and responses and returns the result to the functions
 * More ofter for parsing errors
 * If the 'onError' function was implemented then the standard implementation is not used
 * @param view implementation [ViewThrowableListener] for displaying the errors
 * @param successFunction called when [DisposableCompletableObserver] calls [DisposableCompletableObserver.onComplete]
 * @param onErrorFunction called when [DisposableCompletableObserver] calls [DisposableCompletableObserver.onError]
 * @return DisposableCompletableObserver
 * */
fun wrapViewCompletable(view: ViewThrowableListener,
                        successFunction: (() -> Unit) = {},
                        onErrorFunction: ((throwable: Throwable) -> Unit)? = null
): DisposableCompletableObserver {
    return object : DisposableCompletableObserver() {

        override fun onComplete() {
            successFunction.invoke()
        }

        override fun onError(e: Throwable) {
            if (onErrorFunction != null) {
                onErrorFunction.invoke(e)
            } else {
                e.manageThrowableView(view)
            }
        }

    }
}

/**
 * Processed errors and responses and returns the result to the functions
 * More ofter for parsing errors
 * If the 'onError' function was implemented then the standard implementation is not used
 * @param view implementation [ViewThrowableListener] for displaying the errors
 * @param onNextFunction called when [DisposableObserver] calls [DisposableObserver.onNext]
 * @param onCompleteFunction called when [DisposableObserver] calls [DisposableObserver.onComplete]
 * @param onErrorFunction called when [DisposableObserver] calls [DisposableObserver.onError]
 * @return DisposableObserver
 * */
fun <T> wrapViewObservable(view: ViewThrowableListener,
                           onNextFunction: ((T) -> Unit) = {},
                           onCompleteFunction: (() -> Unit) = {},
                           onErrorFunction: ((throwable: Throwable) -> Unit)? = null
): DisposableObserver<T> {
    return object : DisposableObserver<T>() {

        override fun onNext(t: T) {
            onNextFunction.invoke(t)
        }

        override fun onComplete() {
            onCompleteFunction.invoke()
        }

        override fun onError(e: Throwable) {
            if (onErrorFunction != null) {
                onErrorFunction.invoke(e)
            } else {
                e.manageThrowableView(view)
            }
        }

    }
}
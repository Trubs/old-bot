package com.rocks.sportibot.utils.extensions

import android.app.DatePickerDialog
import android.content.Context
import java.util.*

fun Context.showDateDialog(currentDate: Date?,
                           minDate: Date?,
                           maxDate: Date?,
                           onDateSelectedFunction: (year: Int, month: Int, dayOfMonth: Int) -> Unit) {

    val calendar: Calendar = currentDate?.toCalendar() ?: Calendar.getInstance()

    val checkedMinDate = if (minDate != null && maxDate != null && minDate.after(maxDate)) maxDate else minDate

    val dialog = DatePickerDialog(this,

            DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                onDateSelectedFunction(year, month, dayOfMonth)
            },

            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH))

    if (checkedMinDate != null) dialog.datePicker.minDate = checkedMinDate.time
    if (maxDate != null) dialog.datePicker.maxDate = maxDate.time

    dialog.show()
}

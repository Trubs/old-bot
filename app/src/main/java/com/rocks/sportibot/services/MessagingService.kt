package com.rocks.sportibot.services

import com.crashlytics.android.Crashlytics
import com.google.gson.Gson
import com.onesignal.NotificationExtenderService
import com.onesignal.OSNotificationReceivedResult
import com.rocks.sportibot.data.dto.bet.Bet
import com.rocks.sportibot.data.managers.notifications.AppNotificationManager
import com.rocks.sportibot.data.repository.bet.BetsRepository
import com.rocks.sportibot.data.repository.session.SessionRepository
import com.rocks.sportibot.di.providers.ComponentProvider
import com.rocks.sportibot.ui.main.MainActivity
import com.rocks.sportibot.utils.extensions.log_e
import com.rocks.sportibot.utils.extensions.log_i
import com.rocks.sportibot.utils.extensions.tag
import javax.inject.Inject


class MessagingService : NotificationExtenderService() {

    @Inject
    lateinit var appNotificationManager: AppNotificationManager

    @Inject
    lateinit var sessionRepository: SessionRepository

    @Inject
    lateinit var betsRepository: BetsRepository

    @Inject
    lateinit var gson: Gson

    init {
        ComponentProvider.Services.getServicesComponent().inject(this)
    }

    override fun onNotificationProcessing(osNotification: OSNotificationReceivedResult?): Boolean {

        val notification = osNotification?.payload

        if (!sessionRepository.isSessionExist()) {
            val log = "_Message: ${notification?.body} not received. Session does not exist"
            Crashlytics.log(tag() + "_$log")
            log_e(tag(), log)
            return true
        }


        notification?.let {
            val jsonData = it.additionalData.toString()
            log_i("push notification: $jsonData")

            if (!it.additionalData.has("bet")) {
                Crashlytics.log("PushData not contains the bet: $jsonData")
                log_e("PushData not contains the bet: $jsonData")
                return false
            }

            val betJson = it.additionalData.get("bet")

            val bet = gson.fromJson(betJson.toString(), Bet::class.java)
            val game = bet.game
            val id = bet.id.toInt()

            //Notify views to add bet
            betsRepository.onBetAdded(bet)

            //Push notification
            appNotificationManager.sendNotification(id,
                    bet.tipster?.user?.username.orEmpty(),
                    game?.title.orEmpty(),
                    it.body.orEmpty(),
                    MainActivity.getIntentExtra(this, bet))

        }
        return true
    }
}
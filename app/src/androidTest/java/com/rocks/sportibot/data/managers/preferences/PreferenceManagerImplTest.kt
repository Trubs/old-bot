package com.rocks.sportibot.data.managers.preferences

import android.content.Context
import android.support.test.InstrumentationRegistry
import android.support.test.filters.SmallTest
import android.support.test.runner.AndroidJUnit4
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class PreferenceManagerImplTest {

    private val context: Context = InstrumentationRegistry.getTargetContext()

    private val preferenceManager: PreferenceManager = PreferenceManagerImpl(context)

    val key = "Test key"

    @Before
    fun setUp() {
        preferenceManager.clear(key)
    }

    @Test
    fun isContainsData_false() {

        val isContains = preferenceManager.isContainsData(key)

        assertEquals(isContains, false)
    }

    @Test
    fun isContainsData_true() {

        preferenceManager.setData(key, "test data")

        val isContains = preferenceManager.isContainsData(key)

        assertEquals(isContains, true)
    }

    @Test
    fun getData_null() {

        assertEquals(preferenceManager.isContainsData(key), false)

        assertNull(preferenceManager.getData(key))
    }

    @Test
    fun getData_non_null() {
        val testData = "TestDataMustBeSimilar"

        preferenceManager.setData(key, testData)

        assertTrue(preferenceManager.isContainsData(key))

        assertNotNull(preferenceManager.getData(key))

        assertEquals(preferenceManager.getData(key), testData)
    }

    @Test
    fun setData() {

        val testData = "TestDataMustBeSimilar"

        preferenceManager.setData(key, testData)

        assertTrue(preferenceManager.isContainsData(key))
    }

    @Test
    fun clear() {

        preferenceManager.clear(key)

        assertFalse(preferenceManager.isContainsData(key))

        assertNull(preferenceManager.getData(key))

    }
}